#include "Cutflow/CutsInxAOD.h"
#include <iostream>

using namespace std;

CutsInxAOD::CutsInxAOD(double MET){
    missingEt = MET;
}

vector<TLorentzVector> CutsInxAOD ::  getElectronNeg(){
    return e_vector_neg;
}

vector<TLorentzVector> CutsInxAOD ::  getElectronPos(){
    return e_vector_pos;
}

vector<TLorentzVector> CutsInxAOD ::  getJets(){
    return jet_vector;
}

vector<TLorentzVector> CutsInxAOD ::  getZbosonsFromElectrons(){
    return Z_from_electrons;
}

vector<TLorentzVector> CutsInxAOD ::  getZbosonsFromJets(){
    return Z_from_jets;
}

vector<TLorentzVector> CutsInxAOD ::  getZbosonsFromMuons(){
    return Z_from_muons;
}

vector<TLorentzVector> CutsInxAOD ::  getMuonNeg(){
    return mu_vector_neg;
}

vector<TLorentzVector> CutsInxAOD ::  getMuonPos(){
    return mu_vector_pos;
}

vector<TLorentzVector> CutsInxAOD ::  getChPFO(){
    return chPFO;
}

vector<TLorentzVector> CutsInxAOD ::  getNeuPFO(){
    return neuPFO;
}

void CutsInxAOD :: cutNeuPFO(const xAOD::PFOContainer* neuPFOs) {
    double PFO_pt;

    for(unsigned int i = 0; i < neuPFOs->size(); i++) {
        PFO_pt = neuPFOs->at(i)->pt();
        if (PFO_pt > 10000) {
            neuPFO.push_back(TLorentzVector());
            neuPFO.back().SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
        }
    }
}

void CutsInxAOD :: cutChPFO(const xAOD::PFOContainer* chPFOs) {
    double PFO_pt;

    for(unsigned int i = 0; i < chPFOs->size(); i++) {
        PFO_pt = chPFOs->at(i)->pt();
        if (PFO_pt > 10000) {
            chPFO.push_back(TLorentzVector());
            chPFO.back().SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());
        }
    }
}

std::vector<TLorentzVector> CutsInxAOD :: analyzeJets(const xAOD::JetContainer* jets,JetCleaningTool *m_jetCleaning, double cutpt, double cutmass) {
    std::vector<TLorentzVector> jet_vector;
    // loop over the jets in the container
    xAOD::JetContainer::const_iterator jet_itr = jets->begin();
    xAOD::JetContainer::const_iterator jet_end = jets->end();
    for( ; jet_itr != jet_end; ++jet_itr ) {
        //cout << "jet pt = " << (*jet_itr)->pt() << "jet z = " << (*jet_itr)->z0() << endl;
        if( !m_jetCleaning->accept( **jet_itr )) continue; //only keep good clean jets
        if ((*jet_itr)->pt() > cutpt && (*jet_itr)->m() > cutmass && abs((*jet_itr)->eta()) < 2.5) {
            jet_vector.push_back(TLorentzVector());
            jet_vector.back().SetPtEtaPhiM((*jet_itr)->pt(), (*jet_itr)->eta(), (*jet_itr)->phi(), (*jet_itr)->m());
        }
    }
    return jet_vector;
}


std::vector<TLorentzVector> CutsInxAOD :: analyzeFatjets(const xAOD::JetContainer* fatjets, double cutpt, double cutmass) {
    std::vector<TLorentzVector> fatjet_vec;
    // loop over the muons in the container
    xAOD::JetContainer::const_iterator jet_itr = fatjets->begin();
    xAOD::JetContainer::const_iterator jet_end = fatjets->end();

    for(; jet_itr != jet_end; ++jet_itr) {
        if ((*jet_itr)->pt() > cutpt && (*jet_itr)->m() > cutmass && abs((*jet_itr)->eta()) < 2.5) {
            fatjet_vec.push_back(TLorentzVector());
            fatjet_vec.back().SetPtEtaPhiM((*jet_itr)->pt(), (*jet_itr)->eta(), (*jet_itr)->phi(), (*jet_itr)->m());
        }
    }
    return fatjet_vec;
}




std::vector<TLorentzVector> CutsInxAOD :: analyzeMuons(const xAOD::MuonContainer* muons, double cutpt, double cutmass, bool isolated, double etacut) {
    // loop over the muons in the container
    xAOD::MuonContainer::const_iterator muon_itr = muons->begin();
    xAOD::MuonContainer::const_iterator muon_end = muons->end();
    std::vector<TLorentzVector> mu_vector;
    float ptcone;

    for(; muon_itr != muon_end; ++muon_itr) {
        double muon_pt = (*muon_itr)->pt();
        ptcone = (*muon_itr)->auxdata<float>("ptcone20");
        double iso = ptcone / muon_pt;
        double muon_eta = (*muon_itr)->eta();
        double muon_phi = (*muon_itr)->phi();
        double muon_charge = (*muon_itr)->charge();
        //cout << "muon pt = " << muon_pt << ", muon iso = " << iso << ", muon eta = " << muon_eta << ", muon_z = " <<(*muon_itr)->z0() <<  endl;
        
        if (!isolated && muon_pt > cutpt && (*muon_itr)->m() > cutmass ){
            if (abs(muon_eta) < 2.5 ){
                if (muon_charge > 0) {
                    mu_vector.push_back(TLorentzVector());
                    mu_vector.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);
                    mu_vector_pos.push_back(TLorentzVector());
                    mu_vector_pos.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);
                }
                else {
                    mu_vector.push_back(TLorentzVector());
                    mu_vector.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);      
                    mu_vector_neg.push_back(TLorentzVector());
                    mu_vector_neg.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);          
                }
            }
        }
        
        if (iso < 0.1 && isolated) {
            if (muon_pt > cutpt && abs(muon_eta) < etacut){
                if (muon_charge > 0) {
                    mu_vector.push_back(TLorentzVector());
                    mu_vector.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);
                    mu_vector_pos.push_back(TLorentzVector());
                    mu_vector_pos.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);
                }
                else {
                    mu_vector.push_back(TLorentzVector());
                    mu_vector.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);      
                    mu_vector_neg.push_back(TLorentzVector());
                    mu_vector_neg.back().SetPtEtaPhiM(muon_pt, muon_eta, muon_phi, 105.6);          
                }
            }
        }
    }
    return mu_vector;
}

std::vector<TLorentzVector> CutsInxAOD :: analyzeTauJets(const xAOD::TauJetContainer* taujets, double cutpt,double cutmass ) {

    std::vector<TLorentzVector> taujet_vec;
    xAOD::TauJetContainer::const_iterator tau_itr = taujets->begin();
    xAOD::TauJetContainer::const_iterator tau_end = taujets->end();

    for(; tau_itr != tau_end; ++tau_end) {
        if ((*tau_itr)->pt() > cutpt && (*tau_itr)->m() > cutmass) {
            taujet_vec.push_back(TLorentzVector());
            taujet_vec.back().SetPtEtaPhiM((*tau_itr)->pt(), (*tau_itr)->eta(), (*tau_itr)->phi(), (*tau_itr)->m());
        }
    }
    return taujet_vec;
}






std::vector<TLorentzVector> CutsInxAOD :: analyzeElectrons(const xAOD::ElectronContainer* electrons, double cutpt, double cutmass, bool isolated, double etacut) {
    // loop over the muons in the container
    xAOD::ElectronContainer::const_iterator e_itr = electrons->begin();
    xAOD::ElectronContainer::const_iterator e_end = electrons->end();
    std::vector<TLorentzVector> e_vector;
    float ptcone;

    for(; e_itr != e_end; ++e_itr) {
        double e_pt = (*e_itr)->pt();
        ptcone = (*e_itr)->auxdata<float>("ptcone20");
        double iso = ptcone / e_pt;
        double e_eta = (*e_itr)->eta();
        double e_phi = (*e_itr)->phi();
        double e_charge = (*e_itr)->charge();
//cout << "e pt = " << e_pt << ", e iso = " << iso << ", e eta = " << e_eta << endl;
                if (!isolated && e_pt > cutpt && (*e_itr)->m() > cutmass ){
            if (abs(e_eta) < 2.5 ){
                if (e_charge > 0) {
                    e_vector.push_back(TLorentzVector());
                    e_vector.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);
                    e_vector_pos.push_back(TLorentzVector());
                    e_vector_pos.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);
                }
                else {
                    e_vector.push_back(TLorentzVector());
                    e_vector.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);      
                    e_vector_neg.push_back(TLorentzVector());
                    e_vector_neg.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);          
                }
            }
        }
        
        if (iso < 0.1 && isolated) {
            if (e_pt > cutpt && abs(e_eta) < etacut){
                if (e_charge > 0) {
                    e_vector.push_back(TLorentzVector());
                    e_vector.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);
                    e_vector_pos.push_back(TLorentzVector());
                    e_vector_pos.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);
                }
                else {
                    e_vector.push_back(TLorentzVector());
                    e_vector.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);      
                    e_vector_neg.push_back(TLorentzVector());
                    e_vector_neg.back().SetPtEtaPhiM(e_pt, e_eta, e_phi, 0.511);          
                }
            }
        }
    }
    return e_vector;
}

/*
double CutsInxAOD :: analyzeZbosonsFromElectrons(const xAOD::ElectronContainer* electrons) {
    double cut = 0;

    analyzeElectrons(electrons,0,0);
    //cout << "positive electrons: " << e_vector_pos.size() << endl;
    //cout << "negative electrons: " << e_vector_neg.size() << endl;
    if (e_vector_neg.size() > 0 || e_vector_pos.size() > 0) {
        //cut++;
        for (unsigned int i = 0; i < e_vector_pos.size(); i++) {
            for (unsigned int j = 0; j < e_vector_neg.size(); j++) {
                if (e_vector_neg[j].Pt() > 50000 || e_vector_pos[i].Pt() > 50000){
                    cut++;
                //if ((e_vector_pos[i] + e_vector_neg[j]).Pt()*0.001 > 200) {
                    //cout << "ee: Z.pt > 200 GeV" << endl;
                    if ((e_vector_neg[j].Et()*0.001 + missingEt*0.001) > 200 ||  (e_vector_pos[j].Et()*0.001 + missingEt*0.001) > 200) {
                        cut++;
                    //if ((e_vector_pos[i] + e_vector_neg[j]).M()*0.001 - 90 > 15) {
                        //cut++;
                        //cout << "| m_ee - m_Z | < 15 GeV" << endl;
                        //cout << "  " << endl;
                        Z_from_electrons.push_back(e_vector_pos[i] + e_vector_neg[j]);
                    }
                }
            }
        }
    }
    return cut;
}

double CutsInxAOD :: analyzeZbosonsFromJets(const xAOD::JetContainer* jets,JetCleaningTool *m_jetCleaning, double cutmass, double energycut) {
    analyzeJets(jets,m_jetCleaning,0,0);
    double cut = 0;
    if (jet_vector.size() != 0) {
        for (unsigned int i = 0; i < jet_vector.size() - 1; i = i + 2) {
            if ((jet_vector[i] + jet_vector[i+1]).M() > cutmass) {
                if ((jet_vector[i] + jet_vector[i+1]).Pt() > energycut) {
                    Z_from_jets.push_back(jet_vector[i] + jet_vector[i + 1]);
                    cut = 1;
                }
            }
        }
        return cut;
    }
}

double CutsInxAOD :: analyzeZbosonsFromMuons(const xAOD::MuonContainer* muons) {
    analyzeMuons(muons,0,0);
    double cut = 0;

    //cout << "positive muons: " << mu_vector_pos.size() << endl;
    //cout << "negative muons: " << mu_vector_neg.size() << endl;
    if (mu_vector_neg.size() > 0 || mu_vector_pos.size() > 0) {
        //cut++;
        for (unsigned int i = 0; i < mu_vector_pos.size(); i++) {
            for (unsigned int j = 0; j < mu_vector_neg.size(); j++) {
                if (mu_vector_neg[j].Pt() > 50000 || mu_vector_pos[i].Pt() > 50000){
                    cut++;
                    if ((mu_vector_neg[j].Et()*0.001 + missingEt*0.001) > 200 ||  (mu_vector_pos[j].Et()*0.001 + missingEt*0.001) > 200) {
                //if ( (mu_vector_pos[i] + mu_vector_neg[j]).Pt()*0.001 > 200) {
                    //cout << "mumu: Z.pt > 200 GeV" << endl;
                        cut++;
                        /*
                    if ((mu_vector_pos[i]).Pt()*0.001 > 25 && (mu_vector_neg[i]).Pt()*0.001 > 25) {
                        cout << "mumu: both mu.pt() > 25 GeV" << endl;
                        cout << "  " << endl;
                        cut++;
                        Z_from_muons.push_back(mu_vector_pos[i] + mu_vector_neg[j]);
                    }*/
                    /*}}
                }
            }
        }
        return cut;
    }

    void CutsInxAOD :: printZbosonsFromElectrons() {
        for (unsigned int i = 0; i < Z_from_electrons.size(); i++) {
            Info("execute()", "  Z boson from electrons      pt = %.2f GeV   eta = %.3f   phi = %.3f    m = %.1f",
               Z_from_electrons[i].Pt() * 0.001,
               Z_from_electrons[i].Eta(),
               Z_from_electrons[i].Phi(),
               Z_from_electrons[i].M());

            Info("execute()", "       1st electron pt = %.2f GeV   eta = %.3f   phi = %.3f,    m = %.1f",
               e_vector_pos[i].Pt() * 0.001,
               e_vector_pos[i].Eta(),
               e_vector_pos[i].Phi(),
               e_vector_pos[i].M());
            Info("execute()", "       2nd electron pt = %.2f GeV   eta = %.3f   phi = %.3f,    m = %.1f",
               e_vector_neg[i].Pt() * 0.001,
               e_vector_neg[i].Eta(),
               e_vector_neg[i].Phi(),
               e_vector_neg[i].M());
            cout <<
            "-----------------------------------------------------------------------------------------------------------------------" <<
            endl;
        }
    }

    void CutsInxAOD :: printZbosonsFromMuons() {
        for (unsigned int i = 0; i < Z_from_muons.size(); i++) {
            Info("execute()", "  Z boson from muons      pt = %.2f GeV   eta = %.3f   phi = %.3f    m = %.1f",
               Z_from_muons[i].Pt() * 0.001,
               Z_from_muons[i].Eta(),
               Z_from_muons[i].Phi(),
               Z_from_muons[i].M());

            Info("execute()", "       1st muon pt = %.2f GeV   eta = %.3f   phi = %.3f",
               mu_vector_pos[i].Pt() * 0.001,
               mu_vector_pos[i].Eta(),
               mu_vector_pos[i].Phi());
            Info("execute()", "       2nd muon pt = %.2f GeV   eta = %.3f   phi = %.3f",
               mu_vector_neg[i].Pt() * 0.001,
               mu_vector_neg[i].Eta(),
               mu_vector_neg[i].Phi());
            cout <<
            "-----------------------------------------------------------------------------------------------------------------------" <<
            endl;

        }
    }
*/