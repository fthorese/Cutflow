#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "Cutflow/Boildown.h"
#include "Cutflow/MyMETAnalysis.h"
#include <fstream> 
#include <ctime>

using namespace std;

int main( int argc, char* argv[]) {
char gridon = '1';
    // Take the submit directory from the input if provided:
    std::string submitDir = "runs/test5";
    if( argc > 1 ){ submitDir = argv[ 1 ];}

    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on:
    SH::SampleHandler sh;
    cout << "hellos" << endl;
    cout << "argv[1] " << argv[1] << endl;
    cout << "argv[2] " << argv[2] << endl;
    // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
   // for grid

    if (*argv[2] == gridon) {
        //SH::scanDQ2(sh, "mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7701/");
        
        cout << "reading files..." << endl;
        std::ifstream infile("/hep/thoresen/work/april/Cutflow/Cutflow/util/sampleLists/Zmumu.txt");
        std::string sample;
        while (std::getline(infile, sample)) {
            cout << "in da file!" << endl;
            SH::scanDQ2 (sh, sample.c_str());
            cout << sample.c_str() << endl;
        }
        
    }
    else {
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/work/Cutflow/runs/36/data-outputLabel/");
        //SH::ScanDir().filePattern("mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.merge.AOD.e4054_s2608_s2183_r6630_r6264.root").scan(sh, inputFilePath);
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/work/JanCutflow/Cutflow/");
        //SH::ScanDir().filePattern("user.fthorese.Searching_for_Z.TEST.lol.341314.e3940_s2608_s2183_r6869_r6282_outputLabel.root").scan(sh, inputFilePath);

        //const char *inputFilePath = gSystem->ExpandPathName("/hep/storage/thoresen/");
        //SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
//mc15_13TeV.301256.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m800.merge.AOD.e3743_s2608_s2183_r7380_r6282
//WWdata/mc15_13TeV.303227.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_WW_lvqq_c10_m0800.merge.AOD.e4175_s2608_r6869_r6282
//mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7380_r6282

/*
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2630_s2206_r7709
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2630_s2206_r7768
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2630_s2206_r7769
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7699
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7700
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7701
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2640_s2206_r7702
*/

       /* std::ifstream infile("/hep/thoresen/work/april/Cutflow/Cutflow/util/sampleLists/samples2.txt");
        std::string sample;
        while (std::getline(infile, sample)) {
            cout << "in da file!" << endl;
            const char *inputFilePath = gSystem->ExpandPathName(sample.c_str());
            SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
            cout << sample.c_str() << endl;
        }*/

//user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2630_s2206_r7768
//user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2630_s2206_r7769
//user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2638_s2206_r7699
//user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2640_s2206_r7702

        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/grid/grid_06/22/user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2630_s2206_r7768/user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2630_s2206_r7768_outputLabel.root"); //test9
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/grid/grid_06/22/user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2630_s2206_r7769/user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2630_s2206_r7769_outputLabel.root"); //test10
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/grid/grid_06/user.fthorese.ttbar.boildown.real.06.22.407099.e4906_s2640_s2206_r7702_outputLabel.root"); // test11
        /*
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2630_s2206_r7709_outputLabel.root
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2630_s2206_r7768_outputLabel.root
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2630_s2206_r7769_outputLabel.root
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2638_s2206_r7699_outputLabel.root
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2638_s2206_r7700_outputLabel.root
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2638_s2206_r7701_outputLabel.root
        /hep/thoresen/grid/07_19/user.fthorese.ttbar.07.19.407099.e4906_s2640_s2206_r7702_outputLabel.root
        */

        // mc15_14TeV.147807.PowhegPythia8_AU2CT10_Zmumu.recon.AOD.e1564_s2630_s2206_r7768
        // mc15_14TeV.147807.PowhegPythia8_AU2CT10_Zmumu.recon.AOD.e1564_s2630_s2206_r7769
        // mc15_14TeV.147807.PowhegPythia8_AU2CT10_Zmumu.recon.AOD.e1564_s2638_s2206_r7699

        const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/grid/07_06/user.fthorese.ttbar.boildown.07.06.407099.e4906_s2630_s2206_r7769_outputLabel.root"); // test11
/*
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2630_s2206_r7709_outputLabel.root
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2630_s2206_r7768_outputLabel.root
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2630_s2206_r7769_outputLabel.root
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2638_s2206_r7699_outputLabel.root
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2638_s2206_r7700_outputLabel.root
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2638_s2206_r7701_outputLabel.root
user.fthorese.ttbar.boildown.07.06.407099.e4906_s2640_s2206_r7702_outputLabel.root
*/
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/data/ttbar/mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7699/");
        SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/work/JanCutflow/Cutflow/runs/DAOD_VBF1000H_ZZllqq/data-outputLabel/");
        //SH::ScanDir().filePattern("mc15_13TeV.341339.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH1000NW_ZZllqq.merge.AOD.e3940_s2608_s2183_r6765_r6282.root").scan(sh, inputFilePath);
    }
    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );
    //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
    // Print what we found:
    sh.print();

    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler( sh );
    //cout << "args = " << argv[3] << ", " << argv[4] << ", " << argv[5] << endl;
    job.options()->setDouble (EL::Job::optMaxEvents, 1000);
    // Add our analysis to the job:
    //GridMETAnalysis* alg = new GridMETAnalysis();
    //double filterCut, double denoiseCut,double timesRatio

    //MyMETAnalysis* alg = new MyMETAnalysis(atof(argv[3]),atof(argv[4]),atof(argv[5]));
    MyMETAnalysis* alg = new MyMETAnalysis();
    // alg->filtercut = atof(argv[3]);
    // alg->denoisecut = atof(argv[4]);
    // alg->timesratio = atof(argv[5]);

    job.algsAdd( alg );

    // define an output and an ntuple associated to that output
    EL::OutputStream output("search_out");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("search_out");
    job.algsAdd (ntuple);
    alg->outputName = "search_out"; // give the name of the output to our algorithm
    //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);

    //string rawtime = ctime(0);
    //cout << rawtime << endl;

    // for grid
    if (*argv[2] == gridon) {
        EL::PrunDriver driver;
        driver.options()->setString("nc_outputSampleName",
                                    "user.fthorese.METfromWWboildown.08.16.%in:name[2]%.%in:name[6]%");
        driver.submitOnly( job, submitDir );
    }
    else {
        EL::DirectDriver driver;
        driver.options()->setString("nc_outputSampleName",
                                    "user.fthorese.Searching_for_Z.%in:name[2]%.%in:name[6]%");
        driver.submit( job, submitDir );
    }

    return 0;
}