// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include <TTree.h>
#include "TFile.h"
#include <vector>
#include "TLorentzVector.h"
#include "EventLoop/OutputStream.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include "Cutflow/Boildown.h"
#include "Cutflow/CutsInxAOD.h"
#include "xAODMuon/MuonContainer.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include <TSystem.h>
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
//#include "TrigConfxAOD/xAODConfigTool.h"
//#include "TrigDecisionTool/TrigDecisionTool.h"
#include <fstream>
using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(Boildown)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
do {                                                        \
  if( ! EXP.isSuccess() ) {                                 \
   Error( CONTEXT,                                        \
    XAOD_MESSAGE( "Failed to execute: %s" ),            \
                #EXP );                                     \
    return EL::StatusCode::FAILURE;                     \
}                                                       \
} while( false )


Boildown :: Boildown ()
{
}



EL::StatusCode Boildown :: setupJob (EL::Job& job)
{
    // tell EventLoop about our output xAOD:
    EL::OutputStream out ("outputLabel", "xAOD");
    job.outputAdd (out);

    job.useXAOD ();

    EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode Boildown :: histInitialize ()
{
    TFile *outputFile = wk()->getOutputFile (outputName);
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode Boildown :: fileExecute ()
{
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode Boildown :: changeInput (bool firstFile)
{
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode Boildown :: initialize ()
{
    xAOD::TEvent* event = wk()->xaodEvent();
    // output xAOD
    TFile *file = wk()->getOutputFile ("outputLabel");
    EL_RETURN_CHECK("initialize()",event->writeTo(file));

    cutsplot = new TH1F("Cutflow","Cutflow", 7,0,7);
    cutsplot->GetXaxis()->SetBinLabel( 1 , "Initial" );
    cutsplot->GetXaxis()->SetBinLabel( 2 , "> 4 tracks with pt > 0.4 GeV" ); 
    cutsplot->GetXaxis()->SetBinLabel( 3 , "Single lepton with pt(iso) > 60(24) GeV" );
    cutsplot->GetXaxis()->SetBinLabel( 4 , "Exactly one iso lepton, mu(e), pT > 25(25), eta < 2.5(0.0)" );
    cutsplot->GetXaxis()->SetBinLabel( 5 , "4 Jets with pt > 25 GeV, eta < 2.5" ); 
    cutsplot->GetXaxis()->SetBinLabel( 6 , "> 0 b-jets with eff. = 0.7" ); 
    e_tree = new TNtuple("electrons", "electrons","pt:iso");
    e_tree->SetDirectory (file);
    mu_tree = new TNtuple("muons", "muons","pt:iso");
    mu_tree->SetDirectory (file);
    //cutsplot->GetXaxis()->SetBinLabel( 7 , "Taujet passed" ); 

    // B-tagging for 70 percent background efficiency
    btag_70 = new BTaggingSelectionTool("btag_70");
    btag_70->setProperty( "MaxEta", 2.5 );
    btag_70->setProperty( "MinPt", 25000. ); 
    btag_70->setProperty( "FlvTagCutDefinitionsFileName","xAODBTaggingEfficiency/cutprofiles_22072015.root" );
    btag_70->setProperty( "TaggerName",     "MV2c20"  );
    btag_70->setProperty( "OperatingPoint", "FixedCutBEff_70"   );
    btag_70->setProperty( "JetAuthor",      "AntiKt4EMTopoJets" );
    btag_70->initialize() ;


    wk()->addOutput(cutsplot);

    // Initialize and configure trigger tools
/*
    m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    EL_RETURN_CHECK("hej", m_trigConfigTool->initialize() );
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK("hej",m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK("hej",m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    EL_RETURN_CHECK("hej",m_trigDecisionTool->initialize() );
*/

    // initialize and configure the jet cleaning tool
    m_jetCleaning = new JetCleaningTool("JetCleaning");
    m_jetCleaning->msg().setLevel( MSG::ERROR ); 
    EL_RETURN_CHECK("initialize()", m_jetCleaning->setProperty( "CutLevel", "LooseBad"));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->setProperty("DoUgly", false));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->initialize());

    // count number of events
    m_eventCounter = 0;

    // as a check, let's see the number of events in our xAOD
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

    //const xAOD::EventInfo* eventInfo = 0;

    // check if the event is MC
    //if(!eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

            /*
        m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
        const char* GRLFilePath = "share/GRLs";
        const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
        std::vector<std::string> vecStringGRL;
        vecStringGRL.push_back(fullGRLFilePath);
        EL_RETURN_CHECK("initialize()",m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
        EL_RETURN_CHECK("initialize()",m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
        EL_RETURN_CHECK("initialize()",m_grl->initialize());
        */
    //}
        return EL::StatusCode::SUCCESS;
    }



    EL::StatusCode Boildown :: execute ()
    {

        double trigIsPassed = 0;
    // examine the HLT_xe80* chains, see if they passed/failed and their total prescale
    //    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80.*");
        /*
        auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80_L1XE50");

        std::map<std::string,int> triggerCounts;
        for(auto &trig : chainGroup->getListOfTriggers()) {
          auto cg = m_trigDecisionTool->getChainGroup(trig);
          std::string thisTrig = trig;
          Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
          if (cg->isPassed() == 1) {trigIsPassed = 1;}
    } // end for loop (c++11 style) over chain group matching "HLT_xe80*" 
*/
    trigIsPassed = 1;
    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- EVENT INFORMATION -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    bool copyEvent = false;
    xAOD::TEvent* event = wk()->xaodEvent();

    // print every 100 events, so we know where we are:
    if( (m_eventCounter % 1) ==0 ) //Info("execute()", "Event number = %i", m_eventCounter );
    m_eventCounter++;

    const xAOD::EventInfo* eventInfo = 0;
     //const xAOD::EventInfo* eventInfo = 0;
    EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));
    double N_pileup = eventInfo->averageInteractionsPerCrossing();

    //bool isMC = false;
    // check if the event is MC
    //if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     //   isMC = true; // can do something with this later
    //}

    
    Info("execute()", " ");
    Info("execute()", "=====================");
    Info("execute()", " * Event number %4d", m_eventCounter);
    Info("execute()", "---------------------");
    



    // check if the event is data or MC
    // (many tools are applied either to data or MC)
    bool isMC = false;
    // check if the event is MC
    if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
        isMC = true; // can do something with this later
        cout << "------ Data is from MC" << endl;
    }
    else {
        cout << "------ Data is from raw data" << endl;
    }

    // if data check if event passes GRL
    /*
    if(!isMC){ // it's data!
    if(!m_grl->passRunLB(*eventInfo)){
        //return EL::StatusCode::SUCCESS; // go to next event
        cout << "This is not in GoodRunsList" << endl;
        }
    } // end if not MC
    */

    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- CONTAINERS --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    cout << "Getting containers" << endl;
    const xAOD::BTaggingContainer* btags = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( btags, "BTagging_AntiKt4LCTopo" ) );

    const xAOD::VertexContainer* PVs = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( PVs, "PrimaryVertices" ) );

    const xAOD::TauJetContainer* taujets = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( taujets,  "TauJets" ) );

    const xAOD::MissingETContainer* METcontainer = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( METcontainer, "MET_Truth"));

    const xAOD::JetContainer* jets = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( jets, "AntiKt4LCTopoJets" ));

    const xAOD::MuonContainer* muons = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( muons, "Muons" ));

    const xAOD::ElectronContainer* electrons = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( electrons, "Electrons" ));
    
    const xAOD::JetContainer* fatjets = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( fatjets, "AntiKt10LCTopoJets" ));

    const xAOD::PFOContainer* chPFOs = 0;
    const xAOD::PFOContainer* neuPFOs = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( chPFOs,  "JetETMissChargedParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( neuPFOs, "JetETMissNeutralParticleFlowObjects" ) );
    const xAOD::TruthEventContainer* xTruthEventContainer = 0;
    EL_RETURN_CHECK( "execute()", event->retrieve( xTruthEventContainer, "TruthEvents"));

    xAOD::ElectronContainer::const_iterator e_itr = electrons->begin();
    xAOD::ElectronContainer::const_iterator e_end = electrons->end();
    std::vector<TLorentzVector> e_vector;

    double e_max = 0;

    for(; e_itr != e_end; ++e_itr) {
        double e_pt = (*e_itr)->pt();
        if (e_pt > e_max) {
                    e_max = e_pt;
        }
    }

    cout << "e_max = " << e_max << endl;
e_itr = electrons->begin();
e_end = electrons->end();
    for(; e_itr != e_end; ++e_itr) {
        float ptcone;

        double e_pt = (*e_itr)->pt();
        ptcone = (*e_itr)->auxdata<float>("ptcone20");
        double iso = ptcone / e_pt;
        if (e_pt > 40000 && e_pt < e_max) {
            e_tree->Fill(e_pt, iso);
        }
    }
    double mu_max = 0;
    xAOD::MuonContainer::const_iterator mu_itr = muons->begin();
    xAOD::MuonContainer::const_iterator mu_end = muons->end();
    std::vector<TLorentzVector> mu_vector;
    for(; mu_itr != mu_end; ++mu_itr) {
        double mu_pt = (*mu_itr)->pt();
        if (mu_pt > mu_max) {
                mu_max = mu_pt; 
        }
    }
    cout << "mu_max = " << mu_max << endl;
    mu_itr = muons->begin();
    mu_end = muons->end();
    for(; mu_itr != mu_end; ++mu_itr) {
        float ptcone;

        double mu_pt = (*mu_itr)->pt();
        ptcone = (*mu_itr)->auxdata<float>("ptcone20");
        double iso = ptcone / mu_pt;
        if (mu_pt > 24000 && mu_pt < mu_max) {
                mu_tree->Fill(mu_pt, iso);
        }
    }

// pileup
    std::vector<TLorentzVector> pileup_vector;  

    xAOD::TruthEventContainer::const_iterator itr;
    for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
        const
        xAOD::TruthVertex* nVert = (*itr)->truthVertex(0);
        int nvertex = (*itr)->nTruthVertices();
        int nParth = (*itr)->nTruthParticles();
        //nParthg = (*itr)->nTruthParticles();
        //const xAOD::TruthParticle* nPart = (*itr)->truthParticle(0);
        //cout << "HEEEJ TRUTH " << nPart->pt() << ", " << nParth << endl;
        cout << "nTruthVertices = " << nvertex << endl;
        cout << "z = " << nVert->z() << endl;
        /*
        for (int i = 0; i < nParth; i++) {
            const xAOD::TruthParticle* truth = (*itr)->truthParticle(i);

            TLorentzVector ps = TLorentzVector();
            ps.SetPxPyPzE(truth->p4().Px(), truth->p4().Py(), truth->p4().Pz(), truth->p4().E());
            if (abs(truth->eta()) < 3.2 && truth->status() == 1 && truth->barcode() < 100000 ){
                if (abs(truth->pdgId()) != 12 && abs(truth->pdgId()) != 14 && abs(truth->pdgId()) != 16) {
                    truthInt.push_back(ps);
                }              
                else {
                    truthNonInt.push_back(ps);
                }
            }
            if (abs(truth->eta()) < 3.2 && truth->status() == 1 && truth->barcode() > 100000){
                truthPileup.push_back(ps);
            }
        }*/
    }




    double bjetcut = 0;
    int num_b70 = 0;
    int ijet = -1;
    double cutvar = 0;
    for ( const xAOD::Jet* jet : *jets ) {
        ijet++;
        num_b70 += btag_70->accept( jet );   
    }
    cout << "b70 = " << num_b70 << endl;
    cout << "ijet" << ijet << endl;

    double PVcut = 0;
        xAOD::VertexContainer::const_iterator PV_itr = PVs->begin();
        xAOD::VertexContainer::const_iterator PV_end = PVs->end();
    for ( ; PV_itr != PV_end; ++PV_itr) {
        for (unsigned i = 0; i < (*PV_itr)->nTrackParticles(); i++) {
            if ((*PV_itr)->trackParticle(i)->pt() > 400) { PVcut++; }
        }
        if (PVcut < 5) {PVcut = 0;}
    }

    CutsInxAOD cutAnalyzer = CutsInxAOD((*METcontainer)["NonInt"]->met());

    std::vector<TLorentzVector> PFO;
    cout << "PFO fill" << endl;

    // Charged
    /*for (unsigned int i = 0; i < chPFOs->size(); i++) {

        TLorentzVector particle =  TLorentzVector();
        particle.SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(),chPFOs->at(i)->p4().Pz(),chPFOs->at(i)->p4().E());
        particle.SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());

        PFO.push_back(particle);
    }
    // Neutral
    for (unsigned int i = 0; i < neuPFOs->size(); i++) {
        TLorentzVector particle =  TLorentzVector();
        particle.SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(),neuPFOs->at(i)->p4().Pz(),neuPFOs->at(i)->p4().E());
        particle.SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
        PFO.push_back(particle);
    }*/
    // ---------------------------------------------- Muons --------------------------------------------------
    // ptcut, masscut
    //double METcut = 2000;
    cout << "Cutflow -- muon" << endl;

    std::vector<TLorentzVector> firstCutMuon = cutAnalyzer.analyzeMuons(muons,24000,0.0,true,0.0);
    std::vector<TLorentzVector> secondCutMuon = cutAnalyzer.analyzeMuons(muons,36000,0.0,false,0.0);
    std::vector<TLorentzVector> thirdCutMuon = cutAnalyzer.analyzeMuons(muons,25000,0.0,true,2.5);


    cout << "Cutflow -- electron" << endl;
    std::vector<TLorentzVector> firstCutElectron = cutAnalyzer.analyzeElectrons(electrons,24000,0.0,true,0.0);
    std::vector<TLorentzVector> secondCutElectron = cutAnalyzer.analyzeElectrons(electrons,60000,0.0,false,0.0);
    std::vector<TLorentzVector> thirdCutElectron = cutAnalyzer.analyzeElectrons(electrons,40000,0.0,true,0.0);

    cout << "Cutflow -- jet" << endl;
    std::vector<TLorentzVector> cutJet = cutAnalyzer.analyzeJets(jets,m_jetCleaning,25000,0.0);


    //std::vector<TLorentzVector> cutTaujets = cutAnalyzer.analyzeTauJets(taujets,70000,0.0);
    //cout << "Cutflow -- fatjet" << endl;
    //std::vector<TLorentzVector> cutFatjets = cutAnalyzer.analyzeFatjets(fatjets,0.0,0.0);

    cutsplot->Fill(0);
    copyEvent = false;
    if (PVcut > 0 ) {copyEvent = true; cutsplot->Fill(1);}

    //if (copyEvent && trigIsPassed == 1) {passTrigger++; cutsplot->Fill(1);}        
    //else{copyEvent = false;}

    //if (copyEvent && METvector(PFO)[2] > METcut) {cutsplot->Fill(2);}
    //else{copyEvent = false;}

    if (copyEvent && firstCutElectron.size() > 0 || firstCutMuon.size() > 0 || secondCutElectron.size() > 0 || secondCutMuon.size() > 0) {cutsplot->Fill(2);}
    else{copyEvent = false;}

    if ((copyEvent && thirdCutMuon.size() == 1 && thirdCutElectron.size() == 0) || (copyEvent && thirdCutMuon.size() == 0 && thirdCutElectron.size() == 1)) {cutsplot->Fill(3);}
    else{copyEvent = false;}

    if (copyEvent && cutJet.size() > 3) {cutsplot->Fill(4);}
    else{copyEvent = false;}

    if (copyEvent && num_b70 > 0) {cutsplot->Fill(5);}
    else{copyEvent = false;}
    

    if (copyEvent == true) {
        cout << "copying event " << m_eventCounter << endl;
        string line;

        string out = gSystem->ExpandPathName( "$ROOTCOREBIN" );

        ifstream myfile (out + "/../Cutflow/share/" + "outputttbar.txt");
        if (myfile.is_open())
        {
            cout << "   file is open" << endl;
            while ( getline (myfile,line) )
            {
                if (event->copy(line).isSuccess()) {
                    //cout << "reading line.." << endl;
                    //cout << line << endl;
                    //cout << typeid(line).name() << endl;
                    EL_RETURN_CHECK("execute()",event->copy(line));
                }
            }
            myfile.close();
            cout << "   file is closed" << endl;
            event->fill();
        }
        else cout << "Unable to open file"; 
    }
    else {
        //cout << "event not copied" << endl;
    }

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode Boildown :: postExecute ()
{

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode Boildown :: finalize ()
{
    xAOD::TEvent* event = wk()->xaodEvent();
    /*   
    if (m_grl) {
    delete m_grl;
    m_grl = 0;
    }
    */
    
    // finalize and close our output xAOD file:
    TFile *file = wk()->getOutputFile ("outputLabel");
    EL_RETURN_CHECK("finalize()",event->finishWritingTo( file ));

    cout << "-----------------------------------------------------------" << endl;
    cout << "----------------------- CUTS ------------------------------" << endl;
    cout << "-----------------------------------------------------------" << endl;
    cout << "  " << endl;
    cout << "pass trigger: " << passTrigger << endl;
    cout << "e, mu > 50 GeV: " << passmuonenergy << endl;
    cout << "lv > 200 GeV: " << passWenergy << endl;
    cout << "  " << endl;
    cout << "-----------------------------------------------------------" << endl;
// cleaning up trigger tools
/*
    if( m_trigConfigTool ) {
      delete m_trigConfigTool;
      m_trigConfigTool = 0;
  }
  if( m_trigDecisionTool ) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = 0;
  }*/
if( btag_70 ) {
        delete btag_70;
        btag_70 = 0;
    }

      if( m_jetCleaning ) {
        delete m_jetCleaning;
        m_jetCleaning = 0;
    }
/*
      if( m_trigDecisionTool ) {
        delete m_trigDecisionTool;
        m_trigDecisionTool = 0;
    }
          if( m_trigConfigTool ) {
        delete m_trigConfigTool;
        m_trigConfigTool = 0;
    }
*/
        int m_eventCounter= 0; 
        int EventNumber = 0;         
    int passZnumbercut = 0;
    int passZptcut = 0; 
    int passTrigger = 0;
    int passelectronmass = 0; 
    int passmuonenergy = 0; 
    int passWenergy = 0; 
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode Boildown :: histFinalize ()
{
    return EL::StatusCode::SUCCESS;
}


std::vector<double> Boildown :: METvector (std::vector<TLorentzVector> v) {
    double metx = 0; 
    double mety = 0;
    double summetx = 0; 
    double summety = 0;
    double sumET=0.;
    std::vector<double> u;
    for (unsigned int i = 0; i < v.size(); i++){
        metx += v.at(i).Px();
        sumET += v.at(i).Et();
        summetx += (v.at(i).Px()*v.at(i).Px());
        summety += (v.at(i).Py()*v.at(i).Py());
        //sumET += sqrt(v.at(i).Py()*v.at(i).Py() + v.at(i).Px()*v.at(i).Px());

        mety += v.at(i).Py();
    }
    u.push_back(-metx);
    u.push_back(-mety);
    u.push_back(sqrt(metx*metx + mety*mety));
    u.push_back(sumET);
    //u.push_back(sqrt(summety + summety));
    return u;
}