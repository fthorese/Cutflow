setupATLAS
localSetupROOT
cd Cutflow
cd gsl-2.1
./configure --prefix=pwd/Cutflow/share/gsl/2.1
make
make install
cd ..
rm -rf gsl-2.1
svn co http://newwave.hepforge.org/svn/trunk NewWave
cd NewWave
autoreconf -fvi
./configure --prefix=pwd/Cutflow/share/NewWave/2.1
make
make install
cd ..
rm -rf NewWave
lsetup 'rcsetup Base,2.3.21'
rc find_packages
rc compile
mkdir runs
