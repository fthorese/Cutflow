#ifndef theSearch_WWAnalysis_H
#define theSearch_WWAnalysis_H
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include <EventLoop/Algorithm.h>
#include "TNtuple.h"
#include "NewWave/NewWave.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TH2.h"
#include "fastjet/ClusterSequence.hh"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
using namespace std;
using namespace fastjet;

class WWAnalysis : public EL::Algorithm
{

public:
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();

    WWAnalysis();

    double dR(TLorentzVector U, TLorentzVector V);
    double length(TLorentzVector U);
    std::vector<double> METvector (std::vector<TLorentzVector> v);
    TLorentzVector sum(std::vector<TLorentzVector> v);

    ST::SUSYObjDef_xAOD *m_susyTool;//!

    Trig::TrigDecisionTool *m_trigDecisionTool; //!
    TrigConf::xAODConfigTool *m_trigConfigTool; //!
    
    xAOD::MissingETContainer* met;//!
    xAOD::MissingETAuxContainer* metAux;//!
    JetCleaningTool *m_jetCleaning; //!  

    string outputName;
    int m_eventCounter; //!
    int EventNumber; //!

    TNtuple* leptontruthtup;//!
    TH1F* cutsplot;//!
    TNtuple* truthWW;//!
    TNtuple* myDenoise;//!
    TNtuple* meanMETwcut; //!
    TNtuple* totalMETwcut; //!
    TNtuple* METTruthTree1; //!
    TNtuple* METTruthTree3; //!
    TNtuple* METRefFinal;//!

    TNtuple* PFOMET;//!
    TNtuple* PFOMETwl_denoise;//!
    TNtuple* PFOMETwl_filter;//!
    TNtuple* PFOMETwl_track;//!

    TNtuple* PFOWW;//!
    TNtuple* PFOWWwl_denoise;//!
    TNtuple* PFOWWwl_filter;//!
    TNtuple* PFOWWwl_track;//!

    TNtuple* lepton;//!
    TNtuple* jetsTup;//!

    TNtuple* pileupTup;//!
    TNtuple* mycuts;//!
    TNtuple* chRatio;//!
    TNtuple* PFOWWref;//!
    TNtuple* MET_TruthPileup;//!
    TNtuple* crosssection;//!
    TNtuple* weight;//!
    TNtuple* MET_Truth_NonInt;//!
    TNtuple* MET_Truth_Unknown;//!
    TNtuple* MET_Truth_IntMuons;//!
    TNtuple* MET_TruthPar_Int;//!
    TNtuple* MET_TruthPar_NonInt;//!
    TNtuple* METfinaltrack;//!
    TNtuple* METfinalclus;//!

ClassDef(WWAnalysis, 1);
};

#endif
