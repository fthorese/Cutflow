//#include "theSearch/CutsInxAOD.h"
#include <Cutflow/Boildown.h>
#include <Cutflow/ObjDef.h>
#include <Cutflow/MyMETAnalysis.h>
#include <Cutflow/AnalysisOfPFOS.h>
#include <Cutflow/WWAnalysis.h>
#include <Cutflow/WWboildown.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif
#ifdef __CINT__

#pragma link C++ class Boildown+;
#pragma link C++ class ObjDef+;
#pragma link C++ class MyMETAnalysis+;
#pragma link C++ class AnalysisOfPFOS+;
#pragma link C++ class WWAnalysis+;
#pragma link C++ class WWboildown+;

#endif
