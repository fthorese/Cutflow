#ifndef theSearch_Boildown_H
#define theSearch_Boildown_H

#include <EventLoop/Algorithm.h>
#include "TNtuple.h"
#include "NewWave/NewWave.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TH1.h"
#include "xAODBTagging/BTaggingAuxContainer.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

using namespace std;

class Boildown : public EL::Algorithm
{
    public:
        // these are the functions inherited from Algorithm
        virtual EL::StatusCode setupJob (EL::Job& job);
        virtual EL::StatusCode fileExecute ();
        virtual EL::StatusCode histInitialize ();
        virtual EL::StatusCode changeInput (bool firstFile);
        virtual EL::StatusCode initialize ();
        virtual EL::StatusCode execute ();
        virtual EL::StatusCode postExecute ();
        virtual EL::StatusCode finalize ();
        virtual EL::StatusCode histFinalize ();
        Boildown();
    std::vector<double> METvector (std::vector<TLorentzVector> v);
    BTaggingSelectionTool* btag_70 = nullptr; //!
        string outputName; 
        TH1F* cutsplot;//!
        int m_eventCounter; //!
        int EventNumber; //!
        TNtuple* e_tree;//!
                TNtuple* mu_tree;//!

    int passZnumbercut = 0; //!
    int passZptcut = 0; //!
    int passTrigger = 0; //!
    int passelectronmass = 0; //!
    int passmuonenergy = 0; //!
    int passWenergy = 0; //!

        //GoodRunsListSelectionTool *m_grl; //!
        JetCleaningTool *m_jetCleaning; //!
        Trig::TrigDecisionTool *m_trigDecisionTool; //!
        TrigConf::xAODConfigTool *m_trigConfigTool; //!
        //vector<TLorentzVector> Z_from_muons; //!
        //vector<TLorentzVector> Z_from_electrons; //! 
        ClassDef(Boildown, 1);
};

#endif
