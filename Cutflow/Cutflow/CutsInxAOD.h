#ifndef theSearch_CutsInxAOD_H
#define theSearch_CutsInxAOD_H
#include "TObject.h"
#include "TLorentzVector.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include <vector>
#include "xAODJet/JetContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJet.h"

using namespace std;
class CutsInxAOD  { // : public TObject

    private:
        vector<TLorentzVector> mu_vector_neg; //!
        vector<TLorentzVector> mu_vector_pos; //!
        vector<TLorentzVector> e_vector_neg; //!
        vector<TLorentzVector> e_vector_pos; //!
        vector<TLorentzVector> jet_vector; //!

        vector<TLorentzVector> Z_from_jets; //!
        vector<TLorentzVector> Z_from_muons; //!
        vector<TLorentzVector> Z_from_electrons; //!

        vector<TLorentzVector> chPFO; //!
        vector<TLorentzVector> neuPFO; //!


    public:

        // constructor
        CutsInxAOD(double MET);
        double missingEt;//!
        // functions
        vector<TLorentzVector> analyzeMuons(const xAOD::MuonContainer* muons,double cutpt, double cutmass, bool isolated, double etacut);
        vector<TLorentzVector> analyzeJets(const xAOD::JetContainer* jets,JetCleaningTool *m_jetCleaning,double cutpt, double cutmass);
        vector<TLorentzVector> analyzeTauJets(const xAOD::TauJetContainer* taujets, double cutpt, double cutmass);
        vector<TLorentzVector> analyzeFatjets(const xAOD::JetContainer* fatjets, double cutpt, double cutmass);

        vector<TLorentzVector> analyzeElectrons(const xAOD::ElectronContainer* electrons, double cutpt, double cutmass, bool isolated, double etacut);
        //double analyzeZbosonsFromElectrons(const xAOD::ElectronContainer* electrons);
        //double analyzeZbosonsFromMuons(const xAOD::MuonContainer* muons);
        //double analyzeZbosonsFromJets(const xAOD::JetContainer* jets,JetCleaningTool *m_jetCleaning, double cutpt, double cutmass);

        //void printZbosonsFromElectrons();
        //void printZbosonsFromMuons();

        void cutNeuPFO(const xAOD::PFOContainer* neuPFOs);
        void cutChPFO(const xAOD::PFOContainer* chPFOs);

        // vectors
        vector<TLorentzVector> getMuonNeg();
        vector<TLorentzVector> getMuonPos();
        vector<TLorentzVector> getElectronNeg();
        vector<TLorentzVector> getElectronPos();
        vector<TLorentzVector> getJets();

        // variables
        int muon_counter; //!
        double muon_count; //!
        double muon_pt; //!
        double muon_eta; //!
        double muon_phi; //!
        double muon_charge;//!

        vector<TLorentzVector> getZbosonsFromElectrons();
        vector<TLorentzVector> getZbosonsFromMuons();
        vector<TLorentzVector> getZbosonsFromJets();
        vector<TLorentzVector> getNeuPFO();
        vector<TLorentzVector> getChPFO();

};

#endif