// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
// EDM includes:
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include <vector>
#include "TLorentzVector.h"
#include "EventLoop/OutputStream.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include "Cutflow/MyMETAnalysis.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "TH2.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "fastjet/ClusterSequence.hh"
#include "Cutflow/WaveletAnalysis.h"
#include "Cutflow/CutsInxAOD.h"
#include "METUtilities/METMaker.h"
#include "AsgTools/ToolHandle.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJet.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "METUtilities/METSystematicsTool.h"
#include "METUtilities/METMaker.h"
#include "METUtilities/CutsMETMaker.h"
using namespace std;
using namespace fastjet;

// this is needed to distribute the algorithm to the workers
ClassImp(MyMETAnalysis)
/*
* This class is for making NTuples of MET values.
*/

// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
do {                                                        \
  if( ! EXP.isSuccess() ) {                                 \
   Error( CONTEXT,                                        \
    XAOD_MESSAGE( "Failed to execute: %s" ),            \
                #EXP );                                     \
    return EL::StatusCode::FAILURE;                     \
}                                                       \
} while( false )


/*MyMETAnalysis :: MyMETAnalysis (double filterCut, double denoiseCut,double timesRatio)
{
     filtercut = filterCut;
     denoisecut = denoiseCut;
     timesratio = timesRatio;
}*/
MyMETAnalysis :: MyMETAnalysis ()
{
}


EL::StatusCode MyMETAnalysis :: setupJob (EL::Job& job)
{
    job.useXAOD ();

    EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: histInitialize ()
{
    cout << "outputName = " << outputName << endl;

    TFile *outputFile = wk()->getOutputFile (outputName);

    pileupTup = new TNtuple("Pileup", "Pileup","mu:none:none1:none2");
    pileupTup->SetDirectory(outputFile);

    // METRefFinal = new TNtuple("MET_Ref_Final", "MET Ref Final","px:py:met:sumet");
    // METRefFinal->SetDirectory(outputFile);

    // METfinaltrack = new TNtuple("MET_Final_Track", "MET_Final_Track", "px:py:met:sumet");
    // METfinalclus = new TNtuple("MET_Final_Clus", "MET_Final_Clus", "px:py:met:sumet");
    // METfinalclus->SetDirectory(outputFile);
    // METfinaltrack->SetDirectory(outputFile);
    // METTruthTree1 = new TNtuple("MET_Truth_Int", "Int","px:py:met:sumet");
    // METTruthTree3 = new TNtuple("MET_Truth_Int_and_IntMuons", "Int_and_IntMuons","px:py:met:sumet");
    // MET_Truth_NonInt = new TNtuple("MET_Truth_NonInt", "MET_Truth_NonInt","px:py:met:sumet");
    // MET_Truth_Unknown = new TNtuple("MET_Truth_Unknown", "MET_Truth_Unknown","px:py:met:sumet");
    // MET_Truth_IntMuons= new TNtuple("MET_Truth_IntMuons", "MET_Truth_IntMuons","px:py:met:sumet");

    // MET_TruthPileup = new TNtuple("MET_TruthPileup", "MET_TruthPileup","px:py:met:sumet");
    // MET_TruthPileup->SetDirectory(outputFile);
    MET_TruthPar_Int = new TNtuple("MET_TruthPar_Int", "MET_TruthPar_Int","px:py:met:sumet");
    // MET_TruthPar_NonInt = new TNtuple("MET_TruthPar_NonInt", "MET_TruthPar_NonInt","px:py:met:sumet");
    // METTruthTree1->SetDirectory (outputFile);
    // METTruthTree3->SetDirectory (outputFile);
    // MET_Truth_NonInt->SetDirectory(outputFile);
    // MET_Truth_Unknown->SetDirectory(outputFile);
    // MET_Truth_IntMuons->SetDirectory(outputFile);
    MET_TruthPar_Int->SetDirectory(outputFile);
    // MET_TruthPar_NonInt->SetDirectory(outputFile);
    // mycutspt = new TNtuple("mycutspt", "mycuts", "mycuts:mycuts1:mycuts2:mycuts3");

    for (int i = 0; i < 49; i++){
        myalphas.push_back(new TNtuple(("#alpha at scale j = " + to_string(i)).c_str(),("#alpha at scale j = " + to_string(i)).c_str(),"alpha"));
        wk()->addOutput(myalphas[i]);       
    }

    // mycuts = new TNtuple("mycuts", "mycuts", "mycuts:mycuts1:mycuts2:mycuts3");
    // myDenoise = new TNtuple("Ratio_Denoise", "Ratio_Denoise","px:py:met:sumet");
    // chRatio = new TNtuple("Charged Ratio", "Charged_Ratio","px:py:met:sumet");
    // mycutspt->SetDirectory(outputFile);
    // chptRatio = new TNtuple("Charged p_{T} Ratio", "Charged_Ratio","px:py:met:sumet");
    // chptRatio->SetDirectory(outputFile);

    // myDenoise->SetDirectory(outputFile);
    // mycuts->SetDirectory(outputFile);
    // chRatio->SetDirectory(outputFile);
    // PFOch = new TNtuple("Charged PFOs", "Particle Flow MET", "px:py:met:sumet");
    // PFOneu = new TNtuple("Neutral PFOs", "Particle Flow MET", "px:py:met:sumet");
    // PFOch->SetDirectory (outputFile);
    // PFOneu->SetDirectory (outputFile);

    //crosssection = new TNtuple("Cross Section", "Particle Flow MET", "cross");

    // PFOMETwl_track = new TNtuple("Track Scaling", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_filter = new TNtuple("Track Filtering", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    PFOMET = new TNtuple("PFOs", "Particle Flow MET", "px:py:met:sumet");
    // PFOMETwl_denoise = new TNtuple("Flat Denoising", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_mydenoise = new TNtuple("Dynamic Flat Denoising", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_ratiodenoise_filter = new TNtuple("Ratio Denoise + Filter", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_ratiodenoise_track = new TNtuple("Ratio Denoise + Track", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_ratiodenoisept_filter = new TNtuple("Pt Ratio Denoise + Filter", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_ratiodenoisept_track = new TNtuple("Pt Ratio Denoise + Track", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_denoise_filter = new TNtuple("Denoise + Track Filtering", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_denoise_track = new TNtuple("Denoise + Track Scaling", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    // PFOMETwl_track->SetDirectory (outputFile);
    // PFOMETwl_filter->SetDirectory (outputFile);
    PFOMET_entropi = new TNtuple("Entropi", "Particle Flow MET", "px:py:met:sumet");

    PFOMET_entropi->SetDirectory(outputFile);
    PFOMET->SetDirectory (outputFile);
    // PFOMETwl_denoise->SetDirectory (outputFile);
    // PFOMETwl_mydenoise->SetDirectory (outputFile);
    // PFOMETwl_ratiodenoise_filter->SetDirectory (outputFile);
    // PFOMETwl_ratiodenoise_track->SetDirectory(outputFile);
    // PFOMETwl_denoise_filter->SetDirectory(outputFile);
    // PFOMETwl_denoise_track->SetDirectory(outputFile);
    // PFOMETwl_ratiodenoisept_filter->SetDirectory (outputFile);
    // PFOMETwl_ratiodenoisept_track->SetDirectory(outputFile);
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: fileExecute ()
{
    cout << "file execute"  << endl;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: changeInput (bool firstFile)
{
    cout << "changeInput" << endl;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: initialize ()
{
    gErrorIgnoreLevel = kError;

    // m_susyTool = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD" ); //
    // m_susyTool->setProperty("DataSource",ST::SUSYObjDef_xAOD::FullSim); 
    // //ST::ISUSYObjDef_xAODTool::FullSim
    // m_susyTool->msg().setLevel( MSG::ERROR ); 

   //EL_RETURN_CHECK("initialize()",m_susyTool->initialize());

    // initialize and configure the jet cleaning tool
    m_jetCleaning = new JetCleaningTool("JetCleaning");
    m_jetCleaning->msg().setLevel( MSG::ERROR ); 
    EL_RETURN_CHECK("initialize()", m_jetCleaning->setProperty( "CutLevel", "LooseBad"));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->setProperty("DoUgly", false));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->initialize());

    m_eventCounter = 0;
    xAOD::TEvent* event = wk()->xaodEvent();
    EventNumber = 0;
    // as a check, let's see the number of events in our xAOD
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: execute ()
{
    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- EVENT INFORMATION -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    const xAOD::EventInfo* eventInfo = 0;
    xAOD::TEvent* event = wk()->xaodEvent();

    

    EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

    // print every 25 events, so we know where we are:
    if( (m_eventCounter % 1) ==0 ) //
        m_eventCounter++;

    // check if the event is data or MC
    // (many tools are applied either to data or MC)
    //bool isMC = false;
    // check if the event is MC
    //if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    //    isMC = true; // can do something with this later
        //cout << "This is MC" << endl;
    //}
    EventNumber++;
    cout << "MyMETAnalysis::execute()  INFO    EventNumber = " << EventNumber << endl;

    double N_pileup = eventInfo->averageInteractionsPerCrossing();
    pileupTup->Fill(N_pileup,0,0,0);

    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- CONTAINERS -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    Info("execute()", "Getting containers");

    const xAOD::PFOContainer* chPFOs = 0;
    const xAOD::PFOContainer* neuPFOs = 0;
   // const xAOD::MissingETContainer* METcontainer = 0;
    const xAOD::VertexContainer* PVs = 0;
    const xAOD::TrackParticleContainer* TrackParticles = 0;
    // const xAOD::TauJetContainer* taujets = 0;
    // const xAOD::JetContainer* jets = 0;
    // const xAOD::MuonContainer* muons = 0;
    // const xAOD::ElectronContainer* electrons = 0;
    // const xAOD::PhotonContainer* gammas = 0;
    const xAOD::TruthParticleContainer* TruthParticles = 0;
    const xAOD::TruthEventContainer* xTruthEventContainer = 0;
//    const xAOD::TruthPileupEventContainer* xTruthPileupEventContainer = 0;

    //EL_RETURN_CHECK( "execute()", event->retrieve( xTruthPileupEventContainer, "TruthPileupEvents"));
    EL_RETURN_CHECK( "execute()", event->retrieve( xTruthEventContainer, "TruthEvents"));
    EL_RETURN_CHECK("execute()", event->retrieve( PVs, "PrimaryVertices" ) );
    // EL_RETURN_CHECK("execute()", event->retrieve( jets, "AntiKt4EMTopoJets" ));
    // EL_RETURN_CHECK("execute()", event->retrieve( taujets,  "TauJets" ) );
    // EL_RETURN_CHECK("execute()", event->retrieve( METcontainer, "MET_Truth"));
    EL_RETURN_CHECK("execute()", event->retrieve( chPFOs,  "JetETMissChargedParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( neuPFOs, "JetETMissNeutralParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( TrackParticles, "InDetTrackParticles" ) );
    // EL_RETURN_CHECK("execute()", event->retrieve( muons, "Muons" ));
    // EL_RETURN_CHECK("execute()", event->retrieve( electrons, "Electrons" ));
    // EL_RETURN_CHECK("execute()", event->retrieve(gammas, "Photons"));
    EL_RETURN_CHECK("execute()", event->retrieve(TruthParticles, "TruthParticles"));

    // Info("execute()", "Making MET Ref Final");

    // double metEt =0; 
    // double px = 0;
    // double py = 0;
    // double sumet = 0;
    // double metPhi = 0;


    // Info("execute()", "Making MET Ref Final");

    // met = new xAOD::MissingETContainer(); 
    // metAux = new xAOD::MissingETAuxContainer();
    // met->setStore(metAux);
    // m_susyTool->GetMET( *met, jets, electrons, muons, gammas, taujets, true, true, 0);
    //     // MET SUSY
    // xAOD::MissingETContainer::const_iterator met_it = met->find("Final");

    // double metEt =(*met_it)->met(); 
    // double px = (*met_it)->mpx();
    // double py = (*met_it)->mpy();
    // double sumet = (*met_it)->sumet();
    // double metPhi = (*met_it)->phi();

    Info("execute()", "Making Truth (Int/NonInt) MET.  Truth length = %i", TruthParticles->size());

    vector<TLorentzVector> truthInt;
    vector<TLorentzVector> truthNonInt;
    vector<TLorentzVector> truthPileup;
    int nParthg = 0;
    // pileup
    std::vector<TLorentzVector> pileup_vector;  
    xAOD::TruthEventContainer::const_iterator itr;
    for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
        const
        xAOD::TruthVertex* nVert = (*itr)->truthVertex(0);
        // cout << "cross section = " << (*itr)->crossSection() << endl;
        // cout << "weight = " << (*itr)->weights().at(0) << endl;
        // cout << "len(weight) = " << (*itr)->weights().size() << endl;

        int nvertex = (*itr)->nTruthVertices();
        int nParth = (*itr)->nTruthParticles();
        nParthg = (*itr)->nTruthParticles();
        const xAOD::TruthParticle* nPart = (*itr)->truthParticle(0);
        // cout << "HEEEJ TRUTH " << nPart->pt() << ", " << nParth << endl;
        // cout << "nTruthVertices = " << nvertex << endl;

        for (int i = 0; i < nParth; i++) {
            const xAOD::TruthParticle* truth = (*itr)->truthParticle(i);

            TLorentzVector ps = TLorentzVector();
            ps.SetPxPyPzE(truth->p4().Px(), truth->p4().Py(), truth->p4().Pz(), truth->p4().E());
            if (abs(truth->eta()) < 10 && truth->status() == 1 && truth->barcode() < 100000 ){
                if (abs(truth->pdgId()) != 12 && abs(truth->pdgId()) != 14 && abs(truth->pdgId()) != 16) {
                    truthInt.push_back(ps);

                }              
                else {
                    truthNonInt.push_back(ps);
                }
            }
            if (abs(truth->eta()) < 10 && truth->status() == 1 && truth->barcode() > 100000){
                truthPileup.push_back(ps);
            }
        }
    }
//    MET_TruthPileup->Fill(METvector(truthPileup)[0], METvector(truthPileup)[1], METvector(truthPileup)[2], METvector(truthPileup)[3]);
    // bool goodevt = false;
    // bool neutrino = false;
    // bool lepton = false;


    // if (metEt > 50000) {
    //     neutrino = true;
    // }

    // METRefFinal->Fill(px,py,metEt,sumet);

    // vector<TLorentzVector> chc1;
    // vector<TLorentzVector> neuc1;
    // vector<TLorentzVector> PFOc1;

// matching tracks with PVs
    vector<TLorentzVector> track_PV; 
    vector<TLorentzVector> track_All; 
    vector<TLorentzVector> track_NonPV;
    Info("execute()", "Matching tracks with charged PFOs");

    for (const xAOD::TrackParticle* track : *TrackParticles) {
        bool match = true;
        xAOD::VertexContainer::const_iterator PV_itr = PVs->begin();
        xAOD::VertexContainer::const_iterator PV_end = PVs->end();
        PV_itr++;

        for ( ; PV_itr != PV_end; ++PV_itr) {
            for (unsigned i = 0; i < (*PV_itr)->nTrackParticles(); i++) {
                if (track == (*PV_itr)->trackParticle(i)) { match = false; break; }
            }
            if (match == false) { break; }
        }
        if (abs(track->eta()) < 10) {
            TLorentzVector ps = TLorentzVector();
            ps.SetPxPyPzE(track->p4().Px(), track->p4().Py(), track->p4().Pz(), track->p4().E());
            ps.SetPtEtaPhiM(track->pt(), track->eta(), track->phi(), track->m());

            if (match) { 
                track_PV.push_back( ps );
            }
            else { 
                track_NonPV.push_back( ps ); 
            }
            track_All.push_back(ps); }
        }
// -- PFlow objects.
        vector<TLorentzVector> chPFO_PV; 
        vector<TLorentzVector> chPFO_NonPV;
        Info("execute()", "Sorting PFOs");

// Charged
        for (unsigned int i = 0; i < chPFOs->size(); i++) {
            bool match = false;
            if (abs(chPFOs->at(i)->eta()) < 10) {
                TLorentzVector particle =  TLorentzVector();
                particle.SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(),chPFOs->at(i)->p4().Pz(),chPFOs->at(i)->p4().E());
                particle.SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());

                for (size_t i = 0; i < track_PV.size(); i++) {
                    if (dR(particle, track_PV.at(i)) < 0.005) { match = true; break; }
                }
      //          if (abs(chPFOs->at(i)->eta()) < 10 ) { chc1.push_back(particle); PFOc1.push_back(particle);}
                if (match) { chPFO_PV   .push_back( particle ); }
                else       { chPFO_NonPV.push_back( particle ); }
                chPFO.push_back(particle);
                PFO.push_back(particle);
            }
        }
// Neutral
        for (unsigned int i = 0; i < neuPFOs->size(); i++) {
            if (abs(neuPFOs->at(i)->eta()) < 10 ) {

                TLorentzVector particle =  TLorentzVector();
                particle.SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(),neuPFOs->at(i)->p4().Pz(),neuPFOs->at(i)->p4().E());
                particle.SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());

                neuPFO.push_back(particle);
                PFO.push_back(particle);
                //if (neuPFOs->at(i)->eta() < 10 && neuPFOs->at(i)->eta() > -10) {neuc1.push_back(particle);PFOc1.push_back(particle);}
            }
        }



    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ MY METHOD --------------------------------------------
    //-------------------------------------------------------------------------------------------------------

        // double mydenoisecutneupt = 0.0;

        // double neuPFO_PV_pt = 0.1;
        // double neuPFO_PV_size = 0.1*neuPFO.size();
        // vector<TLorentzVector> neuPFO_PV_temp;
        // vector<TLorentzVector> neuPFO_PV_temp_pt;

        // vector<TLorentzVector> chPFO_PV_temp;
        Info("execute()", "Wavelet Methods");

        // My Wavelet Method
        //cout << "ch ratio " << (float)chPFO_PV.size()/(float)chPFO.size() << endl;
        // chRatio->Fill((float)chPFO_PV.size()/(float)chPFO.size(),0,0,0);
        // double ptRatio = METvector(chPFO_PV)[3]/METvector(chPFO)[3];

        // chptRatio->Fill(ptRatio,0,0,0);

        // while (neuPFO_PV_pt/METvector(neuPFO)[3] > ptRatio*timesratio && mydenoisecutneupt < 10000) {
        //     neuPFO_PV_temp_pt = WaveletAnalysis(64,4.9,N_pileup, neuPFO).denoise(mydenoisecutneupt).particles(); 
        //     neuPFO_PV_pt = METvector(neuPFO_PV_temp)[3];
        //     mydenoisecutneupt += 100;
        // }
        // mycutspt->Fill(mydenoisecutneupt,0,0,0);    
        
        // double mydenoisecutneu = 0.0;

        // while ((float)neuPFO_PV_size/(float)neuPFO.size() > (float)chPFO_PV.size()/(float)chPFO.size()*timesratio && mydenoisecutneu < 10000) {
        //     neuPFO_PV_temp = WaveletAnalysis(64,4.9,N_pileup, neuPFO).denoise(mydenoisecutneu).particles(); 
        //     neuPFO_PV_size = neuPFO_PV_temp.size();
        //     mydenoisecutneu += 100;
        // }
        // mycuts->Fill(mydenoisecutneu,0,0,0);  


        // vector<TLorentzVector> PFO_PV_temp;
        // for (unsigned int i = 0; i < neuPFO_PV_temp.size(); i++){
        //     PFO_PV_temp.push_back(neuPFO_PV_temp.at(i));
        // }
        // for (unsigned int i = 0; i < chPFO_PV.size(); i++){
        //     PFO_PV_temp.push_back(chPFO_PV.at(i));
        // }

    //-------------------------------------------------------------------------------------------------------
    //---------------------------------------------- WAVELET --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
//        vector<TLorentzVector> entropi_temp;

        WaveletAnalysis wlana = WaveletAnalysis(64,3.2,N_pileup, PFO);
        vector<TLorentzVector> entropi_temp = wlana.entropy(track_PV, PFO).particles();
        // std::vector<double> alphas = wlana.getAlphas();

        // for (int i = 0; i < 49; i++){
        //     myalphas[i]->Fill(alphas[i]);
        // }

        //vector<TLorentzVector> entropi_temp;
        // track filter to be combined with neutral
    //     vector<TLorentzVector> chMETwl_filter_temp ;//=     WaveletAnalysis(64,4.9,N_pileup, chPFO).trackfilter(chPFO, chPFO_PV, filtercut).particles();
    //     vector<TLorentzVector> chMETwl_track_temp ;//=     WaveletAnalysis(64,4.9,N_pileup, chPFO).trackscaling(track_All,track_PV).particles();

    //     // mydenoise of neutrals
    //     vector<TLorentzVector> neuMETwl_mydenoise_temp ;//= WaveletAnalysis(64,4.9,N_pileup, neuPFO).mydenoise().particles();
    //     // denoise of neutrals
    //     vector<TLorentzVector> neuMETwl_denoise_temp ;//=   WaveletAnalysis(64,4.9,N_pileup, neuPFO).denoise(denoisecut).particles();
    //     // filtering of all
    //     vector<TLorentzVector> METwl_filter_temp;// =       WaveletAnalysis(64,4.9,N_pileup, PFO).trackfilter(PFO, chPFO_PV, filtercut).particles();
    //     // trackscaling of all
    //     vector<TLorentzVector> METwl_track_temp;// =       WaveletAnalysis(64,4.9,N_pileup, PFO).trackscaling(track_All, track_PV).particles();
    //     // mydenoise of all
    //     vector<TLorentzVector> METwl_mydenoise_temp;// =    WaveletAnalysis(64,4.9,N_pileup, PFO).mydenoise().particles();
    //     // denoise of all
    //     vector<TLorentzVector> METwl_denoise_temp;// =      WaveletAnalysis(64,4.9,N_pileup, PFO).denoise(denoisecut).particles();

    // std::vector<TLorentzVector> METwl_ratiodenoisept_filter_temp;
    // std::vector<TLorentzVector> METwl_ratiodenoisept_track_temp;
    // std::vector<TLorentzVector> METwl_ratiodenoise_filter_temp;
    // std::vector<TLorentzVector> METwl_ratiodenoise_track_temp;
    // std::vector<TLorentzVector> METwl_denoise_filter_temp;
    // std::vector<TLorentzVector> METwl_denoise_track_temp;

    // for (TLorentzVector vec : neuMETwl_denoise_temp) {METwl_denoise_filter_temp.push_back(vec);METwl_denoise_track_temp.push_back(vec);}
    // for (TLorentzVector vec : chMETwl_filter_temp)   {METwl_denoise_filter_temp.push_back(vec);METwl_ratiodenoise_filter_temp.push_back(vec);METwl_ratiodenoisept_filter_temp.push_back(vec);}
    // for (TLorentzVector vec : chMETwl_track_temp)    {METwl_denoise_track_temp.push_back(vec); METwl_ratiodenoise_track_temp.push_back(vec);METwl_ratiodenoisept_track_temp.push_back(vec);}
    // for (TLorentzVector vec : neuPFO_PV_temp)        {METwl_ratiodenoise_filter_temp.push_back(vec); METwl_ratiodenoise_track_temp.push_back(vec);}
    // for (TLorentzVector vec : neuPFO_PV_temp_pt)     {METwl_ratiodenoisept_filter_temp.push_back(vec); METwl_ratiodenoisept_track_temp.push_back(vec);}

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ MET --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    Info("execute()", "Filling NTuples");
    PFOMET_entropi->Fill(METvector(entropi_temp)[0], METvector(entropi_temp)[1], METvector(entropi_temp)[2], METvector(entropi_temp)[3]);
    // myDenoise->    Fill(METvector(PFO_PV_temp)[0], METvector(PFO_PV_temp)[1], METvector(PFO_PV_temp)[2], METvector(PFO_PV_temp)[3]);
    // PFOMETwl_filter->  Fill(METvector(METwl_filter_temp)[0],METvector(METwl_filter_temp)[1],METvector(METwl_filter_temp)[2],METvector(METwl_filter_temp)[3]);
    // PFOMETwl_track->  Fill(METvector(METwl_track_temp)[0],METvector(METwl_track_temp)[1],METvector(METwl_track_temp)[2],METvector(METwl_track_temp)[3]);
    // PFOMETwl_denoise->  Fill(METvector(METwl_denoise_temp)[0],METvector(METwl_denoise_temp)[1],METvector(METwl_denoise_temp)[2],METvector(METwl_denoise_temp)[3]);
    // PFOMETwl_mydenoise->Fill(METvector(METwl_mydenoise_temp)[0],METvector(METwl_mydenoise_temp)[1],METvector(METwl_mydenoise_temp)[2],METvector(METwl_mydenoise_temp)[3]);
    // PFOMETwl_ratiodenoise_filter->   Fill(METvector(METwl_ratiodenoise_filter_temp)[0],METvector(METwl_ratiodenoise_filter_temp)[1],METvector(METwl_ratiodenoise_filter_temp)[2],METvector(METwl_ratiodenoise_filter_temp)[3]);
    // PFOMETwl_ratiodenoise_track-> Fill(METvector(METwl_ratiodenoise_track_temp)[0],METvector(METwl_ratiodenoise_track_temp)[1],METvector(METwl_ratiodenoise_track_temp)[2],METvector(METwl_ratiodenoise_track_temp)[3]);
    // PFOMETwl_ratiodenoisept_filter->   Fill(METvector(METwl_ratiodenoisept_filter_temp)[0],METvector(METwl_ratiodenoisept_filter_temp)[1],METvector(METwl_ratiodenoisept_filter_temp)[2],METvector(METwl_ratiodenoisept_filter_temp)[3]);
    // PFOMETwl_ratiodenoisept_track-> Fill(METvector(METwl_ratiodenoisept_track_temp)[0],METvector(METwl_ratiodenoisept_track_temp)[1],METvector(METwl_ratiodenoisept_track_temp)[2],METvector(METwl_ratiodenoisept_track_temp)[3]);

    // PFOMETwl_denoise_filter->   Fill(METvector(METwl_denoise_filter_temp)[0],METvector(METwl_denoise_filter_temp)[1],METvector(METwl_denoise_filter_temp)[2],METvector(METwl_denoise_filter_temp)[3]);
    // PFOMETwl_denoise_track-> Fill(METvector(METwl_denoise_track_temp)[0],METvector(METwl_denoise_track_temp)[1],METvector(METwl_denoise_track_temp)[2],METvector(METwl_denoise_track_temp)[3]);

    PFOMET->Fill(METvector(PFO)[0],METvector(PFO)[1],METvector(PFO)[2],METvector(PFO)[3]);
//    PFOch->Fill(METvector(chPFO)[0],METvector(chPFO)[1],METvector(chPFO)[2],METvector(chPFO)[3]);
//    PFOneu->Fill(METvector(neuPFO)[0],METvector(neuPFO)[1],METvector(neuPFO)[2],METvector(neuPFO)[3]);

    // METTruthTree1->Fill((*METcontainer)["Int"]->mpx(),(*METcontainer)["Int"]->mpy(),(*METcontainer)["Int"]->met(),(*METcontainer)["Int"]->sumet());
    // METTruthTree3->Fill((*METcontainer)["Int"]->mpx() + (*METcontainer)["IntMuons"]->mpx(),(*METcontainer)["Int"]->mpy() + (*METcontainer)["IntMuons"]->mpy(),(*METcontainer)["Int"]->met() + (*METcontainer)["IntMuons"]->met(),(*METcontainer)["Int"]->sumet() + (*METcontainer)["IntMuons"]->sumet());
//        METTruthTree1->Fill(METvector(truthNonInt)[0],METvector(truthNonInt)[1],METvector(truthNonInt)[2],METvector(truthNonInt)[3]);


    // MET_Truth_NonInt->Fill((*METcontainer).at(0)->mpx(), (*METcontainer).at(0)->mpy(),(*METcontainer).at(0)->met(), (*METcontainer).at(0)->sumet());
    // MET_Truth_Unknown->Fill((*METcontainer).at(2)->mpx(), (*METcontainer).at(2)->mpy(),(*METcontainer).at(2)->met(), (*METcontainer).at(2)->sumet());
    // MET_Truth_IntMuons->Fill((*METcontainer).at(3)->mpx(), (*METcontainer).at(3)->mpy(),(*METcontainer).at(3)->met(), (*METcontainer).at(3)->sumet());
    MET_TruthPar_Int->Fill(METvector(truthInt)[0], METvector(truthInt)[1],METvector(truthInt)[2], METvector(truthInt)[3]);
//    MET_TruthPar_NonInt->Fill(METvector(truthNonInt)[0], METvector(truthNonInt)[1],METvector(truthNonInt)[2], METvector(truthNonInt)[3]);
    //METfinaltrack->Fill((*newMetContainer)["FinalTrk"]->mpx(),(*newMetContainer)["FinalTrk"]->mpy(), (*newMetContainer)["FinalTrk"]->met(),(*newMetContainer)["FinalTrk"]->sumet());
    //METfinalclus->Fill((*newMetContainer)["FinalClus"]->mpx(),(*newMetContainer)["FinalClus"]->mpy(), (*newMetContainer)["FinalClus"]->met(),(*newMetContainer)["FinalClus"]->sumet());

    // char buf[256];
    // char pattern[] = "|  %13s  |  %1.3f  |  %1.3f  |  %1.3f  |  %1.3f  ";     
    // cout << "------------------      px     |     py      |     met     |     sumet     " << endl;
    // cout << "------- From Truth Particles within eta 4.9" << endl;
    // sprintf(buf, pattern, "TruthPar Int   ", METvector(truthInt)[0], METvector(truthInt)[1],METvector(truthInt)[2], METvector(truthInt)[3]);
    // cout << std::scientific << buf << endl;
    // sprintf(buf, pattern, "TruthPar NonInt", METvector(truthNonInt)[0], METvector(truthNonInt)[1],METvector(truthNonInt)[2], METvector(truthNonInt)[3]);
    // cout << std::scientific << buf << endl;
    // cout << "------- From SUSY " << endl;
    // sprintf(buf, pattern, "METRefFinal    ",px, py,metEt, sumet);
    // cout << std::scientific << buf << endl;
    // cout << "------- From METMaker " << endl;
    //sprintf(buf, pattern, "FinalTrk    ",(*newMetContainer)["FinalTrk"]->mpx(), (*newMetContainer)["FinalTrk"]->mpy(),(*newMetContainer)["FinalTrk"]->met(), (*newMetContainer)["FinalTrk"]->sumet());
    //cout << std::scientific << buf << endl;   
    //sprintf(buf, pattern, "FinalClus    ",(*newMetContainer)["FinalClus"]->mpx(), (*newMetContainer)["FinalClus"]->mpy(),(*newMetContainer)["FinalClus"]->met(), (*newMetContainer)["FinalClus"]->sumet());
    //cout << std::scientific << buf << endl;   
    // cout << "------- From PFOs and Wavelets" << endl;
    // sprintf(buf, pattern, "PFO            ",METvector(PFO)[0], METvector(PFO)[1],METvector(PFO)[2], METvector(PFO)[3]);
    // cout << std::scientific << buf << endl;
    // sprintf(buf, pattern, "PFOwl          ",METvector(METwl_denoise_temp)[0], METvector(METwl_denoise_temp)[1],METvector(METwl_denoise_temp)[2], METvector(METwl_denoise_temp)[3]);
    // cout << std::scientific << buf << endl;
    // cout << "------- From METTruthContainer" << endl;
    // sprintf(buf, pattern,  "NonInt         ", (*METcontainer).at(0)->mpx(), (*METcontainer).at(0)->mpy(),(*METcontainer).at(0)->met(), (*METcontainer).at(0)->sumet());
    // cout << std::scientific << buf << endl;

    // sprintf(buf, pattern,  "Int            ", (*METcontainer).at(1)->mpx(), (*METcontainer).at(1)->mpy(),(*METcontainer).at(1)->met(), (*METcontainer).at(1)->sumet());
    // cout << std::scientific << buf << endl;

    // sprintf(buf, pattern,  "?              ", (*METcontainer).at(2)->mpx(), (*METcontainer).at(2)->mpy(),(*METcontainer).at(2)->met(), (*METcontainer).at(2)->sumet());
    // cout << std::scientific << buf << endl;

    // sprintf(buf, pattern,  "IntMuons       ", (*METcontainer).at(3)->mpx(), (*METcontainer).at(3)->mpy(),(*METcontainer).at(3)->met(), (*METcontainer).at(3)->sumet());
    // cout << std::scientific << buf << endl;

    // sprintf(buf, pattern, "Int + Muons     ", (*METcontainer).at(1)->mpx()+(*METcontainer).at(3)->mpx(), (*METcontainer).at(1)->mpy()+(*METcontainer).at(3)->mpy(), (*METcontainer).at(1)->met()+(*METcontainer).at(3)->met(), (*METcontainer).at(1)->sumet()+(*METcontainer).at(3)->sumet());
    // cout << std::scientific << buf << endl;
    // sprintf(buf, pattern, "PileupTruth     ",METvector(truthPileup)[0], METvector(truthPileup)[1],METvector(truthPileup)[2], METvector(truthPileup)[3]);
    // cout << std::scientific << buf << endl;


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: postExecute ()
{
    //cout << "postExecute" << endl;
    chPFO.clear();
    neuPFO.clear();
    PFO.clear();

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: finalize ()
{
    // if (m_susyTool) {
    //     delete m_susyTool;
    //     m_susyTool = 0;
    // }
    if( m_jetCleaning ) {
        delete m_jetCleaning;
        m_jetCleaning = 0;
    }

    cout << "finalize" << endl;

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyMETAnalysis :: histFinalize ()
{
    return EL::StatusCode::SUCCESS;
}

TLorentzVector MyMETAnalysis :: sum(std::vector<TLorentzVector> v) {
    TLorentzVector sum;
    for (unsigned int i = 0; i < v.size(); i++) {
        sum += v.at(i);
    }
    return sum;
} 

double MyMETAnalysis :: dR(TLorentzVector U, TLorentzVector V) {
    float deta = fabs( U.Eta() - V.Eta() );
    float dphi = fabs( U.Phi() - V.Phi() );
    if (dphi > pi) dphi = 2.*pi - dphi;
    return sqrt( deta*deta + dphi*dphi );
}

std::vector<double> MyMETAnalysis :: METvector (std::vector<TLorentzVector> v) {
    double metx = 0; 
    double mety = 0;
    double summetx = 0; 
    double summety = 0;
    double sumET=0.;
    std::vector<double> u;
    for (unsigned int i = 0; i < v.size(); i++){
        metx += v.at(i).Px();
        sumET += v.at(i).Et();
        summetx += (v.at(i).Px()*v.at(i).Px());
        summety += (v.at(i).Py()*v.at(i).Py());
    //sumET += sqrt(v.at(i).Py()*v.at(i).Py() + v.at(i).Px()*v.at(i).Px());

        mety += v.at(i).Py();
    }
    u.push_back(-metx);
    u.push_back(-mety);
    u.push_back(sqrt(metx*metx + mety*mety));
    u.push_back(sumET);
//u.push_back(sqrt(summety + summety));
    return u;
}

double MyMETAnalysis :: length(TLorentzVector U) {
    return sqrt(U.Px()*U.Px() + U.Py()*U.Py());
}