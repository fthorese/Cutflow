#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
// EDM includes:
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include <TTree.h>
#include "TFile.h"
#include <vector>
#include "TLorentzVector.h"
#include "EventLoop/OutputStream.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include "Cutflow/MyMETAnalysis.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "TH2.h"
#include "fastjet/ClusterSequence.hh"
#include "Cutflow/WaveletAnalysis.h"
#include "Cutflow/CutsInxAOD.h"
#include "METUtilities/METMaker.h"
#include "AsgTools/ToolHandle.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJet.h"
#include "Cutflow/ObjDef.h"

using namespace std;
using namespace fastjet;

// this is needed to distribute the algorithm to the workers
ClassImp(ObjDef)
/*
* This class is for making NTuples of MET values.
*/

// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )


ObjDef :: ObjDef ()
{
}



EL::StatusCode ObjDef :: setupJob (EL::Job& job)
{
    job.useXAOD ();
    EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: histInitialize ()
{
    TFile *outputFile = wk()->getOutputFile (outputName);

    METTruthTree = new TNtuple("MET Truth 0", "MET Truth","MET");
    METTruthTree1 = new TNtuple("MET Truth 1", "MET Truth","MET");
    METTruthTree2 = new TNtuple("MET Truth 2", "MET Truth","MET");
    METTruthTree3 = new TNtuple("MET Truth 3", "MET Truth","MET");
    METRefFinal = new TNtuple("MET Ref Final", "MET Ref Final","MET");
        METRefFinal->SetDirectory(outputFile);
    chPFOMET = new TNtuple("MET from charged PFOs", "Charged Particle Flow MET","MET");
    neuPFOMET = new TNtuple("MET from neutral PFOs", "Neutral Particle Flow MET","MET");
    neuPFOwlMET = new TNtuple("MET from neutral PFOs using Wavelets", "Neutral Particle Flow MET with Wavelets","MET");
    chPFOwlMET = new TNtuple("MET from charged PFOs using Wavelets", "Charged Particle Flow MET with Wavelets","MET");

    PFOMET = new TNtuple("MET from all PFOs", "Particle Flow MET", "MET");
    PFOMETwl = new TNtuple("MET from all PFOs using Wavelets", "Particle Flow MET with Wavelets", "MET");

    PFO_ET_neuwl= new TH2F("MET from neutral PFOs using Wavelets","PFO_ET_neuwl", 64, -4, 4, 64, -3, 3 );
    PFO_ET_chwl= new TH2F("MET from charged PFOs using Wavelets","PFO_ET_chwl", 64, -4, 4, 64, -3, 3 );
    PFO_ET_neu= new TH2F("MET from neutral PFOs","PFO_ET_neu", 64, -4, 4, 64, -3, 3 );
    PFO_ET_ch= new TH2F("MET from charged PFOs","PFO_ET_ch", 64, -4, 4, 64, -3, 3 );
    
    PFO_ET_sum= new TH2F("MET from all PFOs","PFO_ET_sum", 64, -4, 4, 64, -3, 3 );
    PFO_ET_sumwl= new TH2F("MET from all PFOs using Wavelets","PFO_ET_sumwl", 64, -4, 4, 64, -3, 3 );

    METTruthTree->SetDirectory (outputFile);
    METTruthTree1->SetDirectory (outputFile);
    METTruthTree2->SetDirectory (outputFile);
    METTruthTree3->SetDirectory (outputFile);
    
    chPFOMET->SetDirectory (outputFile);
    neuPFOMET->SetDirectory (outputFile);
    neuPFOwlMET->SetDirectory (outputFile);
    chPFOwlMET->SetDirectory (outputFile);
    
    PFOMET->SetDirectory (outputFile);
    PFOMETwl->SetDirectory (outputFile);

    wk()->addOutput(PFO_ET_neuwl);
    wk()->addOutput(PFO_ET_chwl);
    wk()->addOutput(PFO_ET_neu);
    wk()->addOutput(PFO_ET_ch);
    
    wk()->addOutput(PFO_ET_sum);
    wk()->addOutput(PFO_ET_sumwl);
    
    metMaker = new met::METMaker("metmaker");
    m_jetCleaning = new JetCleaningTool("JetCleaning");
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: fileExecute ()
{
    cout << "file execute"  << endl;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: changeInput (bool firstFile)
{
    cout << "changeInput" << endl;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: initialize ()
{

    // count number of events
    m_eventCounter = 0;
    xAOD::TEvent* event = wk()->xaodEvent();

    // as a check, let's see the number of events in our xAOD
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: execute ()
{
    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- EVENT INFORMATION -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    const xAOD::EventInfo* eventInfo = 0;

    xAOD::TEvent* event = wk()->xaodEvent();

    // print every 25 events, so we know where we are:
    if( (m_eventCounter % 1) ==0 ) //Info("execute()", "Event number = %i", m_eventCounter );
        m_eventCounter++;

    EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

    cout << "EventNumber = " << m_eventCounter << endl;
    // check if the event is data or MC
    // (many tools are applied either to data or MC)
    bool isMC = false;
    // check if the event is MC
    if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
        isMC = true; // can do something with this later
        //cout << "This is MC" << endl;
    }

    // fill the branches of our trees
    EventNumber = eventInfo->eventNumber();
    double N_pileup = eventInfo->averageInteractionsPerCrossing();

    //----------------------------------------- CONTAINERS -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    const xAOD::JetContainer* jets = 0;
    const xAOD::PFOContainer* chPFOs = 0;
    const xAOD::PFOContainer* neuPFOs = 0;
    const xAOD::MissingETContainer* METcontainer = 0;
    //const xAOD::VertexContainer* PVs = 0;
    //const xAOD::TrackParticleContainer* TrackParticles = 0;
    //const xAOD::TauJetContainer* taujets = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( jets, "AntiKt4EMTopoJets" ));
   // EL_RETURN_CHECK("execute()", event->retrieve( taujets,  "TauJets" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( chPFOs,  "JetETMissChargedParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( neuPFOs, "JetETMissNeutralParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( METcontainer, "MET_Truth"));
    
    const xAOD::MuonContainer* muons = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( muons, "Muons" ));

    const xAOD::ElectronContainer* electrons = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( electrons, "Electrons" ));

    
    const xAOD::PhotonContainer* gammas = 0;
    EL_RETURN_CHECK("execute()", event->retrieve(gammas, "Photons"));
    Info("execute()", "  number of photons in the event = %lu", gammas->size()); 

    //xAOD::MissingETContainer* nonconstMETcontainer = const_cast<xAOD::MissingETContainer*>(METcontainer);


    //cout << "number of jets: " << jets->size() << endl;
    //m_metutil = new ToolHandle<IMETMaker>("METTool");    
    
/*
    METRef metref;
    metref = METRef();
    // Calculate MET
    xAOD::MissingETContainer*  met    = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* metAux = new xAOD::MissingETAuxContainer();
    met->setStore(metAux);
    std::vector<double> met_vec = metref.GetMET( met, jets, electrons, muons, taujets, gammas, false ,event, metMaker);
    double missingEt = met_vec[0];
    
    cout << "METRefFinal: " << missingEt << endl;
    METRefFinal->Fill(missingEt);
    //std::vector<double> metvec = metref.GetMET(nonconstMETcontainer, jets,electrons, muons, taujets, false, event, metMaker);
*/


    

// BOILDOWN
    /*
    double missingEt = 0;
    CutsInxAOD* cutAnalyzer = new CutsInxAOD(missingEt);
    
    m_jetCleaning->msg().setLevel( MSG::DEBUG );
    // ---------------------------------------------- Muons --------------------------------------------------
    double cutElectron = cutAnalyzer->analyzeZbosonsFromElectrons(electrons);
    double cutMuon = cutAnalyzer->analyzeZbosonsFromMuons(muons);
    double cutJets = cutAnalyzer->analyzeZbosonsFromJets(jets,m_jetCleaning,0,0);
    bool copyEvent = false;
    if (cutElectron > 0 || cutMuon > 0) {
        if (cutJets > 0) {
            copyEvent = true;
        } 
    }
if (copyEvent == true) {
    //-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    //---------------------------------------------- PFLOW --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    // copy particle flow objects (PFO) in to TLorentzVector
    for (unsigned int i = 0; i < chPFOs->size(); i++) {
        chPFO_vec.push_back(TLorentzVector());
        chPFO_vec.back().SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());
    }

    for (unsigned int i = 0; i < neuPFOs->size(); i++) {
        neuPFO_vec.push_back(TLorentzVector());
        neuPFO_vec.back().SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
    }


    WaveletAnalysis chwlAnalyzer(64,4.9,N_pileup, chPFO_vec);
    WaveletAnalysis neuwlAnalyzer(64,4.9,N_pileup, neuPFO_vec);
    
    

    chPFO_vec_new = chwlAnalyzer.denoise(N_pileup*1000).particles();
    neuPFO_vec_new = neuwlAnalyzer.denoise(N_pileup*1000).particles();
    
    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ MET --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------

    TLorentzVector PFO_ET_neu_data = sum(neuPFO_vec);
    TLorentzVector PFO_ET_neuwl_data = sum(neuPFO_vec_new);
    TLorentzVector PFO_ET_ch_data = sum(chPFO_vec);
    TLorentzVector PFO_ET_chwl_data = sum(chPFO_vec_new);

    chPFOMET->Fill(PFO_ET_ch_data.Et());
    neuPFOMET->Fill(PFO_ET_neu_data.Et());
    neuPFOwlMET->Fill(PFO_ET_neuwl_data.Et());
    chPFOwlMET->Fill(PFO_ET_chwl_data.Et());

    double METTruthpt = 0;
    METTruthpt = METcontainer->at(3)->sumet();
    METTruthTree->Fill(METcontainer->at(0)->sumet());
    METTruthTree1->Fill(METcontainer->at(1)->sumet());
    METTruthTree2->Fill(METcontainer->at(2)->sumet());
    METTruthTree3->Fill(METcontainer->at(3)->sumet());
    
    double pfoMET = PFO_ET_neu_data.Et() + PFO_ET_ch_data.Et();
    double pfoMETwl = PFO_ET_neuwl_data.Et() + PFO_ET_chwl_data.Et();

    PFOMET->Fill(pfoMET);
    PFOMETwl->Fill(pfoMETwl);
    
    PFO_ET_neu->Fill(PFO_ET_neu_data.Eta(), PFO_ET_neu_data.Phi(),  PFO_ET_neu_data.Pt());
    PFO_ET_neuwl->Fill(PFO_ET_neuwl_data.Eta(), PFO_ET_neuwl_data.Phi(),  PFO_ET_neuwl_data.Pt());
    PFO_ET_ch->Fill(PFO_ET_ch_data.Eta(), PFO_ET_ch_data.Phi(),  PFO_ET_ch_data.Pt());
    PFO_ET_chwl->Fill(PFO_ET_chwl_data.Eta(), PFO_ET_chwl_data.Phi(),  PFO_ET_chwl_data.Pt());

    PFO_ET_sum->Fill(
        (PFO_ET_neu_data.Eta() + PFO_ET_ch_data.Eta()), 
        (PFO_ET_neu_data.Phi() + PFO_ET_ch_data.Phi()), 
        (PFO_ET_neu_data.Energy() + PFO_ET_ch_data.Energy()));
    
    PFO_ET_sumwl->Fill(
        (PFO_ET_neuwl_data.Eta() + PFO_ET_chwl_data.Eta()), 
        (PFO_ET_neuwl_data.Phi() + PFO_ET_chwl_data.Phi()), 
        (PFO_ET_neuwl_data.Energy() + PFO_ET_chwl_data.Energy()));
    }*/
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: postExecute ()
{
    cout << "postExecute" << endl;
    chPFO_vec.clear();
    neuPFO_vec.clear();

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: finalize ()
{
    //xAOD::TEvent* event = wk()->xaodEvent();
    delete m_jetCleaning;
    delete metMaker;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode ObjDef :: histFinalize ()
{
    return EL::StatusCode::SUCCESS;
}

TLorentzVector ObjDef :: sum(std::vector<TLorentzVector> v) {
    TLorentzVector sum;
    for (unsigned int i = 0; i < v.size(); i++) {
        sum += v.at(i);
    }
    return sum;
} 

double ObjDef :: dR(TLorentzVector U, TLorentzVector V) {

    float deta = fabs( U.Eta() - V.Eta() );
    float dphi = fabs( U.Phi() - V.Phi() );
    if (dphi > pi) dphi = 2.*pi - dphi;
    return sqrt( deta*deta + dphi*dphi );
}