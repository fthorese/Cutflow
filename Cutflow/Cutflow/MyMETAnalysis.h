#ifndef theSearch_MyMETAnalysis_H
#define theSearch_MyMETAnalysis_H

#include <EventLoop/Algorithm.h>
#include "TNtuple.h"
#include "NewWave/NewWave.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TH2.h"
#include "fastjet/ClusterSequence.hh"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
using namespace std;
using namespace fastjet;

class MyMETAnalysis : public EL::Algorithm
{

public:
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();
    double filtercut;
    double denoisecut;
    double timesratio;
    double dR(TLorentzVector U, TLorentzVector V);
    double length(TLorentzVector U);
    std::vector<double> METvector (std::vector<TLorentzVector> v);
    TLorentzVector sum(std::vector<TLorentzVector> v);
    PseudoJet sumjet(std::vector<PseudoJet> v);
//    MyMETAnalysis(double filterCut, double denoiseCut,double timesRatio);
    MyMETAnalysis();
    TNtuple* mycutspt;//!
    TNtuple* pileupTup;//!
    TNtuple* mycuts;//!
    TNtuple* chRatio;//!
    ST::SUSYObjDef_xAOD *m_susyTool;//!
    std::vector<TNtuple*> myalphas;//!
    TNtuple* MET_TruthPileup;//!
    TNtuple* chptRatio;//!

    TNtuple* MET_Truth_NonInt;//!
    TNtuple* MET_Truth_Unknown;//!
    TNtuple* MET_Truth_IntMuons;//!
    TNtuple* MET_TruthPar_Int;//!
    TNtuple* MET_TruthPar_NonInt;//!
    TNtuple* METfinaltrack;//!
    TNtuple* METfinalclus;//!

    xAOD::MissingETContainer* met;//!
    xAOD::MissingETAuxContainer* metAux;//!
    JetCleaningTool *m_jetCleaning; //!  
    string outputName;
    int m_eventCounter; //!
    int EventNumber; //!

    TNtuple* myDenoise;//!

    TNtuple* meanMETwcut; //!
    TNtuple* totalMETwcut; //!
    TNtuple* METTruthTree1; //!
    TNtuple* METTruthTree3; //!
    TNtuple* METRefFinal;//!
    TNtuple* PFOMETwl_ratiodenoisept_filter;//!
    TNtuple* PFOMETwl_ratiodenoisept_track;//!
    TNtuple* PFOMETwl_denoise_filter;//!
    TNtuple* PFOMETwl_denoise_track;//!
    TNtuple* PFOMETwl_ratiodenoise_filter;//!
    TNtuple* PFOMETwl_ratiodenoise_track;//!
    TNtuple* PFOMET;//!
    TNtuple* PFOch;//!
    TNtuple* PFOneu;//!
    TNtuple* PFOMET_entropi;//!
    TNtuple* PFOMETwl_denoise;//!
    TNtuple* PFOMETwl_mydenoise;//!
    TNtuple* PFOMETwl_filter;//!
    TNtuple* PFOMETwl_combined;//!
    TNtuple* PFOMETwl_track;//!
    TNtuple* PFOMET_filter;//!

    vector<TLorentzVector> PFO; //!
    vector<TLorentzVector> chPFO; //!
    vector<TLorentzVector> neuPFO; //!
ClassDef(MyMETAnalysis, 1);
};

#endif
