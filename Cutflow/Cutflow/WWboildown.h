#ifndef theSearch_WWboildown_H
#define theSearch_WWboildown_H

#include <EventLoop/Algorithm.h>
#include "TNtuple.h"
#include "NewWave/NewWave.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TH1.h"
#include "xAODBTagging/BTaggingAuxContainer.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

//MetaData
#include <xAODMetaDataCnv/FileMetaDataTool.h>
#include <xAODTriggerCnv/TriggerMenuMetaDataTool.h>


using namespace std;

class WWboildown : public EL::Algorithm
{
    public:
        // these are the functions inherited from Algorithm
        virtual EL::StatusCode setupJob (EL::Job& job);
        virtual EL::StatusCode fileExecute ();
        virtual EL::StatusCode histInitialize ();
        virtual EL::StatusCode changeInput (bool firstFile);
        virtual EL::StatusCode initialize ();
        virtual EL::StatusCode execute ();
        virtual EL::StatusCode postExecute ();
        virtual EL::StatusCode finalize ();
        virtual EL::StatusCode histFinalize ();
        WWboildown();
    std::vector<double> METvector (std::vector<TLorentzVector> v);
    BTaggingSelectionTool* btag_70 = nullptr; //!
        string outputName; 
        TH1F* cutsplot;//!
        int m_eventCounter; //!
        int EventNumber; //!
        
    int passZnumbercut = 0; //!
    int passZptcut = 0; //!
    int passTrigger = 0; //!
    int passelectronmass = 0; //!
    int passmuonenergy = 0; //!
    int passWenergy = 0; //!

  /// Pointer for the File MetaData Tool
  xAODMaker::FileMetaDataTool          *m_fileMetaDataTool;    //!
  /// Pointer for the TriggerMenu MetaData Tool
  xAODMaker::TriggerMenuMetaDataTool   *m_trigMetaDataTool; //!

        //GoodRunsListSelectionTool *m_grl; //!
        JetCleaningTool *m_jetCleaning; //!
        Trig::TrigDecisionTool *m_trigDecisionTool; //!
        TrigConf::xAODConfigTool *m_trigConfigTool; //!
        //vector<TLorentzVector> Z_from_muons; //!
        //vector<TLorentzVector> Z_from_electrons; //! 
        TNtuple* cutsNTuple;//!
        ClassDef(WWboildown, 1);
};

#endif
