#ifndef theSearch_ObjDef_H
#define theSearch_ObjDef_H

#include <EventLoop/Algorithm.h>
#include "TNtuple.h"
#include "NewWave/NewWave.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TH2.h"
#include "fastjet/ClusterSequence.hh"
#include "METUtilities/METMaker.h"

using namespace std;
using namespace fastjet;

class ObjDef : public EL::Algorithm
{

public:
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();
    double dR(TLorentzVector U, TLorentzVector V);

    TLorentzVector sum(std::vector<TLorentzVector> v);
    PseudoJet sumjet(std::vector<PseudoJet> v);
    met::METMaker* metMaker;//!
    ObjDef();
    TH2F* PFO_ET_neuwl; //!
    TH2F* PFO_ET_chwl; //!
    TH2F* PFO_ET_neu; //!
    TH2F* PFO_ET_ch; //!
    TH2F* PFO_ET_sum; //!
    TH2F* PFO_ET_sumwl; //!
    TNtuple* METRefFinal;//!
    JetCleaningTool* m_jetCleaning;//!

    string outputName;
    int m_eventCounter; //!
    int EventNumber; //!


    TNtuple* METTruthTree; //!
    TNtuple* METTruthTree1; //!
    TNtuple* METTruthTree2; //!
    TNtuple* METTruthTree3; //!


    TNtuple* chPFOMET; //!
    TNtuple* neuPFOMET; //!
    TNtuple* neuPFOwlMET; //!
    TNtuple* chPFOwlMET; //!

    double totalsumpfo;//!
    double totalsumpfowl;//!
    double totalsumtruth;//!
    TNtuple* PFOMET;//!
    TNtuple* PFOMETwl;//!

    vector<TLorentzVector> chPFO_vec; //!
    vector<TLorentzVector> chPFO_vec_new; //! 
    vector<TLorentzVector> neuPFO_vec; //!
    vector<TLorentzVector> neuPFO_vec_new; //! 
ClassDef(ObjDef, 1);
};

#endif
