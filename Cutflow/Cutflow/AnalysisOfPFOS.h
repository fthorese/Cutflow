#ifndef theSearch_AnalysisOfPFOS_H
#define theSearch_AnalysisOfPFOS_H

#include <EventLoop/Algorithm.h>
#include "TNtuple.h"
#include "NewWave/NewWave.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "TH2.h"
#include "fastjet/ClusterSequence.hh"
#include "TH1.h"

using namespace std;
using namespace fastjet;

class AnalysisOfPFOS : public EL::Algorithm
{

public:
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();
    bool goodevent;
    TLorentzVector sum(std::vector<TLorentzVector> v);
    PseudoJet sumjet(std::vector<PseudoJet> v);
    double dR(TLorentzVector U, TLorentzVector V);

    AnalysisOfPFOS();
    TH2* hist_all_coef;//!
    TH2* hist_all_coef_scale;//!
    TH2* hist_all_coef_entropy;//!
    TH2* hist_all_alphas;//!

    TH2F* jet_ET_neuwl; //!
    TH2F* jet_ET_chwl; //!
    TH2F* jet_ET_neu; //!
    TH2F* jet_ET_ch; //!
    TH2F* jet_ET_sum; //!
    TH2F* jet_ET_sumwl; //!
    std::vector<TH2F*> coefficients;//!
    std::vector<TNtuple*> coefficients1D;//!
    std::vector<TNtuple*> coefficientsratio1D;//!
    std::vector<TNtuple*> coefficientsscale1D;//!
    std::vector<TNtuple*> coefficientsentropi1D;//!

    std::vector<TH2F*> pileupcoefficients;//!
    std::vector<TNtuple*> rms;//!

    TH2F* neuwlcoef; //!
    TH2F* chwlcoef; //!

    std::vector<NewWave::WaveletCoefficient> neuCoefs;//!
    std::vector<NewWave::WaveletCoefficient> chCoefs;//!
TNtuple* pileup_eta;//!
TNtuple* pileup_pt;//!
TNtuple* pileup_phi;//!
TNtuple* resi_pt;//!
TNtuple* PFO;//!

    TNtuple* truth_pt;//!
    TNtuple* ch_eta; //!
    TNtuple* ch_phi;//!
    TNtuple* neu_phi;//!
    TNtuple* neu_eta;//!
    TNtuple* chPFO; //!
    TNtuple* neuPFO; //!
    TNtuple* neuPFOwl; //!
    TNtuple* chPFOwl; //! 
    TNtuple* PFOwl;//!

    TH2F* hist_neuwl; //! 
    TH2F* hist_chwl; //!  
    TH2F* hist_neu; //! 
    TH2F* hist_ch; //!   
    TH2F* hist_pfo  ;//!
    TH2F* hist_pfowl;//!   
    TH2F* hist_truth;//! 
    TH2F* hist_resi ;//! 
    TH2F* hist_pileup ;//! 



    string outputName;
    int m_eventCounter; //!
    int EventNumber; //!
    
    TNtuple* METTruthTree; //!
    TNtuple* chPFOMET; //!
    TNtuple* neuPFOMET; //!
    TNtuple* neuPFOwlMET; //!
    TNtuple* chPFOwlMET; //!

    vector<TLorentzVector> chPFO_vec; //!
    vector<TLorentzVector> chPFO_vec_new; //! 
    vector<TLorentzVector> neuPFO_vec; //!
    vector<TLorentzVector> neuPFO_vec_new; //! 
ClassDef(AnalysisOfPFOS, 1);
};

#endif
