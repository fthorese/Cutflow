#!/usr/bin/env python

from __future__ import print_function
import ROOT
import os
import sys
import argparse
import fnmatch
import pprint
import numpy

from collections import OrderedDict

class Dictlist(OrderedDict):
    def __setitem__(self, key, value):
        try:
            self[key]
        except KeyError:
            super(Dictlist, self).__setitem__(key, [])
        self[key].append(value)

def checkArguments(args):
    noDir = False
    noFile = False
    noDSID = False
        
    if args.input_dir is not None and len(args.input_dir) > 0:
        for d in args.input_dir:
            if os.path.exists(d) and os.path.isdir(d): continue
            print("ERROR: dir {0} does not exist".format(d))
            sys.exit()
    
    if args.input_dir is None or args.input_dir == []:
        print("ERROR: no inputs provided!")
        sys.exit()
   
    if args.output_file is None or args.output_file == "":
        print("ERROR: no output file provided")
        sys.exit()

    hasData = False
    hasMC = False
    for p in args.process:
        if "data" in p:
            hasData = True
        else:
            hasMC = True

    if hasData and hasMC:
        print("ERROR: you specified both a data and an MC pattern!")
        sys.exit()
   
    # Load sensible defaults here - advance 1 for data
    if args.channel_pos == 0:
        args.channel_pos = 3
        if hasData: 
            args.channel_pos = 4
    if args.name_pos == 0:   
        args.name_pos = 4
        if hasData:
            args.name_pos = 5

    return

def parseArguments(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("--xsecfile", help="cross section file to use", default="{0}/data/SUSYTools/susy_crosssections_13TeV.txt".format(os.getenv("ROOTCOREBIN")))
    
    parser.add_argument("--input-dir", action='append', help="input directory to scan for datasets", default=None)  
    parser.add_argument("--input-file", type=str, help="input file with names to match datasets against", default=None ) 
    parser.add_argument("--dsid", action='append', type=int, help="dsids to match datasets against", default=None)
    parser.add_argument("--process", type=str, action="append", help="process to match against, e.g. 'Wenu'. Used in a simple substring search.", default=[])
    
    parser.add_argument("--output-file", type=str, help="output filename", default=None)
    parser.add_argument("--scan-pattern", help="scan pattern to use when scanning dirs", default="user.*.LFV*_hist") 
    parser.add_argument("--channel_pos", help="position of mc_channel_id in dataset names (split by the character '.')", default=0)
    parser.add_argument("--name_pos", help="position of name in full dataset names (split by the character '.')", default=0)
 
    parser.add_argument("--signal", default=False, action="store_true", help="are you using signal datasets?")
    parser.add_argument("--data", default=False, action="store_true", help="are you using data?")

    args = parser.parse_args(argv)

    checkArguments(args)

    return args

def getSignalName(DSID):
    names = {}
    names[303778] = "Ztaumu"
    names[303779] = "Ztaue"

    if DSID in names:
        return names[DSID]

    return "Unknown"

def histogramNeedsWeight(name):
    unweightedNames = ["metadata", "EventLoop_EventCount", "EventLoop_RunTime"]
    
    if name in unweightedNames:
        return False

    return True

def acceptFilename(filename, args):
    scan_pattern = args.scan_pattern
    DSID = args.dsid
    channel_pos = args.channel_pos
    scan_file = args.input_file

    # Check what we have to match
    matchPattern = False
    if scan_pattern is not None and scan_pattern != "":
        matchPattern = True

    matchDSID = False
    if DSID is not None and DSID != []:
        matchDSID = True

    # Now match it
    if matchPattern and not fnmatch.fnmatch(filename, scan_pattern):
        # have to match and I fail
        return False

    # Check if we need a DSID match
    DSIDFound = False
    if matchDSID:
        for d in DSID:
            split = filename.split(".")
            if len(split) < channel_pos+1: continue
        
            if int(split[channel_pos]) == d:
                DSIDFound = True
                break

        if not DSIDFound:
            return False

    # Check if we're in the list of files
    if scan_file is not None and os.path.exists(scan_file):
        with open(scan_file) as f:
            names = [l.strip() for l in f.readlines()]

        if not filename in names:
            return False

    # Optional simple substring search
    if args.process is not None and args.process != [] and args.process != "":
        if not any(p in filename for p in args.process):
            return False

    return True

def readDatasetNames(args):
    _filenames = Dictlist()
    scan_pattern = args.scan_pattern
    for d in args.input_dir:
        pprint.pprint(d)
        # DictList can't use a genexp so do it like this
        for f in os.listdir(d):
            if not acceptFilename(f, args): continue
            pprint.pprint()
            pprint.pprint(f.split(".")[args.channel_pos])
            _filenames[f.split(".")[args.channel_pos]] = os.path.join(d,f)
            pprint.pprint(os.path.join(d,f))

    # Now find the latest one:
    filenames = {}
    for DSID in _filenames:
        if len(_filenames[DSID]) == 1: 
            filenames[DSID] = _filenames[DSID][0]
            continue

        # Just pick the latest one
        print("WARNING: found {0:d} datasets for run {1}, picking latest!".format(len(_filenames[DSID]), DSID))
        filenames[DSID] = sorted(_filenames[DSID], reverse=True)[0] 
        #pprint.pprint(sorted(_filenames[DSID], reverse=True))

    return filenames

def processFile(filename, xsec):
    print("\t\tProcessing {0}".format(os.path.basename(filename)))

    if not os.path.isfile(filename):
        print("\t\tERROR: {0} is not a file".format(os.path.basename(filename)))
        return

    f = ROOT.TFile.Open(filename, "READ")
    h_metadata = f.Get("metadata")

    if h_metadata is None or h_metadata == 0x0:
        print("\t\tERROR: can't find metadata for {0}".format(os.path.basename(filename)))
        return

    N_processed = int(h_metadata.GetBinContent(2))
    sumW = h_metadata.GetBinContent(8) # this is m_xAOD_initialSumOfWeights in EventSelection.cxx
    lumi = sumW / (xsec * 1000.0)

    normWeight = xsec / sumW

    h = f.Get("EventLoop_EventCount")
    
    if h is None or h == 0x0:
        print("\t\tWARNING: can't find EventLoop_EventCount for {0}".format(DSID))
    else:
        N = int(h.GetBinContent(1))
        if N != N_processed:
            print("\t\tWARNING: metadata and EventLoop_EventCount disagree on how many events we saw!")

    print("\t\t\tFound {0:d} events in metadata; sumWeights = {1:.2f}".format(N_processed, sumW))
    print("\t\t\tnormWeight = {0:.2e}".format(normWeight))
    print("\t\t\t(Lumi = {0:.2e} fb-1)".format(lumi))

    return (normWeight, sumW, N_processed)

def getFilenames(path):
    return [os.path.join(path, f) for f in os.listdir(path) if f.endswith(".root") or f.split(".")[-2] == "root"]

def getSignalCrossSection(DSID):
    _DSIDs = [303778, 303779]

    if not DSID in _DSIDs:
        return (0.0, 0)

    if(DSID == 303779):
        eff = 6.8658E-01 # from AMI
    elif(DSID == 303778):
        eff = 6.9251E-01 # from AMI

    xsec = 1869 / 0.034 # total Z xsec
    xsec *= eff # efficiency of the generator
    xsec *= 1.2e-05 # braching ratio

    rel_uncert = 0.0 # TODO: to be implemented??

    return (xsec, rel_uncert)

def processDataset(xsecDB, DSID, path):
    pprint.pprint("--------- DSID")

    pprint.pprint(DSID)
    if int(DSID) == 303778 or int(DSID) == 303779:
        print("\tWARNING: DSID {0} is a signal dataset. Flagging as such!".format(DSID))
        args.signal = True

    # find all the root files in this dir
    if not os.path.isdir(path):
        print("\tWARNING: skipping - path is not a directory")
        return None

    filenames = getFilenames(path)

    print("\tFound the following files:")
    for f in filenames:
        print("\t\t{0}".format(os.path.basename(f)))

    if args.data:
        xsec = 1
        rel_uncert = 0
    elif args.signal:
        xsec, rel_uncert = getSignalCrossSection(int(DSID))
        if xsec < 0:
            print("\tWARNING: signal channel {0} not known. Skipping!".format(DSID))
            return
    else:
        xsec = xsecDB.xsectTimesEff(int(DSID))
        rel_uncert = xsecDB.rel_uncertainty(int(DSID))
        if xsec < 0:
            print("\tWARNING: Channel {0} not known to SUSYTools. Skipping!".format(DSID))
            return None

    print("\tExtracted xsec*eff = {0:e} pb (rel_unc = {1:.2e})".format(xsec, rel_uncert))

    print("\tProcessing files")
    normWeights = {}
    for f in filenames:
        if os.path.basename(f) == "merged.root": continue # this is likely a locally merged file already!!
        (normWeight, sumW, N_processed) = processFile(f, xsec)
        
        if args.data:
            print("Forcing normWeight = 1 for data")
            normWeight = 1

        normWeights[f] = {"weight" : normWeight, "rel_unc" : rel_uncert, "sumW" : sumW, "xsec" : xsec, "N_processed" : N_processed}

    return normWeights

def getListOfTrees(filename):
    f = ROOT.TFile.Open(filename, "READ")

    if not f or f.IsZombie():
        print("getListOfTrees: could not open input file {0}".format(filename))
        return []

    names = []
    keys = f.GetListOfKeys()
    for k in keys:
        name = k.GetName()
        obj = f.Get(name)
        if not obj.IsA().InheritsFrom(ROOT.TTree.Class()): 
            continue
        names.append(name)

    f.Close()

    return names

def getListOfHistograms(filename):
    f = ROOT.TFile.Open(filename, "READ")

    if not f or f.IsZombie():
        print("getListOfHistograms: could not open input file {0}".format(filename))
        return []

    names = []
    keys = f.GetListOfKeys()
    for k in keys:
        name = k.GetName()
        obj = f.Get(name)
        if not obj.IsA().InheritsFrom(ROOT.TH1D.Class()) and not obj.IsA().InheritsFrom(ROOT.TH2D.Class()): 
            continue
        names.append(name)

    f.Close()

    return names

def cloneAndScaleHistograms(filename, names, weight):
    f = ROOT.TFile.Open(filename, "READ")

    if not f or f.IsZombie():
        print("getListOfHistograms: could not open input file {0}".format(filename))
        return []

    hists = {}
    for n in names:
        h = f.Get(n).Clone()
        
        if histogramNeedsWeight(n):
            h.Scale(weight)
        else:
            print("cloneAndScaleHistograms: using unweighted merge for {0}".format(n))
        
        h.SetDirectory(0)
        hists[n] = h

    f.Close()

    return hists

def mergeHistograms(inputFiles, normWeights, f, isData=False):
    if len(inputFiles) == 0:
        print("mergeHistograms(): no input files provided!")
        return

    hists = getListOfHistograms(inputFiles[0])

    if hists == []:
        print("mergeHistograms: no histograms to merge!")
        return

    print("Identified the following histograms:")
    for h in hists: 
        print("\t{0}".format(h))

    # Make output hists by cloning from the first file
    if not inputFiles[0] in normWeights:
        print("ERROR: all your files got rejected because of unknown cross sections!")
        return
    
    outputHists = cloneAndScaleHistograms(inputFiles[0], hists, normWeights[inputFiles[0]]["weight"])

    # Ensure they end up in the correct file
    for name in hists:
        outputHists[name].SetDirectory(f)

    # Now add the rest
    for filename in inputFiles[1:]:
        if not isData and filename not in normWeights:
            print("Skipping file with unknown xsec {0}".format(filename))
            continue

        fIn = ROOT.TFile.Open(filename, "READ")
        for name in hists:
            h = fIn.Get(name)
            #print("DEBUG: loading {0} from {1}".format(name, filename))
            if not h or h is None:
                print("WARNING: {0} is 0x0 in {1}!!".format(name, filename))
                continue

            if not isData and histogramNeedsWeight(name):
                outputHists[name].Add(h, normWeights[filename]["weight"])
            else:
                print("mergeHistograms: using unweighted merge for {0}".format(name))
                outputHists[name].Add(h)
        fIn.Close()

    f.cd() # otherwise we're no longer in a file!
    for name in hists:
        outputHists[name].SetDirectory(f)
        outputHists[name].Write()

    return

def setupClonedTree(filename, name):
    f = ROOT.TFile.Open(filename, "READ")

    if not f or f.IsZombie():
        print("setupClonedTree: could not open input file {0}".format(filename))
        return None
    
    inputTree = f.Get(name)

    outputTree = inputTree.CloneTree(-1) # just copy the structure
    inputTree.GetListOfClones().Remove(outputTree) # detach to not copy data
    outputTree.SetDirectory(0)

    f.Close()

    return clonedTree

def mergeTrees(inputFiles, normWeights, f, isData=False):
    if len(inputFiles) == 0:
        print("mergeHistograms(): no input files provided!")
        return

    trees = getListOfTrees(inputFiles[0])

    if trees == []:
        print("mergeTrees: no trees to merge!")
        return

    print("Identified the following trees:")
    for t in trees: 
        print("\t{0}".format(t))
    
    # Make output hists by cloning from the first file
    if not isData and not inputFiles[0] in normWeights:
        print("ERROR: all your files got rejected because of unknown cross sections!")
        return

    for t in trees: 
        print("Processing {0}".format(t))
        
        # Build a list for each of these
        outputTrees = ROOT.TList()
        for filename in inputFiles:
            if not isData and filename not in normWeights:
                print("Skipping file with unknown xsec {0}".format(filename))
                continue
            
            print("\tLoading from {0}".format(os.path.basename(filename)))
            fIn = ROOT.TFile.Open(filename, "READ")
            inputTree = fIn.Get(t)
            inputTree.SetBranchStatus("*", 1)

            # This line will cause " ERROR   unknown branch -> normWeight" 
            # but without it, it doesn't actually work!!
            inputTree.SetBranchStatus("normWeight", 0) 

            # Now loop over the entries and replace the normWeight
            outputTree = inputTree.CloneTree(0)
            outputTree.SetDirectory(0)

            # The numpy stuff is needed to make PyROOT want to eat our normWeight and actually set it
            normWeight = numpy.zeros(1, dtype=float)
            normWeightUp = numpy.zeros(1, dtype=float)
            normWeightDown = numpy.zeros(1, dtype=float)
            outputTree.Branch('normWeight', normWeight, 'normWeight/D')
            outputTree.Branch('normWeightUp', normWeightUp, 'normWeightUp/D')
            outputTree.Branch('normWeightDown', normWeightDown, 'normWeightDown/D')

            if isData:
                normWeight[0] = 1.0
                rel_unc = 0
            else:
                normWeight[0] = normWeights[filename]["weight"]
                rel_unc = normWeights[filename]["rel_unc"]
            
            normWeightUp[0] = normWeight[0] * (1 + rel_unc)
            normWeightDown[0] = normWeight[0] * (1 - rel_unc)
    
            print("Using weight {0}".format(normWeight[0]))
                
            processed = 0
            N = inputTree.GetEntries()
            for i in range(N):
                processed += 1
                inputTree.GetEntry(i)
                outputTree.Fill()
                sys.stdout.write("\tProcessed: {0:10d} / {1:d}\r".format(processed, N))
                sys.stdout.flush()

            sys.stdout.write("\n")
            sys.stdout.flush()

            outputTrees.Add(outputTree)
            fIn.Close()

        # Now merge all of these together
        print("Writing output file...")
        f.cd()
        mergedTree = ROOT.TTree.MergeTrees(outputTrees)
        mergedTree.Write()

    f.Close()

    return

def writeTexOutput(normWeights, xsecDB, process, output_file, isSignal=False, isData=False):

    label = process
    if process is None or process == "":
        label = os.path.basename(output_file).replace(".tex", "")

    with open(output_file, "w+") as f:
        print("\\definecolor{lightgray}{gray}{0.925}", file=f)
        print("\\begin{sidewaystable}", file=f)
        print("\\footnotesize", file=f)
        print("\\rowcolors{1}{lightgray}{}", file=f)
        print("\\begin{tabularx}{\\textwidth}{", file=f)
        print("    l % DSID", file=f)
        print("    X % Name", file=f)
        # NOTE: make sure that these numbers always match the print specifiers below: if you print 4 digits but only have space for 3, the table looks weird
        print("    S[table-format=1.2e+2] % xsec*eff", file=f)
        print("    S[table-format=1.2] % k-factor", file=f)
        print("    S[table-format=8] %S[table-format=3.2,table-number-alignment=right] % Ngen", file=f)
        print("    S[table-format=1.2e+2] % sumW", file=f)
        print("    S[table-format=1.2e+2] % lumi", file=f)
        print("    }", file=f)
        print("\\toprule", file=f)
        print("\\rowcolor{white}", file=f)
        # Use multicolumn as a way to left-align
        print("\\multicolumn{1}{l}{\\bfseries DSID} & \\multicolumn{1}{l}{\\bfseries Name} & \\multicolumn{1}{l}{$\\mathbf{\\sigma\\cdot\\epsilon}$~[\\si{\\pb}]} & \\multicolumn{1}{l}{$k\\textup{-factor}$} & \\multicolumn{1}{l}{$N_{\\mathrm{gen}}$} & \\multicolumn{1}{l}{$\\Sigma\\, w$} & \\multicolumn{1}{l}{$\\mathcal{L}$~[\\si{\\ifb}]} \\\\", file=f)
        print("\\midrule", file=f)

        if not isData and len(normWeights) > 0:
            print("Norm. weights for 4fb-1:") # to stdout
        
        for DSID in sorted(normWeights):
            weights = normWeights[DSID]
            sumW = 0
            N_processed = 0
            normWeight = 0
            xsec = 0
            for filename in weights:
                sumW += weights[filename]["sumW"]
                N_processed += weights[filename]["N_processed"]
                xsec = weights[filename]["xsec"]

            if not isSignal:
                xsec = xsecDB.xsectTimesEff(int(DSID))
            
            if sumW > 0:
                normWeight = xsec / sumW
                lumi = sumW / (xsec * 1000.0)
            else:
                # empty file
                normWeight = 0
                lumi = 0

            if int(DSID) == 361108:
                # Powheg Ztautau counts different internally
                sumW /= 1000.0
                lumi /= 1000.0

            if not isSignal:
                name = xsecDB.name(int(DSID)).replace('_','\_')
                raw_xsec = xsecDB.rawxsect(int(DSID)) * xsecDB.efficiency(int(DSID))
                k_factor = xsecDB.kfactor(int(DSID))
            else:
                raw_xsec = xsec
                k_factor = 1
                name = getSignalName(int(DSID))

            print("{0:d} & {1:s} & {2:.2e} & {3:.2f} & {4:5d} & {5:.2e} & {6:.2e} \\\\".format(int(DSID), name, raw_xsec, k_factor, N_processed, int(sumW), lumi), file=f)

            if not isData:
                print("\t{0:d} => {1:.3g}".format(int(DSID), normWeight * 4000.0)) # to stdout

        print("\\bottomrule", file=f)
        print("\\end{tabularx}", file=f)
        print("\\caption{Summary of samples used in the analysis. $\sigma \cdot \epsilon$ corresponds to the cross-section provided by the Monte Carlo generator multiplied with a possible truth-level filter efficiency. The $k$-factor is used to normalise the generator cross-section to the best known for a process. $\sum w$ includes the generator-level weights and the pileup reweighting effect. $\mathcal{L}_{int}$ is the generated equivalent integrated luminosity for the sample.}", file=f)
        print("\\label{tab:crosssection_%s}" % label, file=f)
        print("\\end{sidewaystable}", file=f)

    return

def normWeightsByDSID(normWeights, xsecDB, isData):
    retval = {}
    for DSID in sorted(normWeights):
        weights = normWeights[DSID]
        sumW = 0
        N_processed = 0
        normWeight = 0
        for filename in weights:
            sumW += weights[filename]["sumW"]
            N_processed += weights[filename]["N_processed"]
            
            xsec = weights[filename]["xsec"]
            rel_uncert = weights[filename]["rel_unc"]

        if isData:
            # data
            normWeight = 1
            lumi = 0
            rel_uncert = 0
        elif N_processed > 0:
            # MC with calculable weight
            normWeight = xsec / sumW
            lumi = sumW / (xsec * 1000.0)
        else:
            # empty file
            normWeight = 0
            lumi = 0

        retval[DSID] = {"weight" : normWeight, "rel_unc" : rel_uncert, "sumW" : sumW, "xsec" : xsec, "N_processed" : N_processed}

    return retval

def mergeDatasets(datasets, normWeights, xsecDB, output_file, isData):
    print("Will write output to {0}".format(output_file))

    # Get all files and set the normWeights per file
    filenames = []
    for DSID in datasets:
        filenames += getFilenames(datasets[DSID])

    # Ignore merged.root files in datasets
    filenames = [f for f in filenames if os.path.basename(f) != "merged.root"]
    
    # Group the weights by DSID
    weightsByDSID = normWeightsByDSID(normWeights, xsecDB, isData)
  
    # Construct a flat dict of filename => weight
    _normWeights = {}
    for DSID in normWeights:
        #_normWeights.update({f: normWeights[DSID][f] for f in normWeights[DSID]})
        _normWeights.update({f: weightsByDSID[DSID] for f in normWeights[DSID]})
    
    # Now merge the whole thing
    f = ROOT.TFile.Open(output_file, "RECREATE")
    mergeHistograms(filenames, _normWeights, f, isData)
    mergeTrees(filenames, _normWeights, f, isData)
    f.Close()
   
    return

args = parseArguments(sys.argv[1:])
datasets = readDatasetNames(args)
#pprint.pprint(datasets)

if len(datasets) == 0:
    print("ERROR: no datasets found with your criteria! Exiting.")
    sys.exit()

if os.getenv("ROOTCOREDIR") is None:
    print("ERROR: $ROOTCOREDIR is not set. Did you source rcSetup.sh?")
    sys.exit()

## Load RootCore
ROOT.gROOT.ProcessLine(".x $ROOTCOREDIR/scripts/load_packages.C")
print('Packages loaded')

xsecDB = ROOT.SUSY.CrossSectionDB(args.xsecfile)

print("Processing all datasets")
N = len(datasets)
normWeights = {}
for i, DSID in enumerate(datasets):
    print("{0} / {1} - DSID: {2} - processing {3}".format(i+1, N, DSID, os.path.basename(datasets[DSID])))
    w = processDataset(xsecDB, DSID, datasets[DSID])
    if w is not None:
        normWeights[DSID] = w
    print("")

# NB: it is the user's responsibility to provide a sensible list of inputs, so we merge all that we have. 
# Doesn't make sense? Too bad for you -> run again

mergeDatasets(datasets, normWeights, xsecDB, args.output_file, args.data)
writeTexOutput(normWeights, xsecDB, args.process, args.output_file.replace(".root", ".tex"), args.signal, args.data)

