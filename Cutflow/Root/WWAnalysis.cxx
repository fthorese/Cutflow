// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
// EDM includes:
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include "TLorentzVector.h"
#include "EventLoop/OutputStream.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include "Cutflow/WWAnalysis.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "TH2.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "fastjet/ClusterSequence.hh"
#include "Cutflow/WaveletAnalysis.h"
#include "Cutflow/CutsInxAOD.h"
#include "METUtilities/METMaker.h"
#include "AsgTools/ToolHandle.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJet.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "METUtilities/METSystematicsTool.h"
#include "METUtilities/METMaker.h"
#include "METUtilities/CutsMETMaker.h"
using namespace std;
using namespace fastjet;

// this is needed to distribute the algorithm to the workers
ClassImp(WWAnalysis)
/*
* This class is for making NTuples of MET values.
*/

// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
do {                                                        \
  if( ! EXP.isSuccess() ) {                                 \
   Error( CONTEXT,                                          \
    XAOD_MESSAGE( "Failed to execute: %s" ),                \
                #EXP );                                     \
    return EL::StatusCode::FAILURE;                         \
}                                                           \
} while( false )


WWAnalysis :: WWAnalysis ()
{
}


EL::StatusCode WWAnalysis :: setupJob (EL::Job& job)
{
    job.useXAOD ();
    EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: histInitialize ()
{
    cout << "outputName = " << outputName << endl;

    TFile *outputFile = wk()->getOutputFile (outputName);

    pileupTup = new TNtuple("Pileup", "Pileup","mu:none:none1:none2");
    pileupTup->SetDirectory(outputFile);

    METRefFinal = new TNtuple("MET_Ref_Final", "MET Ref Final","px:py:met:sumet");
    METRefFinal->SetDirectory(outputFile);

    METfinaltrack = new TNtuple("MET_Final_Track", "MET_Final_Track", "px:py:met:sumet");
    METfinalclus = new TNtuple("MET_Final_Clus", "MET_Final_Clus", "px:py:met:sumet");
    METfinalclus->SetDirectory(outputFile);
    METfinaltrack->SetDirectory(outputFile);
    METTruthTree1 = new TNtuple("MET_Truth_Int", "Int","px:py:met:sumet");
    METTruthTree3 = new TNtuple("MET_Truth_Int_and_IntMuons", "Int_and_IntMuons","px:py:met:sumet");
    MET_Truth_NonInt = new TNtuple("MET_Truth_NonInt", "MET_Truth_NonInt","px:py:met:sumet");
    MET_Truth_Unknown = new TNtuple("MET_Truth_Unknown", "MET_Truth_Unknown","px:py:met:sumet");
    MET_Truth_IntMuons= new TNtuple("MET_Truth_IntMuons", "MET_Truth_IntMuons","px:py:met:sumet");

    MET_TruthPileup = new TNtuple("MET_TruthPileup", "MET_TruthPileup","px:py:met:sumet");
    MET_TruthPileup->SetDirectory(outputFile);
    MET_TruthPar_Int = new TNtuple("MET_TruthPar_Int", "MET_TruthPar_Int","px:py:met:sumet");
    MET_TruthPar_NonInt = new TNtuple("MET_TruthPar_NonInt", "MET_TruthPar_NonInt","px:py:met:sumet");

    MET_Truth_NonInt->SetDirectory(outputFile);
    MET_Truth_Unknown->SetDirectory(outputFile);
    MET_Truth_IntMuons->SetDirectory(outputFile);
    MET_TruthPar_Int->SetDirectory(outputFile);
    MET_TruthPar_NonInt->SetDirectory(outputFile);

    mycuts = new TNtuple("mycuts", "mycuts", "mycuts:mycuts1:mycuts2:mycuts3");
    myDenoise = new TNtuple("Ratio_Denoise", "Ratio_Denoise","px:py:met:sumet");
    chRatio = new TNtuple("Charged_Ratio", "Charged_Ratio","px:py:met:sumet");

    myDenoise->SetDirectory(outputFile);
    mycuts->SetDirectory(outputFile);
    METTruthTree1->SetDirectory (outputFile);
    METTruthTree3->SetDirectory (outputFile);
    chRatio->SetDirectory(outputFile);

    crosssection = new TNtuple("cross section","cross section","mu:none:none1:none2");
    crosssection->SetDirectory(outputFile);
    weight = new TNtuple("weight","weight","mu:none:none1:none2");
    weight->SetDirectory(outputFile);

    PFOMET = new TNtuple("PFOs", "Particle Flow MET", "px:py:met:sumet");
    PFOMETwl_denoise = new TNtuple("Flat Denoising", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    PFOMETwl_filter = new TNtuple("Track Filtering", "Particle Flow MET with Wavelets", "px:py:met:sumet");
    PFOMETwl_track = new TNtuple("Track Scaling", "Particle Flow MET with Wavelets", "px:py:met:sumet");

    PFOMET->SetDirectory (outputFile);
    PFOMETwl_denoise->SetDirectory (outputFile);
    PFOMETwl_filter->SetDirectory (outputFile);
    PFOMETwl_track->SetDirectory(outputFile);
    leptontruthtup= new TNtuple("Truth High energy lepton", "Particle Flow MET", "pt:eta:phi:m");
    leptontruthtup->SetDirectory(outputFile);
    lepton = new TNtuple("High energy lepton", "Particle Flow MET", "pt:eta:phi:m");
    jetsTup = new TNtuple("High energy jets", "Particle Flow MET", "pt:eta:phi:m");
    lepton->SetDirectory(outputFile);
    jetsTup->SetDirectory(outputFile);
    truthWW = new TNtuple("WW from Truth Particles", "Particle Flow MET", "pt:eta:phi:m");
    truthWW->SetDirectory(outputFile);
    PFOWW = new TNtuple("WW from PFOs", "Particle Flow MET", "pt:eta:phi:m");
    PFOWWwl_denoise = new TNtuple("WW from PFOs w. Flat Denoising", "Particle Flow MET with Wavelets", "pt:eta:phi:m");
    PFOWWwl_filter = new TNtuple("WW from PFOs w. Track Filtering", "Particle Flow MET with Wavelets", "pt:eta:phi:m");
    PFOWWwl_track = new TNtuple("WW from PFOs w. Track Scaling", "Particle Flow MET with Wavelets", "pt:eta:phi:m");
    PFOWWref = new TNtuple("WW from PFOs w. METRefFinal", "Particle Flow MET with METRefFinal", "pt:eta:phi:m");
    PFOWWref->SetDirectory(outputFile);
    PFOWW->SetDirectory (outputFile);
    PFOWWwl_denoise->SetDirectory (outputFile);
    PFOWWwl_filter->SetDirectory (outputFile);
    PFOWWwl_track->SetDirectory(outputFile);

    cutsplot = new TH1F("Cutflow","Cutflow", 6,0,6);
    cutsplot->GetXaxis()->SetBinLabel( 1 , "Initial" );
    cutsplot->GetXaxis()->SetBinLabel( 2 , "Trigger" );
    cutsplot->GetXaxis()->SetBinLabel( 3 , "MET > 200 GeV" ); 
    cutsplot->GetXaxis()->SetBinLabel( 4 , "1 lepton > 100 GeV" ); 
    cutsplot->GetXaxis()->SetBinLabel( 5 , "2 jets with > 100 GeV" );

    cutsplot->SetDirectory(outputFile);

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: fileExecute ()
{
    cout << "file execute"  << endl;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: changeInput (bool firstFile)
{
    cout << "changeInput" << endl;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: initialize ()
{
    met = new xAOD::MissingETContainer(); 
    metAux = new xAOD::MissingETAuxContainer();
    // Initialize and configure trigger tools

    m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    EL_RETURN_CHECK("hej", m_trigConfigTool->initialize() );
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK("hej",m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK("hej",m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    EL_RETURN_CHECK("hej",m_trigDecisionTool->initialize() );


    // initialize and configure the jet cleaning tool
    m_jetCleaning = new JetCleaningTool("JetCleaning");
    m_jetCleaning->msg().setLevel( MSG::ERROR ); 
    EL_RETURN_CHECK("initialize()", m_jetCleaning->setProperty( "CutLevel", "LooseBad"));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->setProperty("DoUgly", false));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->initialize());

    // count number of events
    m_eventCounter = 0;

    //here ,,FullSim'' means you work with a simulation. ,,Data'' would mean you are working with data. 
    gErrorIgnoreLevel = kError;

    m_susyTool = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD" ); //
    m_susyTool->setProperty("DataSource",ST::SUSYObjDef_xAOD::FullSim); 
    m_susyTool->msg().setLevel( MSG::ERROR ); 

    EL_RETURN_CHECK("initialize()",m_susyTool->initialize());

    EventNumber = 0;

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: execute ()
{
    bool copyEvent = false;
    double trigIsPassed = 0;
    // examine the HLT_xe80* chains, see if they passed/failed and their total prescale
    //    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80.*");
    xAOD::TEvent* event = wk()->xaodEvent();

    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80_L1XE50");

    std::map<std::string,int> triggerCounts;
    for(auto &trig : chainGroup->getListOfTriggers()) {
        auto cg = m_trigDecisionTool->getChainGroup(trig);
        std::string thisTrig = trig;
        Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
        if (cg->isPassed() == 1) {trigIsPassed = 1;}
    }
    vector<TLorentzVector> PFO; 
    vector<TLorentzVector> chPFO;
    vector<TLorentzVector> neuPFO;

    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- EVENT INFORMATION -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    const xAOD::EventInfo* eventInfo = 0;
    EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

    //EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

    // print every 25 events, so we know where we are:
    if( (m_eventCounter % 1) ==0 ) //
        m_eventCounter++;

    EventNumber++;
    cout << "WWAnalysis::execute()  INFO    EventNumber = " << EventNumber << endl;

    double N_pileup = eventInfo->averageInteractionsPerCrossing();
    pileupTup->Fill(N_pileup,0,0,0);

    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- CONTAINERS -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    //Info("execute()", "Getting containers");

    const xAOD::PFOContainer* chPFOs = 0;
    const xAOD::PFOContainer* neuPFOs = 0;
    const xAOD::MissingETContainer* METcontainer = 0;
    const xAOD::VertexContainer* PVs = 0;
    const xAOD::TrackParticleContainer* TrackParticles = 0;
    const xAOD::TauJetContainer* taujets = 0;
    const xAOD::JetContainer* jets = 0;
    const xAOD::JetContainer* fatjets = 0;
    const xAOD::MuonContainer* muons = 0;
    const xAOD::ElectronContainer* electrons = 0;
    const xAOD::PhotonContainer* gammas = 0;
    const xAOD::TruthParticleContainer* TruthParticles = 0;
    const xAOD::TruthEventContainer* xTruthEventContainer = 0;
    //const xAOD::TruthPileupEventContainer* xTruthPileupEventContainer = 0;


    //EL_RETURN_CHECK( "execute()", event->retrieve( xTruthPileupEventContainer, "TruthPileupEvents"));
    EL_RETURN_CHECK( "execute()", event->retrieve( xTruthEventContainer, "TruthEvents"));
    EL_RETURN_CHECK("execute()", event->retrieve( PVs, "PrimaryVertices" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( fatjets, "AntiKt10LCTopoJets" ));
    EL_RETURN_CHECK("execute()", event->retrieve( jets, "AntiKt4EMTopoJets" ));
    EL_RETURN_CHECK("execute()", event->retrieve( taujets,  "TauJets" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( METcontainer, "MET_Truth"));
    EL_RETURN_CHECK("execute()", event->retrieve( chPFOs,  "JetETMissChargedParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( neuPFOs, "JetETMissNeutralParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( TrackParticles, "InDetTrackParticles" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( muons, "Muons" ));
    EL_RETURN_CHECK("execute()", event->retrieve( electrons, "Electrons" ));
    EL_RETURN_CHECK("execute()", event->retrieve(gammas, "Photons"));
    EL_RETURN_CHECK("execute()", event->retrieve(TruthParticles, "TruthParticles"));

    double PVcut = 0;
    xAOD::VertexContainer::const_iterator PV_itr = PVs->begin();
    xAOD::VertexContainer::const_iterator PV_end = PVs->end();
    for ( ; PV_itr != PV_end; ++PV_itr) {
        for (unsigned i = 0; i < (*PV_itr)->nTrackParticles(); i++) {
            if ((*PV_itr)->trackParticle(i)->pt() > 400) { PVcut++; }
        }
        if (PVcut < 5) {PVcut = 0;}
    }

    CutsInxAOD cutAnalyzer = CutsInxAOD((*METcontainer)["NonInt"]->met());

    // Charged
    for (unsigned int i = 0; i < chPFOs->size(); i++) {

        TLorentzVector particle =  TLorentzVector();
        particle.SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(),chPFOs->at(i)->p4().Pz(),chPFOs->at(i)->p4().E());
        particle.SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());

        PFO.push_back(particle);
    }
    // Neutral
    for (unsigned int i = 0; i < neuPFOs->size(); i++) {
        TLorentzVector particle =  TLorentzVector();
        particle.SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(),neuPFOs->at(i)->p4().Pz(),neuPFOs->at(i)->p4().E());
        particle.SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
        PFO.push_back(particle);
    }
    // ---------------------------------------------- Muons --------------------------------------------------
    // ptcut, masscut
    double METcut = 200000;

    // std::vector<TLorentzVector> firstCutMuon = cutAnalyzer.analyzeMuons(muons,200000,0.0,false,0.0);
    // std::vector<TLorentzVector> firstCutElectron = cutAnalyzer.analyzeElectrons(electrons,200000,0.0,false,0.0);
    // std::vector<TLorentzVector> cutJet = cutAnalyzer.analyzeJets(jets,m_jetCleaning,100000,0.0);

    // cutsplot->Fill(0);
    // copyEvent = false;

    // if (trigIsPassed == 1) {copyEvent = true; cutsplot->Fill(1);}        
    // else{copyEvent = false;}

    // if (copyEvent && METvector(PFO)[2] > METcut) {cutsplot->Fill(2);}
    // else{copyEvent = false;}

    // if (copyEvent && (firstCutElectron.size() > 0 || firstCutMuon.size() > 0) ) {cutsplot->Fill(3);}
    // else{copyEvent = false;}

    // if (copyEvent && cutJet.size() > 1) {cutsplot->Fill(4);}
    // else{copyEvent = false;}
    copyEvent = true;
    if (copyEvent == true) {

        vector<TLorentzVector> mu_vector; 
        vector<TLorentzVector> e_vector; 
        vector<TLorentzVector> fatjet_vector; 
        vector<TLorentzVector> tau_vector; 
        // loop over the jets in the container
        xAOD::JetContainer::const_iterator jet_itr = jets->begin();
        xAOD::JetContainer::const_iterator jet_end = jets->end();
        double jet1 = 0;
        double jet2 = 0;
        fatjet_vector.push_back(TLorentzVector());
        fatjet_vector.push_back(TLorentzVector());
        for( ; jet_itr != jet_end; ++jet_itr ) {
            if( !m_jetCleaning->accept( **jet_itr )) continue; //only keep good clean jets
            if ((*jet_itr)->pt() > jet1) {
                if ((*jet_itr)->pt() > jet2) {
                    fatjet_vector.at(1).SetPtEtaPhiM((*jet_itr)->pt(), (*jet_itr)->eta(), (*jet_itr)->phi(), (*jet_itr)->m());
                    jet2 =     (*jet_itr)->pt();           
                }
                else{
                    fatjet_vector.at(0).SetPtEtaPhiM((*jet_itr)->pt(), (*jet_itr)->eta(), (*jet_itr)->phi(), (*jet_itr)->m());               
                    jet1 =     (*jet_itr)->pt();
                }
            }
        }

        // loop over the muons in the container
        xAOD::MuonContainer::const_iterator muon_itr = muons->begin();
        xAOD::MuonContainer::const_iterator muon_end = muons->end();

        mu_vector.push_back(TLorentzVector());

        double muon1 = 0;
        for(; muon_itr != muon_end; ++muon_itr) {
            if ((*muon_itr)->pt() > muon1) {
                mu_vector.back().SetPtEtaPhiM((*muon_itr)->pt(), (*muon_itr)->eta(),(*muon_itr)->phi(),(*muon_itr)->m());
                muon1 = (*muon_itr)->pt();
            }
        }

        // loop over the muons in the container
        xAOD::ElectronContainer::const_iterator e_itr = electrons->begin();
        xAOD::ElectronContainer::const_iterator e_end = electrons->end();
        e_vector.push_back(TLorentzVector());

        double electron1 = 0;
        for (; e_itr != e_end; ++e_itr) {
            if ((*e_itr)->pt() > electron1) {
                e_vector.back().SetPtEtaPhiM((*e_itr)->pt(), (*e_itr)->eta(),(*e_itr)->phi(),(*e_itr)->m());
                electron1 = (*e_itr)->pt();
            }
        }


        met->setStore(metAux);
        m_susyTool->GetMET( *met, jets, electrons, muons, gammas, taujets, true, true, 0);
        // MET SUSY
        xAOD::MissingETContainer::const_iterator met_it = met->find("Final");

        double metEt =(*met_it)->met(); 
        double px = (*met_it)->mpx();
        double py = (*met_it)->mpy();
        double sumet = (*met_it)->sumet();
        //double metPhi = (*met_it)->phi();
        vector<TLorentzVector> Tmet_ref;
        Tmet_ref.push_back(TLorentzVector());
        Tmet_ref.back().SetPxPyPzE(px,py,0,metEt);


        vector<TLorentzVector> truthInt;
        vector<TLorentzVector> truthNonInt;
        vector<TLorentzVector> truthPileup;
        //int nParthg = 0;
        // pileup
        std::vector<TLorentzVector> pileup_vector;  
        std::vector<TLorentzVector> WWtruth_vector;  
        double lepton_pt = 0;
        TLorentzVector leptonvec;

        xAOD::TruthEventContainer::const_iterator itr;
        for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
            const
            //xAOD::TruthVertex* nVert = (*itr)->truthVertex(0);
            //int nvertex = (*itr)->nTruthVertices();
            int nParth = (*itr)->nTruthParticles();
            //nParthg = (*itr)->nTruthParticles();

            crosssection->Fill((*itr)->crossSection(),0,0,0);
            weight->Fill((*itr)->weights().at(0),0,0,0);

            //const xAOD::TruthParticle* nPart = (*itr)->truthParticle(0);

            for (int i = 0; i < nParth; i++) {
                const xAOD::TruthParticle* truth = (*itr)->truthParticle(i);

                TLorentzVector ps = TLorentzVector();
                ps.SetPxPyPzE(truth->p4().Px(), truth->p4().Py(), truth->p4().Pz(), truth->p4().E());
                if (abs(truth->eta()) < 3.2 && truth->status() == 1 && truth->barcode() < 100000 ){

                    if (abs(truth->pdgId()) != 12 && abs(truth->pdgId()) != 14 && abs(truth->pdgId()) != 16) {
                        truthInt.push_back(ps);
                        if (abs(truth->pdgId()) == 24) {
                            WWtruth_vector.push_back(ps);
                        }
                        if (abs(truth->pdgId()) == 11 || abs(truth->pdgId()) == 13){
                            if (truth->p4().Pt() > lepton_pt){
                                lepton_pt = truth->p4().Pt();
                                leptonvec = ps;
                            }
                        }
                    }              
                    else {
                        truthNonInt.push_back(ps);
                    }
                }
                if (abs(truth->eta()) < 3.2 && truth->status() == 1 && truth->barcode() > 100000){
                    truthPileup.push_back(ps);
                }
            }
        }
        leptontruthtup->Fill(leptonvec.Pt());

        for (int k = 0; k < WWtruth_vector.size(); k++){
            truthWW->Fill(WWtruth_vector.at(k).Pt(), WWtruth_vector.at(k).Px(),WWtruth_vector.at(k).Py(),WWtruth_vector.at(k).M());
        }

        MET_TruthPileup->Fill(METvector(truthPileup)[0], METvector(truthPileup)[1], METvector(truthPileup)[2], METvector(truthPileup)[3]);
        
        METRefFinal->Fill(px,py,metEt,sumet);

        vector<TLorentzVector> chc1;
        vector<TLorentzVector> neuc1;
        vector<TLorentzVector> PFOc1;

        // matching tracks with PVs
        vector<TLorentzVector> track_PV; 
        vector<TLorentzVector> track_All; 
        vector<TLorentzVector> track_NonPV;

        for (const xAOD::TrackParticle* track : *TrackParticles) {
            bool match = true;
            xAOD::VertexContainer::const_iterator PV_itr = PVs->begin();
            xAOD::VertexContainer::const_iterator PV_end = PVs->end();
            PV_itr++;

            for ( ; PV_itr != PV_end; ++PV_itr) {
                for (unsigned i = 0; i < (*PV_itr)->nTrackParticles(); i++) {
                    if (track == (*PV_itr)->trackParticle(i)) { match = false; break; }
                }
                if (match == false) { break; }
            }
            if (abs(track->eta()) < 3.2) {
                TLorentzVector ps = TLorentzVector();
                ps.SetPxPyPzE(track->p4().Px(), track->p4().Py(), track->p4().Pz(), track->p4().E());
                ps.SetPtEtaPhiM(track->pt(), track->eta(), track->phi(), track->m());

                if (match) { 
                    track_PV.push_back( ps );
                }
                else { 
                    track_NonPV.push_back( ps ); 
                }
                track_All.push_back(ps); 
            }
        }

        // -- PFlow objects.
        vector<TLorentzVector> chPFO_PV; 
        vector<TLorentzVector> chPFO_NonPV;

        // Charged
        for (unsigned int i = 0; i < chPFOs->size(); i++) {
            bool match = false;
            if (abs(chPFOs->at(i)->eta()) < 3.2) {
                TLorentzVector particle =  TLorentzVector();
                particle.SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(),chPFOs->at(i)->p4().Pz(),chPFOs->at(i)->p4().E());
                particle.SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());

                for (size_t i = 0; i < track_PV.size(); i++) {
                    if (dR(particle, track_PV.at(i)) < 0.005) { match = true; break; }
                }
                if (abs(chPFOs->at(i)->eta()) < 3.2 ) { chc1.push_back(particle); PFOc1.push_back(particle);}
                if (match) { chPFO_PV   .push_back( particle ); }
                else       { chPFO_NonPV.push_back( particle ); }
                chPFO.push_back(particle);
                PFO.push_back(particle);
            }
        }
        // Neutral
        for (unsigned int i = 0; i < neuPFOs->size(); i++) {
            if (abs(neuPFOs->at(i)->eta()) < 3.2 ) {

                TLorentzVector particle =  TLorentzVector();
                particle.SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(),neuPFOs->at(i)->p4().Pz(),neuPFOs->at(i)->p4().E());
                particle.SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());

                neuPFO.push_back(particle);
                PFO.push_back(particle);
                if (neuPFOs->at(i)->eta() < 3.2 && neuPFOs->at(i)->eta() > -3.2) {neuc1.push_back(particle);PFOc1.push_back(particle);}
            }
        }

        //-------------------------------------------------------------------------------------------------------
        //------------------------------------------------ MY METHOD --------------------------------------------------
        //-------------------------------------------------------------------------------------------------------
        //double mydenoisecutneu = 1000.0;
        //double neuPFO_PV_size = 1000*neuPFO.size();
        vector<TLorentzVector> neuPFO_PV_temp;
        vector<TLorentzVector> chPFO_PV_temp;

        vector<TLorentzVector> PFO_PV_temp;
        for (unsigned int i = 0; i < neuPFO_PV_temp.size(); i++){
            PFO_PV_temp.push_back(neuPFO_PV_temp.at(i));
        }
        for (unsigned int i = 0; i < chPFO_PV.size(); i++){
            PFO_PV_temp.push_back(chPFO_PV.at(i));
        }

        //-------------------------------------------------------------------------------------------------------
        //---------------------------------------------- WAVELET --------------------------------------------------
        //-------------------------------------------------------------------------------------------------------

        double filtercut = 0.2;
        double denoisecut =4500;

        vector<TLorentzVector> METwl_filter_temp =       WaveletAnalysis(64,3.2,N_pileup, PFO).trackfilter(PFO, chPFO_PV, filtercut).particles();
        vector<TLorentzVector> METwl_track_temp =        WaveletAnalysis(64,3.2,N_pileup, PFO).trackscaling(track_All,track_PV).particles();
        vector<TLorentzVector> METwl_denoise_temp =      WaveletAnalysis(64,3.2,N_pileup, PFO).denoise(denoisecut).particles();
        
        //-------------------------------------------------------------------------------------------------------
        //------------------------------------------------ MET --------------------------------------------------
        //-------------------------------------------------------------------------------------------------------

        PFOMETwl_denoise->  Fill(METvector(METwl_denoise_temp)[0],METvector(METwl_denoise_temp)[1],METvector(METwl_denoise_temp)[2],METvector(METwl_denoise_temp)[3]);
        PFOMETwl_filter->   Fill(METvector(METwl_filter_temp)[0],METvector(METwl_filter_temp)[1],METvector(METwl_filter_temp)[2],METvector(METwl_filter_temp)[3]);
        PFOMETwl_track-> Fill(METvector(METwl_track_temp)[0],METvector(METwl_track_temp)[1],METvector(METwl_track_temp)[2],METvector(METwl_track_temp)[3]);
        METTruthTree1->Fill((*METcontainer)["Int"]->mpx(),(*METcontainer)["Int"]->mpy(),(*METcontainer)["Int"]->met(),(*METcontainer)["Int"]->sumet());
        METTruthTree3->Fill((*METcontainer)["Int"]->mpx() + (*METcontainer)["IntMuons"]->mpx(),(*METcontainer)["Int"]->mpy() + (*METcontainer)["IntMuons"]->mpy(),(*METcontainer)["Int"]->met() + (*METcontainer)["IntMuons"]->met(),(*METcontainer)["Int"]->sumet() + (*METcontainer)["IntMuons"]->sumet());
        PFOMET->Fill(METvector(PFO)[0],METvector(PFO)[1],METvector(PFO)[2],METvector(PFO)[3]);

        MET_Truth_NonInt->Fill((*METcontainer).at(0)->mpx(), (*METcontainer).at(0)->mpy(),(*METcontainer).at(0)->met(), (*METcontainer).at(0)->sumet());
        MET_Truth_Unknown->Fill((*METcontainer).at(2)->mpx(), (*METcontainer).at(2)->mpy(),(*METcontainer).at(2)->met(), (*METcontainer).at(2)->sumet());
        MET_Truth_IntMuons->Fill((*METcontainer).at(3)->mpx(), (*METcontainer).at(3)->mpy(),(*METcontainer).at(3)->met(), (*METcontainer).at(3)->sumet());
        MET_TruthPar_Int->Fill(METvector(truthInt)[0], METvector(truthInt)[1],METvector(truthInt)[2], METvector(truthInt)[3]);
        MET_TruthPar_NonInt->Fill(METvector(truthNonInt)[0], METvector(truthNonInt)[1],METvector(truthNonInt)[2], METvector(truthNonInt)[3]);

        vector<TLorentzVector> Tmet_track;
        Tmet_track.push_back(TLorentzVector());
        Tmet_track.back().SetPxPyPzE(METvector(METwl_track_temp)[0],METvector(METwl_track_temp)[1],0,METvector(METwl_track_temp)[2]);
        vector<TLorentzVector> Tmet;
        Tmet.push_back(TLorentzVector());
        Tmet.back().SetPxPyPzE(METvector(PFO)[0],METvector(PFO)[1],0,METvector(PFO)[2]);

        vector<TLorentzVector> Tmet_filter;
        Tmet_filter.push_back(TLorentzVector());
        Tmet_filter.back().SetPxPyPzE(METvector(METwl_filter_temp)[0],METvector(METwl_filter_temp)[1],0,METvector(METwl_filter_temp)[2]);

        vector<TLorentzVector> Tmet_denoise;
        Tmet_denoise.push_back(TLorentzVector());
        Tmet_denoise.back().SetPxPyPzE(METvector(METwl_denoise_temp)[0],METvector(METwl_denoise_temp)[1],0,METvector(METwl_denoise_temp)[2]);

        if (e_vector.at(0).Pt() > mu_vector.at(0).Pt()) {
            lepton->Fill(e_vector.at(0).Pt(), e_vector.at(0).Eta(), e_vector.at(0).Phi(), e_vector.at(0).M());
            PFOWW->Fill((e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).Pt(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).Eta(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).Phi(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).M()
                );
            PFOWWwl_track->Fill((e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).Pt(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).Eta(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).Phi(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).M()
                );
            PFOWWwl_filter->Fill((e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).Pt(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).Eta(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).Phi(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).M());
            PFOWWwl_denoise->Fill((e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Pt(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Eta(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Phi(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).M());
            PFOWWref->Fill((e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Pt(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).Eta(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).Phi(),
                (e_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).M());
            //cout << "neutrino mass" << Tmet_ref.at(0).M() << endl;
        }
        else {
            lepton->Fill(mu_vector.at(0).Pt(), mu_vector.at(0).Eta(), mu_vector.at(0).Phi(), mu_vector.at(0).M());       
            jetsTup->Fill((fatjet_vector.at(0) + fatjet_vector.at(1)).Pt(), (fatjet_vector.at(0) + fatjet_vector.at(1)).Eta(), (fatjet_vector.at(0) + fatjet_vector.at(1)).Phi(), (fatjet_vector.at(0) + fatjet_vector.at(1)).M());       

            PFOWW->Fill((mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).Pt(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).Eta(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).Phi(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet.at(0)).M()
                );
            PFOWWwl_track->Fill((mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).Pt(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).Eta(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).Phi(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_track.at(0)).M()
                );
            PFOWWwl_filter->Fill((mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).Pt(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).Eta(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).Phi(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_filter.at(0)).M());
            PFOWWwl_denoise->Fill((mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Pt(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Eta(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).Phi(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_denoise.at(0)).M());
            PFOWWref->Fill((mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).Pt(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).Eta(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).Phi(),
                (mu_vector.at(0) + fatjet_vector.at(0) + fatjet_vector.at(1) + Tmet_ref.at(0)).M());
        }
    }
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: postExecute ()
{
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: finalize ()
{
    if (m_susyTool) {
        delete m_susyTool;
        m_susyTool = 0;
    }
    if( m_jetCleaning ) {
        delete m_jetCleaning;
        m_jetCleaning = 0;
    }
    if( met ) {
        delete met;
        met = 0;
    }
    if( metAux ) {
        delete metAux;
        met = 0;
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWAnalysis :: histFinalize ()
{
    return EL::StatusCode::SUCCESS;
}

TLorentzVector WWAnalysis :: sum(std::vector<TLorentzVector> v) {
    TLorentzVector sum;
    for (unsigned int i = 0; i < v.size(); i++) {
        sum += v.at(i);
    }
    return sum;
} 

double WWAnalysis :: dR(TLorentzVector U, TLorentzVector V) {
    float deta = fabs( U.Eta() - V.Eta() );
    float dphi = fabs( U.Phi() - V.Phi() );
    if (dphi > pi) dphi = 2.*pi - dphi;
    return sqrt( deta*deta + dphi*dphi );
}

std::vector<double> WWAnalysis :: METvector (std::vector<TLorentzVector> v) {
    double metx = 0; 
    double mety = 0;
    double summetx = 0; 
    double summety = 0;
    double sumET=0.;
    std::vector<double> u;
    for (unsigned int i = 0; i < v.size(); i++){
        metx += v.at(i).Px();
        sumET += v.at(i).Et();
        summetx += (v.at(i).Px()*v.at(i).Px());
        summety += (v.at(i).Py()*v.at(i).Py());
        //sumET += sqrt(v.at(i).Py()*v.at(i).Py() + v.at(i).Px()*v.at(i).Px());

        mety += v.at(i).Py();
    }
    u.push_back(-metx);
    u.push_back(-mety);
    u.push_back(sqrt(metx*metx + mety*mety));
    u.push_back(sumET);
    //u.push_back(sqrt(summety + summety));
    return u;
}

double WWAnalysis :: length(TLorentzVector U) {
    return sqrt(U.Px()*U.Px() + U.Py()*U.Py());
}