// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include <TTree.h>
#include "TFile.h"
#include <vector>
#include "TLorentzVector.h"
#include "EventLoop/OutputStream.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include "Cutflow/WWboildown.h"
#include "Cutflow/CutsInxAOD.h"
#include "xAODMuon/MuonContainer.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include <TSystem.h>
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include <fstream>
using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(WWboildown)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
do {                                                        \
  if( ! EXP.isSuccess() ) {                                 \
   Error( CONTEXT,                                        \
    XAOD_MESSAGE( "Failed to execute: %s" ),            \
                #EXP );                                     \
    return EL::StatusCode::FAILURE;                     \
}                                                       \
} while( false )


WWboildown :: WWboildown ()
{
}



EL::StatusCode WWboildown :: setupJob (EL::Job& job)
{
    // tell EventLoop about our output xAOD:
    EL::OutputStream out ("outputLabel", "xAOD");
    job.outputAdd (out);

    job.useXAOD ();

    EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWboildown :: histInitialize ()
{
    TFile *outputFile = wk()->getOutputFile (outputName);
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWboildown :: fileExecute ()
{
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWboildown :: changeInput (bool firstFile)
{
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWboildown :: initialize ()
{
    xAOD::TEvent* event = wk()->xaodEvent();
    // output xAOD
    TFile *file = wk()->getOutputFile ("outputLabel");
    EL_RETURN_CHECK("initialize()",event->writeTo(file));
    cutsNTuple = new TNtuple("Cuts", "Cuts","Cuts");

    cutsplot = new TH1F("Cutflow","Cutflow", 6,0,6);
    cutsplot->GetXaxis()->SetBinLabel( 1 , "Initial" );
    cutsplot->GetXaxis()->SetBinLabel( 2 , "Trigger" );
    cutsplot->GetXaxis()->SetBinLabel( 3 , "MET > 200 GeV" ); 
    cutsplot->GetXaxis()->SetBinLabel( 4 , "1 lepton > 100 GeV" ); 
    cutsplot->GetXaxis()->SetBinLabel( 5 , "2 jets with > 100 GeV" );

    //cutsplot->GetXaxis()->SetBinLabel( 7 , "Taujet passed" ); 

    // B-tagging for 70 percent background efficiency
    btag_70 = new BTaggingSelectionTool("btag_70");
    btag_70->setProperty( "MaxEta", 2.5 );
    btag_70->setProperty( "MinPt", 25000. ); 
    btag_70->setProperty( "FlvTagCutDefinitionsFileName","xAODBTaggingEfficiency/cutprofiles_22072015.root" );
    btag_70->setProperty( "TaggerName",     "MV2c20"  );
    btag_70->setProperty( "OperatingPoint", "FixedCutBEff_70"   );
    btag_70->setProperty( "JetAuthor",      "AntiKt4EMTopoJets" );
    btag_70->initialize() ;


    wk()->addOutput(cutsplot);

    // Initialize and configure trigger tools

    m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    EL_RETURN_CHECK("hej", m_trigConfigTool->initialize() );
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK("hej",m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK("hej",m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    EL_RETURN_CHECK("hej",m_trigDecisionTool->initialize() );


    // initialize and configure the jet cleaning tool
    m_jetCleaning = new JetCleaningTool("JetCleaning");
    m_jetCleaning->msg().setLevel( MSG::ERROR ); 
    EL_RETURN_CHECK("initialize()", m_jetCleaning->setProperty( "CutLevel", "LooseBad"));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->setProperty("DoUgly", false));
    EL_RETURN_CHECK("initialize()",m_jetCleaning->initialize());

    // count number of events
    m_eventCounter = 0;

    // as a check, let's see the number of events in our xAOD
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

    //const xAOD::EventInfo* eventInfo = 0;

    // check if the event is MC
    //if(!eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

            /*
        m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
        const char* GRLFilePath = "share/GRLs";
        const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath);
        std::vector<std::string> vecStringGRL;
        vecStringGRL.push_back(fullGRLFilePath);
        EL_RETURN_CHECK("initialize()",m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
        EL_RETURN_CHECK("initialize()",m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
        EL_RETURN_CHECK("initialize()",m_grl->initialize());
        */
    //}

    m_fileMetaDataTool = new xAODMaker::FileMetaDataTool();
    EL_RETURN_CHECK("MinixAOD::initialize()", m_fileMetaDataTool->initialize());
    Info("initialize()", "FileMetaDataTool initialized...");

    m_trigMetaDataTool = new xAODMaker::TriggerMenuMetaDataTool();
    EL_RETURN_CHECK("MinixAOD::initialize()", m_trigMetaDataTool->initialize());
    Info("initialize()", "TriggerMenuMetaDataTool initialized...");
    std::cout << "Adding xTrigDecision and TrigConfKeys to the list of keys copied from the input file." << std::endl;
        return EL::StatusCode::SUCCESS;
    }



    EL::StatusCode WWboildown :: execute ()
    {

        double trigIsPassed = 0;
    // examine the HLT_xe80* chains, see if they passed/failed and their total prescale
    //    auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80.*");
        
        auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_xe80_L1XE50");

        std::map<std::string,int> triggerCounts;
        for(auto &trig : chainGroup->getListOfTriggers()) {
          auto cg = m_trigDecisionTool->getChainGroup(trig);
          std::string thisTrig = trig;
          Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
          if (cg->isPassed() == 1) {trigIsPassed = 1;}
    } // end for loop (c++11 style) over chain group matching "HLT_xe80*" 

    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- EVENT INFORMATION -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    bool copyEvent = false;
    xAOD::TEvent* event = wk()->xaodEvent();

    // print every 100 events, so we know where we are:
    if( (m_eventCounter % 1) ==0 ) //Info("execute()", "Event number = %i", m_eventCounter );
    m_eventCounter++;

    const xAOD::EventInfo* eventInfo = 0;
     //const xAOD::EventInfo* eventInfo = 0;
    EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));
    double N_pileup = eventInfo->averageInteractionsPerCrossing();

    //bool isMC = false;
    // check if the event is MC
    //if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     //   isMC = true; // can do something with this later
    //}

    
    Info("execute()", " ");
    Info("execute()", "=====================");
    Info("execute()", " * Event number %4d", m_eventCounter);
    Info("execute()", "---------------------");
    

    // check if the event is data or MC
    // (many tools are applied either to data or MC)
    bool isMC = false;
    // check if the event is MC
    if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
        isMC = true; // can do something with this later
        cout << "------ Data is from MC" << endl;
    }
    else {
        cout << "------ Data is from raw data" << endl;
    }

    // if data check if event passes GRL
    /*
    if(!isMC){ // it's data!
    if(!m_grl->passRunLB(*eventInfo)){
        //return EL::StatusCode::SUCCESS; // go to next event
        cout << "This is not in GoodRunsList" << endl;
        }
    } // end if not MC
    */

    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- CONTAINERS --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    cout << "Getting containers" << endl;

    const xAOD::VertexContainer* PVs = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( PVs, "PrimaryVertices" ) );

    const xAOD::TauJetContainer* taujets = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( taujets,  "TauJets" ) );

    const xAOD::MissingETContainer* METcontainer = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( METcontainer, "MET_Truth"));

    const xAOD::JetContainer* jets = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( jets, "AntiKt4LCTopoJets" ));

    const xAOD::MuonContainer* muons = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( muons, "Muons" ));

    const xAOD::ElectronContainer* electrons = 0;
    EL_RETURN_CHECK("execute()",event->retrieve( electrons, "Electrons" ));
    
    const xAOD::JetContainer* fatjets = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( fatjets, "AntiKt10LCTopoJets" ));

    const xAOD::PFOContainer* chPFOs = 0;
    const xAOD::PFOContainer* neuPFOs = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( chPFOs,  "JetETMissChargedParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( neuPFOs, "JetETMissNeutralParticleFlowObjects" ) );


    double PVcut = 0;
        xAOD::VertexContainer::const_iterator PV_itr = PVs->begin();
        xAOD::VertexContainer::const_iterator PV_end = PVs->end();
    for ( ; PV_itr != PV_end; ++PV_itr) {
        for (unsigned i = 0; i < (*PV_itr)->nTrackParticles(); i++) {
            if ((*PV_itr)->trackParticle(i)->pt() > 400) { PVcut++; }
        }
        if (PVcut < 5) {PVcut = 0;}
    }

    CutsInxAOD cutAnalyzer = CutsInxAOD((*METcontainer)["NonInt"]->met());

    std::vector<TLorentzVector> PFO;
    cout << "PFO fill" << endl;

    // Charged
    for (unsigned int i = 0; i < chPFOs->size(); i++) {

        TLorentzVector particle =  TLorentzVector();
        particle.SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(),chPFOs->at(i)->p4().Pz(),chPFOs->at(i)->p4().E());
        particle.SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());

        PFO.push_back(particle);
    }
    // Neutral
    for (unsigned int i = 0; i < neuPFOs->size(); i++) {
        TLorentzVector particle =  TLorentzVector();
        particle.SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(),neuPFOs->at(i)->p4().Pz(),neuPFOs->at(i)->p4().E());
        particle.SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
        PFO.push_back(particle);
    }
    // ---------------------------------------------- Muons --------------------------------------------------
    // ptcut, masscut
    double METcut = 200000;
    cout << "MET PFO = " << METvector(PFO)[2] << endl;
    cout << "Cutflow -- muon" << endl;

    std::vector<TLorentzVector> firstCutMuon = cutAnalyzer.analyzeMuons(muons,100000,0.0,false,0.0);
    //std::vector<TLorentzVector> secondCutMuon = cutAnalyzer.analyzeMuons(muons,36000,0.0,false,0.0);
    //std::vector<TLorentzVector> thirdCutMuon = cutAnalyzer.analyzeMuons(muons,36000,0.0,true,2.5);

    cout << "Cutflow -- electron" << endl;
    std::vector<TLorentzVector> firstCutElectron = cutAnalyzer.analyzeElectrons(electrons,100000,0.0,false,0.0);
    //std::vector<TLorentzVector> secondCutElectron = cutAnalyzer.analyzeElectrons(electrons,60000,0.0,false,0.0);
    //std::vector<TLorentzVector> thirdCutElectron = cutAnalyzer.analyzeElectrons(electrons,36000,0.0,true,0.0);

    cout << "Cutflow -- jet" << endl;
    std::vector<TLorentzVector> cutJet = cutAnalyzer.analyzeJets(jets,m_jetCleaning,100000,0.0);

    cutsplot->Fill(0);
    copyEvent = false;

    if (trigIsPassed == 1) {copyEvent = true;passTrigger++; cutsplot->Fill(1);}        
    else{copyEvent = false;}

    if (copyEvent && METvector(PFO)[2] > METcut) {cutsplot->Fill(2);cout << "---- METcut " << endl;}
    else{copyEvent = false;}

    if (copyEvent && firstCutElectron.size() > 0 || firstCutMuon.size() > 0 ) {cutsplot->Fill(3);cout << "---- Lepton cut " << endl;}
    else{copyEvent = false;}

    if (copyEvent && cutJet.size() > 1) {cutsplot->Fill(4);cout << "---- Jet cut " << endl;}
    else{copyEvent = false;}
   

    if (copyEvent == true) {
        cout << "copying event " << m_eventCounter << endl;
        string line;

        string out = gSystem->ExpandPathName( "$ROOTCOREBIN" );

        ifstream myfile (out + "/../Cutflow/share/" + "output.txt");
        if (myfile.is_open())
        {
            cout << "   file is open" << endl;
            while ( getline (myfile,line) )
            {
                if (event->copy(line).isSuccess()) {
                    //cout << "reading line.." << endl;
                    //cout << line << endl;
                    //cout << typeid(line).name() << endl;
                    EL_RETURN_CHECK("execute()",event->copy(line));
                }
            }
            myfile.close();
            cout << "   file is closed" << endl;
            event->fill();
        }
        else cout << "Unable to open file"; 
    }
    else {
        //cout << "event not copied" << endl;
    }

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode WWboildown :: postExecute ()
{

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode WWboildown :: finalize ()
{
    xAOD::TEvent* event = wk()->xaodEvent();
    /*   
    if (m_grl) {
    delete m_grl;
    m_grl = 0;
    }
    */
    if(m_fileMetaDataTool) delete m_fileMetaDataTool;
    if(m_trigMetaDataTool) delete m_trigMetaDataTool;
    // finalize and close our output xAOD file:
    TFile *file = wk()->getOutputFile ("outputLabel");
    EL_RETURN_CHECK("finalize()",event->finishWritingTo( file ));

    cout << "-----------------------------------------------------------" << endl;
    cout << "----------------------- CUTS ------------------------------" << endl;
    cout << "-----------------------------------------------------------" << endl;
    cout << "  " << endl;
    cout << "pass trigger: " << passTrigger << endl;
    cout << "e, mu > 50 GeV: " << passmuonenergy << endl;
    cout << "lv > 200 GeV: " << passWenergy << endl;
    cout << "  " << endl;
    cout << "-----------------------------------------------------------" << endl;
    // cleaning up trigger tools
    /*
    if( m_trigConfigTool ) {
      delete m_trigConfigTool;
      m_trigConfigTool = 0;
  }
  if( m_trigDecisionTool ) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = 0;
  }*/
      if( m_jetCleaning ) {
        delete m_jetCleaning;
        m_jetCleaning = 0;
    }

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode WWboildown :: histFinalize ()
{
    return EL::StatusCode::SUCCESS;
}


std::vector<double> WWboildown :: METvector (std::vector<TLorentzVector> v) {
    double metx = 0; 
    double mety = 0;
    double summetx = 0; 
    double summety = 0;
    double sumET=0.;
    std::vector<double> u;
    for (unsigned int i = 0; i < v.size(); i++){
        metx += v.at(i).Px();
        sumET += v.at(i).Et();
        summetx += (v.at(i).Px()*v.at(i).Px());
        summety += (v.at(i).Py()*v.at(i).Py());
        //sumET += sqrt(v.at(i).Py()*v.at(i).Py() + v.at(i).Px()*v.at(i).Px());

        mety += v.at(i).Py();
    }
    u.push_back(-metx);
    u.push_back(-mety);
    u.push_back(sqrt(metx*metx + mety*mety));
    u.push_back(sumET);
    //u.push_back(sqrt(summety + summety));
    return u;
}