#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "Cutflow/Boildown.h"
#include "Cutflow/WWAnalysis.h"
#include <fstream> 
#include <ctime>
using namespace std;

int main( int argc, char* argv[]) {
char gridon = '1';
    // Take the submit directory from the input if provided:
    std::string submitDir = "runs/test5";
    if( argc > 1 ){ submitDir = argv[ 1 ];}

    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on:
    SH::SampleHandler sh;
    cout << "hellos" << endl;
    cout << "argv[1] " << argv[1] << endl;
    cout << "argv[2] " << argv[2] << endl;
    // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
   // for grid

    if (*argv[2] == gridon) {
        //SH::scanDQ2(sh, "mc15_13TeV.303227.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_WW_lvqq_c10_m0800.merge.AOD.e4175_s2608_r6869_r6282/");

      cout << "reading files..." << endl;
        std::ifstream infile("/hep/thoresen/work/april/Cutflow/Cutflow/util/sampleLists/samplesGrid.txt");
        std::string sample;
        while (std::getline(infile, sample)) {
            cout << "in da file!" << endl;
            cout << sample.c_str() << endl;
            SH::scanDQ2 (sh, sample.c_str());
            cout << sample.c_str() << endl;
        }
    }
    else {
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/work/Cutflow/runs/36/data-outputLabel/");
        //SH::ScanDir().filePattern("mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.merge.AOD.e4054_s2608_s2183_r6630_r6264.root").scan(sh, inputFilePath);
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/work/JanCutflow/Cutflow/");
        //SH::ScanDir().filePattern("user.fthorese.Searching_for_Z.TEST.lol.341314.e3940_s2608_s2183_r6869_r6282_outputLabel.root").scan(sh, inputFilePath);

        //const char *inputFilePath = gSystem->ExpandPathName("/hep/storage/thoresen/");
        //SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
//mc15_13TeV.301256.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m800.merge.AOD.e3743_s2608_s2183_r7380_r6282
//WWdata/mc15_13TeV.303227.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_WW_lvqq_c10_m0800.merge.AOD.e4175_s2608_r6869_r6282
//mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7380_r6282

/*mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2630_s2206_r7709
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2630_s2206_r7768
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2630_s2206_r7769
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7699
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7700
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2638_s2206_r7701
mc15_14TeV.407099.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_MET100.recon.AOD.e4906_s2640_s2206_r7702
*/

       /* std::ifstream infile("/hep/thoresen/work/april/Cutflow/Cutflow/util/sampleLists/samples2.txt");
        std::string sample;
        while (std::getline(infile, sample)) {
            cout << "in da file!" << endl;
            const char *inputFilePath = gSystem->ExpandPathName(sample.c_str());
            SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
            cout << sample.c_str() << endl;
        }*/

        const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/data/HWWlvqq/mc15_13TeV.341042.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH800NWA_WWlvqq_15_5.merge.AOD.e3944_s2608_s2183_r6869_r6282/");
        SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
        //const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/work/JanCutflow/Cutflow/runs/DAOD_VBF1000H_ZZllqq/data-outputLabel/");
        //SH::ScanDir().filePattern("mc15_13TeV.341339.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH1000NW_ZZllqq.merge.AOD.e3940_s2608_s2183_r6765_r6282.root").scan(sh, inputFilePath);
    }
    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );

    // Print what we found:
    sh.print();

    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler( sh );
    job.options()->setDouble (EL::Job::optMaxEvents, 5000);
    job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);

    // Add our analysis to the job:
    //GridMETAnalysis* alg = new GridMETAnalysis();
    WWAnalysis* alg = new WWAnalysis();
    job.algsAdd( alg );

    // define an output and an ntuple associated to that output
    EL::OutputStream output("search_out");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("search_out");
    job.algsAdd (ntuple);
    alg->outputName = "search_out"; // give the name of the output to our algorithm


    //string rawtime = ctime(0);
    //cout << rawtime << endl;

    // for grid
    if (*argv[2] == gridon) {
        EL::PrunDriver driver;
        driver.options()->setString("nc_outputSampleName",
                                    "user.fthorese.08.17.WW.%in:name[2]%.%in:name[6]%");
        driver.submitOnly( job, submitDir );
    }
    else {
        EL::DirectDriver driver;
        driver.options()->setString("nc_outputSampleName",
                                    "user.fthorese.Searching_for_Z.%in:name[2]%.%in:name[6]%");
        driver.submit( job, submitDir );
    }

    return 0;
}