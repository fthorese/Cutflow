#ifndef Cutflow_WaveletAnalysis_H
#define Cutflow_WaveletAnalysis_H
#include "TObject.h"
#include "TLorentzVector.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include <vector>
#include "xAODJet/JetContainer.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "NewWave/WaveletCoefficient.hh"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
using namespace std;
class WaveletAnalysis  { // : public TObject

    private:
        NewWave::PixelDefinition _pixelDefinition; //!
        NewWave::GSLEngine _waveletEngine; //!
        
        vector<TLorentzVector> fjPFlow; //!
        vector<TLorentzVector> fjTrackPV; //!
        vector<TLorentzVector> fjTrackAll; //!

        vector<TLorentzVector> PFOwl; //!
        vector<NewWave::WaveletCoefficient> coef; //!
        vector<TLorentzVector> PFOs;//!
        double pileup; //!
        static Double_t jm(Double_t *x,Double_t *par);
        std::vector<double> allalphas;//!

    public:
        // constructor
        std::vector<double> p; //!

        double minimize_jm(double modelCoef, double coef, double alpha, double sigma);
        WaveletAnalysis(int nPixel, double yRange, double N_pileup, std::vector<TLorentzVector> myPFOs);
        NewWave::WaveletEvent<vector<TLorentzVector>> entropy(vector<TLorentzVector> model, vector<TLorentzVector> data);

        std::vector<double> getAlphas();

        NewWave::WaveletEvent<vector<TLorentzVector>> trackscaling(std::vector<TLorentzVector> fjTrackAll, std::vector<TLorentzVector> fjTrackPV);
        NewWave::WaveletEvent<vector<TLorentzVector>> trackfilter(std::vector<TLorentzVector> fjTrackAll, std::vector<TLorentzVector> fjTrackPV, double cut);
        NewWave::WaveletEvent<vector<TLorentzVector>> mydenoise();
        NewWave::WaveletEvent<vector<TLorentzVector>> denoise(double cut);

};

#endif