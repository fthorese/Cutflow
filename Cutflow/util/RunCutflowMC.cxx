#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "SampleHandler/ScanDir.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "Cutflow/Boildown.h"
#include "Cutflow/WWboildown.h"

using namespace std;

int main( int argc, char* argv[]) {
char gridon = '1';
    // Take the submit directory from the input if provided:
    std::string submitDir = "submitDir";
    if( argc > 1 ) submitDir =  argv[1];

    // Set up the job for xAOD access:
    xAOD::Init().ignore();

    // Construct the samples to run on:
    SH::SampleHandler sh;

    // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:

    // for grid
    if (*argv[2] == gridon) {
        
         cout << "reading files..." << endl;
        //SH::scanDQ2(sh, "mc15_13TeV.303227.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_WW_lvqq_c10_m0800.merge.AOD.e4175_s2608_r6869_r6282/");

        std::ifstream infile("/hep/thoresen/work/april/Cutflow/Cutflow/util/sampleLists/sampletest.txt");
        std::string sample;
        while (std::getline(infile, sample)) {
            cout << "in da file!" << endl;
            SH::scanDQ2 (sh, sample.c_str());
            cout << sample.c_str() << endl;
        }

    }
    else {
        const char *inputFilePath = gSystem->ExpandPathName("/hep/thoresen/data/WWdata/mc15_13TeV.303227.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_WW_lvqq_c10_m0800.merge.AOD.e4175_s2608_r6869_r6282/");
        SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
    }

    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );

    // Print what we found:
    sh.print();

    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler( sh );
    //job.options()->setDouble (EL::Job::optMaxEvents, 10);

    // Add our analysis to the job:
    WWboildown* alg = new WWboildown();
    job.algsAdd( alg );

    // define an output and an ntuple associated to that output
    EL::OutputStream output("search_out");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("search_out");
    job.algsAdd (ntuple);
    alg->outputName = "search_out"; // give the name of the output to our algorithm

    // for grid
    if (*argv[2] == gridon) {
        EL::PrunDriver driver;
        driver.options()->setString("nc_outputSampleName",
                                    "user.fthorese.WW.08.12.%in:name[2]%.%in:name[6]%");
        driver.submitOnly( job, submitDir );
    }
    else {
        EL::DirectDriver driver;
        driver.options()->setString("nc_outputSampleName",
                                    "user.fthorese.ggH1800NWA_WWlvqq.%in:name[2]%.%in:name[6]%");
        driver.submit( job, submitDir );
    }

    return 0;
}