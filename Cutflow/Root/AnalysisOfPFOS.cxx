// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include <TTree.h>
#include "TFile.h"
#include <vector>
#include "TLorentzVector.h"
#include "EventLoop/OutputStream.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFO.h"
#include "Cutflow/AnalysisOfPFOS.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "xAODMissingET/MissingETContainer.h"
#include "TH2.h"
#include "fastjet/ClusterSequence.hh"
#include "Cutflow/WaveletAnalysis.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TRandom.h"
#include "TGraphErrors.h"
#include "TH2F.h"
#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "NewWave/WaveletCoefficient.hh"
#include "AtlasLabels.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisOfPFOS)
/*
* This class is for making NTuples of MET values.
*/

// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
do {                                                        \
  if( ! EXP.isSuccess() ) {                                 \
   Error( CONTEXT,                                          \
    XAOD_MESSAGE( "Failed to execute: %s" ),                \
                #EXP );                                     \
    return EL::StatusCode::FAILURE;                         \
}                                                           \
} while( false )


AnalysisOfPFOS :: AnalysisOfPFOS ()
{
}



EL::StatusCode AnalysisOfPFOS :: setupJob (EL::Job& job)
{
    job.useXAOD ();
    EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisOfPFOS :: histInitialize ()
{
    TFile *outputFile = wk()->getOutputFile (outputName);
    pileup_pt = new TNtuple("p_{T} of Truth Pile-up Particles", "p_{T} of Truth Pile-up Particles","MET");
    pileup_eta = new TNtuple("#eta of Truth Pile-up Particles", "#eta of Truth Pile-up Particles","MET");
    pileup_phi = new TNtuple("#phi of Truth Pile-up Particles", "#phi of Truth Pile-up Particles","MET");
    truth_pt = new TNtuple("p_{T} of Truth Particles", "p_{T} of Truth Particles","MET");
    resi_pt = new TNtuple("p_{T} of Residual", "p_{T} of Residual","MET");
    resi_pt->SetDirectory(outputFile);

    truth_pt->SetDirectory(outputFile);
    pileup_phi->SetDirectory(outputFile);
    pileup_eta->SetDirectory(outputFile);
    pileup_pt->SetDirectory(outputFile);



    PFO = new TNtuple("p_{T} of PFOs", "Particle Flow","MET");
    PFOwl = new TNtuple("p_{T} of PFOs w. Wavelets", "Particle Flow  with Wavelets","MET");
    PFOwl->SetDirectory(outputFile);
    PFO->SetDirectory(outputFile);

    chPFO = new TNtuple("p_{T} of Charged PFOs", "Charged Particle Flow","MET");
    neuPFO = new TNtuple("p_{T} of Neutral PFOs", "Neutral Particle Flow","MET");
    neuPFOwl = new TNtuple("p_{T} of Neutral PFOs w. Wavelets", "Neutral Particle Flow  with Wavelets","MET");
    chPFOwl = new TNtuple("p_{T} of Charged PFOs w. Wavelets", "Charged Particle Flow  with Wavelets","MET");

    hist_neuwl = new TH2F("Neutral PFOs w. Wavelets","Neutral PFOs w. Wavelets", 32, -3.2, 3.2, 32, -pi, pi );
    hist_chwl  = new TH2F("Charged PFOs w. Wavelets","Charged PFOs w. Wavelets",   32, -3.2, 3.2, 32, -pi, pi );
    hist_neu   = new TH2F("Neutral PFOs","Neutral PFOs",     32, -3.2, 3.2, 32, -pi, pi );
    hist_ch    = new TH2F("Charged PFOs","Charged PFOs",       32, -3.2, 3.2, 32, -pi, pi );
    hist_pfo    = new TH2F("PFOs","PFOs",       32, -3.2, 3.2, 32, -pi, pi );
    hist_pfowl    = new TH2F("PFOs w. Wavelets","PFOs w. Wavelets", 32, -3.2, 3.2, 32, -pi, pi );
    hist_truth    = new TH2F("Truth Particles","Truth Particles", 32, -3.2, 3.2, 32, -pi, pi );
    hist_resi    = new TH2F("Residual after Wavelet Denoising","Residual after Wavelet Denoising",    32, -3.2, 3.2, 32, -pi, pi );
    hist_pileup    = new TH2F("Truth Pile-up Particles","Truth Pile-up Particles",    32, -3.2, 3.2, 32, -pi, pi );

    hist_all_alphas = new TH2F("All Alphas","All Alphas", 50, 0, 50, 50, 0, 1);
    wk()->addOutput(hist_all_alphas);

    hist_all_coef = new TH2F("All Coefficients","All Coefficients", 50, 0, 50, 50, 0, 50000);
    wk()->addOutput(hist_all_coef);

    hist_all_coef_scale = new TH2F("All Coefficients from Scaling","All Coefficients from Scaling", 50, 0, 50, 50, 0, 50000);
    wk()->addOutput(hist_all_coef_scale);

    hist_all_coef_entropy = new TH2F("All Coefficients from Entropy","All Coefficients from Entropy", 50, 0, 50, 50, 0, 50000);
    wk()->addOutput(hist_all_coef_entropy);

    ch_eta= new TNtuple("#eta of Charged PFOs", "#eta of Charged PFOs","MET");
    ch_phi= new TNtuple("#phi of Charged PFOs", "#phi of Charged PFOs","MET");
    neu_eta= new TNtuple("#eta of Neutral PFOs", "#eta of Neutral PFOs","MET");
    neu_phi= new TNtuple("#phi of Neutral PFOs", "#phi of Neutral PFOs","MET");

    ch_eta->SetDirectory (outputFile);
    ch_phi->SetDirectory (outputFile);
    
    neu_eta->SetDirectory (outputFile);
    neu_phi->SetDirectory (outputFile);
    
    neuPFO->SetDirectory (outputFile);
    chPFO->SetDirectory (outputFile);

    neuPFOwl->SetDirectory (outputFile);
    chPFOwl->SetDirectory (outputFile);
    wk()->addOutput(hist_pileup);

    int i = 0;

    for (int ylevel = 0; ylevel < 7; ylevel++){
        for (int philevel = 0; philevel < 7; philevel++){
            // pfo
            coefficients.push_back(new TH2F(("wlcoef_phi" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(),("wlcoef_phi" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(), pow(2,ylevel-1), -3.2, 3.2, pow(2,philevel-1), 0, 2*pi ));
            wk()->addOutput(coefficients[i]);
            coefficients1D.push_back(new TNtuple(("wlcoef_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(),("wlcoef_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(), "MET"));
            wk()->addOutput(coefficients1D[i]);   
            
            // scaling + filtering
            coefficientsratio1D.push_back(new TNtuple(("ratio_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(),("ratio_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(), "MET"));
            wk()->addOutput(coefficientsratio1D[i]);          

            //scaling
            coefficientsscale1D.push_back(new TNtuple(("scale_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(),("scale_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(), "MET"));
            wk()->addOutput(coefficientsscale1D[i]);          
            
            // pileup
            pileupcoefficients.push_back(new TH2F(("pileup_#phi = " + to_string(philevel) + ", y = " + to_string(ylevel)).c_str(),("pileup_phi" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(), pow(2,ylevel-1), -3.2, 3.2, pow(2,philevel-1), 0, 2*pi ));
            wk()->addOutput(pileupcoefficients[i]);
            
            // entropi
            coefficientsentropi1D.push_back(new TNtuple(("entropi_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(),("entropi_phi1D" + to_string(philevel) + "_y" + to_string(ylevel)).c_str(), "MET"));
            wk()->addOutput(coefficientsentropi1D[i]);          
            i = i +1;  
        }
    }



    wk()->addOutput(hist_pfo);
    wk()->addOutput(hist_pfowl);
    wk()->addOutput(hist_truth);
    wk()->addOutput(hist_resi);
    wk()->addOutput(hist_neuwl);
    wk()->addOutput(hist_chwl);
    wk()->addOutput(hist_neu);
    wk()->addOutput(hist_ch);

    for (int i = 0; i < 49; i++){
        rms.push_back(new TNtuple(("#phi = " + to_string(i)).c_str(),("y = " + to_string(i)).c_str(),"MET"));
        wk()->addOutput(rms[i]);       
    }



    goodevent = false;
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisOfPFOS :: fileExecute ()
{
    cout << "file execute"  << endl;
    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisOfPFOS :: changeInput (bool firstFile)
{
    cout << "changeInput" << endl;
    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisOfPFOS :: initialize ()
{

    // count number of events
    m_eventCounter = 0;
    xAOD::TEvent* event = wk()->xaodEvent();

    // as a check, let's see the number of events in our xAOD
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisOfPFOS :: execute ()
{
    //-------------------------------------------------------------------------------------------------------
    //----------------------------------------- EVENT INFORMATION -------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    const xAOD::EventInfo* eventInfo = 0;

    xAOD::TEvent* event = wk()->xaodEvent();
    bool dummy = true;
    // print every 25 events, so we know where we are:
    if( (m_eventCounter % 1) ==0 ) //Info("execute()", "Event number = %i", m_eventCounter );
    m_eventCounter++;

    EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

    cout << "EventNumber = " << m_eventCounter << endl;
    // check if the event is data or MC
    // (many tools are applied either to data or MC)
    //bool isMC = false;
    // check if the event is MC
    //if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
        //isMC = true; // can do something with this later
        //cout << "This is MC" << endl;
    //}

    // fill the branches of our trees
    if (dummy == true) {
    double N_pileup = eventInfo->averageInteractionsPerCrossing();
    //if (N_pileup > 28 && goodevent == false) {
      //  goodevent = true;
    //}
    //else { goodevent = false;}
    //goodevent=true;
    //if (goodevent == true) {
        //-------------------------------------------------------------------------------------------------------
        //----------------------------------------- CONTAINERS -------------------------------------------
        //-------------------------------------------------------------------------------------------------------
    const xAOD::TrackParticleContainer* TrackParticles = 0;
    const xAOD::PFOContainer* chPFOs = 0;
    const xAOD::PFOContainer* neuPFOs = 0;
    const xAOD::MissingETContainer* METcontainer = 0;
    const xAOD::TruthEventContainer* xTruthEventContainer = 0;
    const xAOD::TruthParticleContainer* TruthParticles = 0;
    const xAOD::VertexContainer* PVs = 0;
    EL_RETURN_CHECK("execute()", event->retrieve( PVs, "PrimaryVertices" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( TrackParticles, "InDetTrackParticles" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( xTruthEventContainer, "TruthEvents"));
    EL_RETURN_CHECK("execute()", event->retrieve( chPFOs,  "JetETMissChargedParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( neuPFOs, "JetETMissNeutralParticleFlowObjects" ) );
    EL_RETURN_CHECK("execute()", event->retrieve( METcontainer, "MET_Truth"));
    EL_RETURN_CHECK("execute()", event->retrieve(TruthParticles, "TruthParticles"));
    EL_RETURN_CHECK("execute()", event->retrieve( TrackParticles, "InDetTrackParticles" ) );

    vector<TLorentzVector> truthInt;
    vector<TLorentzVector> truthNonInt;
    vector<TLorentzVector> truthPileup;

    xAOD::TruthEventContainer::const_iterator itr;
    for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
        //const xAOD::TruthVertex* nVert = (*itr)->truthVertex(0);
        //int nvertex = (*itr)->nTruthVertices();
        int nParth = (*itr)->nTruthParticles();
        //const xAOD::TruthParticle* nPart = (*itr)->truthParticle(0);

        for (int i = 0; i < nParth; i++) {
            const xAOD::TruthParticle* truth = (*itr)->truthParticle(i);

            TLorentzVector ps = TLorentzVector();
            ps.SetPxPyPzE(truth->p4().Px(), truth->p4().Py(), truth->p4().Pz(), truth->p4().E());
            if (truth->status() == 1 && truth->barcode() < 100000 ){ //abs(truth->eta()) < 3.2 && truth->status() == 1 &&
                if (abs(truth->pdgId()) != 12 && abs(truth->pdgId()) != 14 && abs(truth->pdgId()) != 16) {
                    truthInt.push_back(ps);
                    truth_pt->Fill(truth->p4().Pt());
                }              
                else {
                    truthNonInt.push_back(ps);
                }
            }
            if ( truth->status() == 1 && truth->barcode() > 100000){//abs(truth->eta()) < 3.2 && truth->status() == 1 &&
                truthPileup.push_back(ps);
            }
        }
    }


    //-------------------------------------------------------------------------------------------------------
    //---------------------------------------------- PFLOW --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    std::vector<TLorentzVector> PFO_vec;
    // copy particle flow objects (PFO) in to TLorentzVector
    for (unsigned int i = 0; i < chPFOs->size(); i++) {
        if (abs(chPFOs->at(i)->eta()) < 3.2){
            chPFO_vec.push_back(TLorentzVector());
            chPFO_vec.back().SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());
            chPFO_vec.back().SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(), chPFOs->at(i)->p4().Pz(), chPFOs->at(i)->p4().E());

            PFO_vec.push_back(TLorentzVector());
            PFO_vec.back().SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());
            PFO_vec.back().SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(), chPFOs->at(i)->p4().Pz(), chPFOs->at(i)->p4().E());

            ch_phi->Fill(chPFO_vec.back().Phi());
            ch_eta->Fill(chPFO_vec.back().Eta());
        }
    }

    for (unsigned int i = 0; i < neuPFOs->size(); i++) {
        if (abs(neuPFOs->at(i)->eta()) < 3.2){
            neuPFO_vec.push_back(TLorentzVector());
            neuPFO_vec.back().SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
            PFO_vec.push_back(TLorentzVector());
            PFO_vec.back().SetPtEtaPhiM(neuPFOs->at(i)->pt(), neuPFOs->at(i)->eta(), neuPFOs->at(i)->phi(), neuPFOs->at(i)->m());
            neuPFO_vec.back().SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(), neuPFOs->at(i)->p4().Pz(), neuPFOs->at(i)->p4().E());
            PFO_vec.back().SetPxPyPzE(neuPFOs->at(i)->p4().Px(), neuPFOs->at(i)->p4().Py(), neuPFOs->at(i)->p4().Pz(), neuPFOs->at(i)->p4().E());
        }
    }


    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ SORTING TRACKS ---------------------------------------
    //-------------------------------------------------------------------------------------------------------

    // matching tracks with PVs
    vector<TLorentzVector> track_PV; 
    vector<TLorentzVector> track_All; 
    vector<TLorentzVector> track_NonPV;
    Info("execute()", "Matching tracks with charged PFOs");

    for (const xAOD::TrackParticle* track : *TrackParticles) {
        bool match = true;
        xAOD::VertexContainer::const_iterator PV_itr = PVs->begin();
        xAOD::VertexContainer::const_iterator PV_end = PVs->end();
        PV_itr++;

        for ( ; PV_itr != PV_end; ++PV_itr) {
            for (unsigned i = 0; i < (*PV_itr)->nTrackParticles(); i++) {
                if (track == (*PV_itr)->trackParticle(i)) { match = false; break; }
            }
            if (match == false) { break; }
        }
        if (abs(track->eta()) < 3.2) {
            TLorentzVector ps = TLorentzVector();
            ps.SetPxPyPzE(track->p4().Px(), track->p4().Py(), track->p4().Pz(), track->p4().E());
            ps.SetPtEtaPhiM(track->pt(), track->eta(), track->phi(), track->m());

            if (match) { 
                track_PV.push_back( ps );
            }
            else { 
                track_NonPV.push_back( ps ); 
            }
            track_All.push_back(ps); }
        }
        // -- PFlow objects.
        vector<TLorentzVector> chPFO_PV; 
        vector<TLorentzVector> chPFO_NonPV;
        Info("execute()", "Sorting PFOs");

        // Charged
        for (unsigned int i = 0; i < chPFOs->size(); i++) {
            bool match = false;
            if (abs(chPFOs->at(i)->eta()) < 3.2) {
                TLorentzVector particle =  TLorentzVector();
                particle.SetPxPyPzE(chPFOs->at(i)->p4().Px(), chPFOs->at(i)->p4().Py(),chPFOs->at(i)->p4().Pz(),chPFOs->at(i)->p4().E());
                particle.SetPtEtaPhiM(chPFOs->at(i)->pt(), chPFOs->at(i)->eta(), chPFOs->at(i)->phi(), chPFOs->at(i)->m());

                for (size_t i = 0; i < track_PV.size(); i++) {
                    if (dR(particle, track_PV.at(i)) < 0.005) { match = true; break; }
                }
                if (match) { chPFO_PV   .push_back( particle ); }
                else       { chPFO_NonPV.push_back( particle ); }
            }
        }


    WaveletAnalysis pileupAnalyzer2(64,3.2,N_pileup, PFO_vec);
    std::vector<TLorentzVector> pileupwlevent2 = pileupAnalyzer2.denoise(4500.0).particles(); 

    std::vector<TLorentzVector> resi;

        for (unsigned int i = 0; i < PFO_vec.size(); i++) {
            bool match = false;
            if (abs(PFO_vec.at(i).Eta()) < 3.2) {
                TLorentzVector particle =  TLorentzVector();
                particle.SetPxPyPzE(PFO_vec.at(i).Px(), PFO_vec.at(i).Py(),PFO_vec.at(i).Pz(),PFO_vec.at(i).E());
                particle.SetPtEtaPhiM(PFO_vec.at(i).Pt(), PFO_vec.at(i).Eta(), PFO_vec.at(i).Phi(), PFO_vec.at(i).M());

                for (size_t i = 0; i < pileupwlevent2.size(); i++) {
                    if (dR(particle, pileupwlevent2.at(i)) < 0.005) { match = true; break; }
                }
                if (!match) { resi   .push_back( particle ); }
            }
        }




    WaveletAnalysis chwlAnalyzer(64,3.2,N_pileup, chPFO_vec);
    WaveletAnalysis neuwlAnalyzer(64,3.2,N_pileup, neuPFO_vec);

    chPFO_vec_new = chwlAnalyzer.denoise(0.00).particles();
    neuPFO_vec_new = neuwlAnalyzer.denoise(0.00).particles();



    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ PILEUP HISTOGRAMS --------------------------------------
    //-------------------------------------------------------------------------------------------------------

    WaveletAnalysis pileupAnalyzer(64,3.2,N_pileup, resi);
    NewWave::WaveletEvent<vector<TLorentzVector>> pileupwlevent = pileupAnalyzer.denoise(0.0); 

    std::vector<TH1F> coefficientspt;

    for (unsigned int i = 0; i < pileupwlevent.frequencyBands().size(); i++)
    {   
        int philevel = pileupwlevent.frequencyBands()[i].coefficients()[0].phiLevel();
        int ylevel = pileupwlevent.frequencyBands()[i].coefficients()[0].yLevel();
        coefficientspt.push_back(TH1F(("#phi = 0" + to_string(philevel) + "y = " + to_string(ylevel)).c_str(),("#phi = 0" + to_string(philevel) + "y = " + to_string(ylevel)).c_str(),1000,0.0,10000000));

        for (unsigned int j = 0; j < pileupwlevent.frequencyBands()[i].coefficients().size(); j++){
            NewWave::WaveletCoefficients coef_temp =  pileupwlevent.frequencyBands()[i].coefficients();
            pileupcoefficients[i]->Fill(coef_temp[j].yCentre(),coef_temp[j].phiCentre(), abs(coef_temp[j].value()));
            coefficientspt[i].Fill(abs(coef_temp[j].value()));
        }
        rms[i]->Fill(coefficientspt[i].GetStdDev(1));
    }


    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ coef analysis --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    WaveletAnalysis PFOwlAnalyzer(64,3.2,N_pileup, PFO_vec);
    WaveletAnalysis all(64,3.2,N_pileup, track_All);
    WaveletAnalysis PV(64,3.2,N_pileup, track_PV);
    WaveletAnalysis entropi(64,3.2,N_pileup, PFO_vec);
    WaveletAnalysis scaling(64,3.2,N_pileup, PFO_vec);


    NewWave::WaveletEvent<vector<TLorentzVector>> wlevent = PFOwlAnalyzer.denoise(0.0); 
    NewWave::WaveletEvent<vector<TLorentzVector>> evt_all = all.denoise(0.0);
    NewWave::WaveletEvent<vector<TLorentzVector>> evt_PV = PV.denoise(0.0);
    NewWave::WaveletEvent<vector<TLorentzVector>> evt_entropi = entropi.entropy(track_PV, PFO_vec);
    NewWave::WaveletEvent<vector<TLorentzVector>> evt_scale = scaling.trackscaling(track_All, track_PV);

    std::vector<double> alphas = entropi.getAlphas();

    for (unsigned int i = 0; i < wlevent.frequencyBands().size(); i++)
    {       

            NewWave::WaveletCoefficients coef_temp =  wlevent.frequencyBands()[i].coefficients();
            NewWave::WaveletCoefficients coef_scale =  evt_scale.frequencyBands()[i].coefficients();
            NewWave::WaveletCoefficients coef_PV =  evt_PV.frequencyBands()[i].coefficients();
            NewWave::WaveletCoefficients coef_All =  evt_all.frequencyBands()[i].coefficients();
            NewWave::WaveletCoefficients coef_entropi =  evt_entropi.frequencyBands()[i].coefficients();
            cout << "i = " << i << endl;
            hist_all_alphas->Fill(i,abs(alphas.at(i)), 1);
            cout << "level: " << i << endl;
        for (unsigned int j = 0; j < wlevent.frequencyBands()[i].coefficients().size(); j++){
            
            coefficientsratio1D[i]->Fill(abs(coef_PV[j].value())/abs(coef_All[j].value()));
            coefficientsscale1D[i]->Fill(abs(coef_scale[j].value()));
            coefficientsentropi1D[i]->Fill(abs(coef_entropi[j].value()));
            coefficients1D[i]->Fill(abs(coef_temp[j].value()));

            hist_all_coef_entropy->Fill(i,abs(coef_entropi[j].value()), 1);
            hist_all_coef_scale->Fill(i,abs(coef_scale[j].value()), 1);
            hist_all_coef->Fill(i,abs(coef_temp[j].value()), 1);
            coefficients[i]->Fill(coef_temp[j].yCentre(),coef_temp[j].phiCentre(), abs(coef_temp[j].value()));
            cout << "y = " << coef_temp[j].yCentre() << ", phi = " << coef_temp[j].phiCentre() << ", pt = " << coef_temp[j].value() << endl;
        }
    }

    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ MET --------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    std::vector<TLorentzVector> PFOwl_vec = wlevent.particles();

    std::vector<TLorentzVector> residual;
    for (TLorentzVector vec : PFO_vec) {
        bool match = false;

        for (TLorentzVector vecwl : PFOwl_vec) {
            if (dR(vec, vecwl) < 0.005) { match = true; break; }
        }
        if (match) {}
        else       { residual.push_back( vec ); }
    }

    TLorentzVector PFO_ET_neu_data = sum(neuPFO_vec);
    TLorentzVector PFO_ET_neuwl_data = sum(neuPFO_vec_new);
    TLorentzVector PFO_ET_ch_data = sum(chPFO_vec);
    TLorentzVector PFO_ET_chwl_data = sum(chPFO_vec_new);

    for (unsigned int i= 0; i < PFOwl_vec.size(); i++) {
        hist_pfowl->Fill(PFOwl_vec.at(i).Eta(), PFOwl_vec.at(i).Phi(), PFOwl_vec.at(i).Pt());
        PFOwl->Fill(PFOwl_vec.at(i).Pt());
    }
    for (unsigned int i= 0; i < PFO_vec.size(); i++) {
        hist_pfo->Fill(PFO_vec.at(i).Eta(), PFO_vec.at(i).Phi(), PFO_vec.at(i).Pt());
        PFO->Fill(PFO_vec.at(i).Pt());
    }
    for (unsigned int i= 0; i < residual.size(); i++) {
        hist_resi->Fill(residual.at(i).Eta(), residual.at(i).Phi(), residual.at(i).Pt());
        resi_pt->Fill(residual.at(i).Pt());
    }
    for (unsigned int i= 0; i < truthPileup.size(); i++) {
        hist_pileup->Fill(truthPileup.at(i).Eta(), truthPileup.at(i).Phi(), truthPileup.at(i).Pt());
        pileup_phi->Fill(truthPileup.at(i).Phi());
        pileup_eta->Fill(truthPileup.at(i).Eta());
        pileup_pt->Fill(truthPileup.at(i).Pt());
    }
    for (unsigned int i= 0; i < truthInt.size(); i++) {
        hist_truth->Fill(truthInt.at(i).Eta(), truthInt.at(i).Phi(), truthInt.at(i).Pt());
    }
    }   
    return EL::StatusCode::SUCCESS;

}



EL::StatusCode AnalysisOfPFOS :: postExecute ()
{
    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ FILL HISTOGRAMS --------------------------------------
    //-------------------------------------------------------------------------------------------------------
    for (unsigned int i= 0; i < chPFO_vec_new.size(); i++) { 
        hist_chwl->Fill(chPFO_vec_new.at(i).Eta(), chPFO_vec_new.at(i).Phi(),  chPFO_vec_new.at(i).Pt());
        chPFOwl->Fill(chPFO_vec_new.at(i).Pt());
    }    
    for (unsigned int i= 0; i < neuPFO_vec_new.size(); i++) {
        hist_neuwl->Fill(neuPFO_vec_new.at(i).Eta(), neuPFO_vec_new.at(i).Phi(),  neuPFO_vec_new.at(i).Pt());
        neuPFOwl->Fill(neuPFO_vec_new.at(i).Pt());
    }

    for (unsigned int i= 0; i < chPFO_vec.size(); i++) { 
        ch_phi->Fill(chPFO_vec.at(i).Phi());
        ch_eta->Fill(chPFO_vec.at(i).Eta());
        hist_ch->Fill(chPFO_vec.at(i).Eta(), chPFO_vec.at(i).Phi(), chPFO_vec.at(i).Pt());
        chPFO->Fill(chPFO_vec.at(i).Et());
    }    
    for (unsigned int i= 0; i < neuPFO_vec.size(); i++) {
        neu_phi->Fill(neuPFO_vec.at(i).Phi());
        neu_eta->Fill(neuPFO_vec.at(i).Eta());
        hist_neu->Fill(neuPFO_vec.at(i).Eta(), neuPFO_vec.at(i).Phi(), neuPFO_vec.at(i).Pt());
        neuPFO->Fill(neuPFO_vec.at(i).Et());
    }


    //-------------------------------------------------------------------------------------------------------
    //------------------------------------------------ CLEAR --------------------------------------
    //-------------------------------------------------------------------------------------------------------
    cout << "postExecute" << endl;
    chPFO_vec.clear();
    neuPFO_vec.clear();
    chPFO_vec_new.clear();
    neuPFO_vec_new.clear();
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisOfPFOS :: finalize ()
{
    cout << "finalize" << endl;

    //xAOD::TEvent* event = wk()->xaodEvent();
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisOfPFOS :: histFinalize ()
{
    return EL::StatusCode::SUCCESS;
}

TLorentzVector AnalysisOfPFOS :: sum(std::vector<TLorentzVector> v) {
    TLorentzVector sum;
    for (unsigned int i = 0; i < v.size(); i++) {
        sum += v.at(i);
    }
    return sum;
} 
double AnalysisOfPFOS :: dR(TLorentzVector U, TLorentzVector V) {
    float deta = fabs( U.Eta() - V.Eta() );
    float dphi = fabs( U.Phi() - V.Phi() );
    if (dphi > pi) dphi = 2.*pi - dphi;
    return sqrt( deta*deta + dphi*dphi );
}