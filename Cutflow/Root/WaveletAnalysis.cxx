#include "Cutflow/WaveletAnalysis.h"
#include <iostream>
//#include "TFunctor.h"
//#include "TBrentMinimizer1D.h"
#include "TF1.h"
#include "NewWave/NewWave.hh"
#include "NewWave/GSLEngine.hh"
#include "NewWave/WaveletCoefficient.hh"
#include "NewWave/WaveletEvent.hh"
#include "Math/BrentMinimizer1D.h"
#include "Math/WrappedTF1.h"
#include <math.h>       /* erf */
using namespace std;
/*
* Tool for wavelet analysis
*/


WaveletAnalysis::WaveletAnalysis(int nPixel, double yRange, double N_pileup, std::vector<TLorentzVector> myPFOs): 
    _pixelDefinition(nPixel, yRange),
    _waveletEngine(gsl_wavelet_haar, 2, _pixelDefinition)
{
    pileup = N_pileup;
    PFOs = myPFOs;
}


Double_t WaveletAnalysis :: jm(Double_t *x,Double_t *par) { 
    Double_t delta_s = abs(par[0] - x[0])/100.0;
    Double_t delta_n = abs(x[0] - par[1])/100.0;

    Double_t hn = 0;
    Double_t hs = 0;
    for (int i = 0; i < 100; i++){
        hn =  hn + i * delta_n * erfc((abs(x[0] - par[1])-i*delta_n)/(sqrt(2)*par[2]))*delta_n;
        hs =  hs + i * delta_s * erf((abs(par[0] - x[0])-i*delta_s)/(sqrt(2)*par[2]))*delta_s;
    }
    Double_t output = (1/(par[2]*par[2])*hs + par[3]*1/(par[2]*par[2])*hn);
    return output;
}


double WaveletAnalysis :: minimize_jm(double modelCoef, double coef, double alpha, double sigma) {
    double range = 0;
    if (abs(modelCoef) > abs(coef)){
        range = abs(modelCoef);
    }
    else{
        range = abs(coef);
    }
    TF1 func = TF1("fit",jm,-range,range,4);
    func.SetParameter(0,coef);
    func.FixParameter(0,coef);    
    func.SetParameter(1,modelCoef);
    func.FixParameter(1,modelCoef);
    func.SetParameter(2,sigma);
    func.FixParameter(2,sigma);
    func.SetParameter(3,alpha);
    func.FixParameter(3,alpha);
    func.SetNpx(abs(range));
    //double xmin = func.GetMinimumX(-range,range);
    //cout << "xmin:" << xmin << endl;
    ROOT::Math::BrentMinimizer1D bm;
    //bm.SetNpx(1E7);
   // use inverse of function

    ROOT::Math::WrappedTF1 wf(func);
    bm.SetFunction(wf,-range,range);
    bm.Minimize(50,1E-15,1.E-15);
    double xmin = bm.XMinimum();
   //  cout << "xmin1:" << xmin1 << endl;

    // double ymax = bm.FValMinimum();
    //printf (  "\nmax [xmin;ymin] : x=%e ; y=%+e  \n"    ,xmin   ,ymax    ) ;

    /*
    cout << "---- xmin = " << xmin << endl;
    cout << "---- coef = " << coef << endl;

    cout << "---- model = " << modelCoef << endl;
    cout << "-------------------------------- " << endl;
    
    /*
    if (abs(xmin - modelCoef) > 100 ) {
            cout << "---- xmin = " << xmin << endl;
    cout << "---- model = " << modelCoef << endl;
    cout << "-------------------------------- " << endl;
    }*/

    //func.SetParameters(modelCoef, alpha, sigma);
    //func.SetParNames ("modelCoef","alpha","sigma");
   /*ROOT::Math::Functor1D func(&jm);
 
   ROOT::Math::BrentMinimizer1D* bm;
   bm->SetFunction(func, -10,10);
   bm->Minimize(10,0,0);
 
   cout << "f(" << bm->XMinimum() << ") = " <<bm->FValMinimum() << endl;
   double bla = bm->XMinimum();
   return bla;
   */


   //  std::vector<double> output;
   //  std::vector<double> points;

   //   // cout << "-modelcoef" <<   modelCoef  << endl;
   //   // cout << "-coef" <<   coef << endl;
   //   // cout << "-sigma" <<   sigma << endl;
   //   // cout << "-alpha" <<   alpha << endl;
   // for (int x = -range; x < range; x++){
   //      double z = x*1.0;
   //      //cout << x << endl;
   //      double hn = 0.0;
   //      double hs = 0.0;
   //      double delta_s = abs(coef - z)/100.0;
   //      double delta_n = abs(z - modelCoef)/100.0;

   //      for (int i = 0; i < 100; i++){
   //          hn =  hn + i * delta_n * erfc((abs(z - modelCoef)-i*delta_n)/(sqrt(2.0)*sigma))*delta_n;
   //          hs =  hs + i * delta_s * erf((abs(coef - z)-i*delta_s)/(sqrt(2.0)*sigma))*delta_s;
   //      }
   //      output.push_back(1.0/(sigma*sigma)*hs + alpha*1.0/(sigma*sigma)*hn);
   //      points.push_back(z);
   // }

   // double mymin = 10000;
   // double myminx = 0;

   //  for (int k = 0; k < output.size(); k++) {
   //      if (output.at(k) < mymin){
   //          mymin = output.at(k);
   //          myminx = points.at(k);
   //      }
   //  }

  // cout << "myminx " << myminx << endl;


   return xmin;
}

std::vector<double> WaveletAnalysis :: getAlphas() {
    return allalphas;
}

NewWave::WaveletEvent<vector<TLorentzVector>> WaveletAnalysis :: entropy(vector<TLorentzVector> modelvec, vector<TLorentzVector> datavec) {
    NewWave::WaveletEvent<vector<TLorentzVector>> model(modelvec,  _pixelDefinition, _waveletEngine);
    NewWave::WaveletEvent<vector<TLorentzVector>> data(PFOs,  _pixelDefinition, _waveletEngine);

    std::vector<double> wlcoef;
    double alpha_min = 0;
    double alpha_max = 200;

    NewWave::WaveletEvent<vector<TLorentzVector>> wePFlow(PFOs,  _pixelDefinition, _waveletEngine);
    wePFlow.setPileUpThreshold(0.1);
    wePFlow.setScaleParticles(false);

    double sigma_array [49] = { 0.0, 0.0, 316.8, 444.4, 461.1, 
                        470.2, 476.5, 0.0, 0.0, 307.3,
                        426.8, 466.1, 471.9, 475.6, 554.9,
                        320.2, 442.9, 489.8, 492, 486.3,
                        488, 2416, 446.6, 518.8, 523.4,
                        522.3, 515.9, 514.2,  557.7, 520.6,
                        539.4, 562.3, 561, 570.1, 578.8,
                        561.2, 537, 554.3, 563.3, 571,
                        585.7, 606,  586.3, 543.9, 555.4,
                        570.3, 584.4, 608.5, 647.4}; 
     /*                   
    double sigma_array[49] = {100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100,100,
100,100,100,100
    };*/

    // double sigma_array [49] = { 0.0, 1400, 2000, 2000, 1800, 
    //                     1700, 0, 0.0, 1500, 2000,
    //                     1900, 1800, 1700, 4600, 1400,
    //                     1800, 2100, 2000, 1900, 1600,
    //                     2800, 2000, 2100, 2100, 2000,
    //                     1800, 1500, 2400,  2000, 2000,
    //                     2000, 1900, 1700, 1400, 2200,
    //                     2100, 2000, 1900, 1700, 1400,
    //                     1200, 2000,  1900, 1800, 1700,
    //                     1500, 1300, 1100, 1300}; 

    double alpha = (alpha_min + alpha_max)/2.0;
    double epsilon = 0.001;

    double sigma_r = 0.0;
    //double sigma_r_old = 0;

    for (unsigned int i = 0; i < model.frequencyBands().size(); i++)
    {   
        alpha_min = 0.0;
        alpha_max = 1.0;
        double sigma = sigma_array[i];
        int itr = 0;
        std::vector<double> wlcoef_temp;


        while (alpha_max - alpha_min > epsilon ){

            wlcoef_temp.clear();

            

            alpha = (alpha_min + alpha_max)/2.0;
            // alpha = 1;
            // alpha_max = 1;
            // alpha_min = 1;
            double sigma_r_temp = 0;

            for (int j = 0; j < data.frequencyBands()[i].coefficients().size(); j++){
                if (data.frequencyBands()[i].coefficients()[j].value() > 0.0){
                    //alpha = 1.0/(data.frequencyBands()[i].rms()*data.frequencyBands()[i].rms());
                    double coef_temp = minimize_jm(model.frequencyBands()[i].coefficients()[j].value(),data.frequencyBands()[i].coefficients()[j].value(), alpha,data.frequencyBands()[i].rms()); //
                    // cout << "---------------------------------------------------------" << endl;
                    // cout << " model: " << model.frequencyBands()[i].coefficients()[j].value() << endl;
                    // cout << " data : " << data.frequencyBands()[i].coefficients()[j].value() << endl;
                    // cout << " rms  : " << model.frequencyBands()[i].rms() << endl;
                    // cout << " coef : " << coef_temp << endl;
                    // cout << "------------" << endl;

                    wlcoef_temp.push_back(coef_temp);
                    sigma_r_temp += (data.frequencyBands()[i].coefficients()[j].value() - coef_temp)*(data.frequencyBands()[i].coefficients()[j].value() - coef_temp);                
                }
                else {
                     wlcoef_temp.push_back(0.0);
                }
            }
            if (wlcoef_temp.size() > 1) {
                sigma_r = sqrt(sigma_r_temp/data.frequencyBands()[i].coefficients().size());
            }
            //cout << "sigma_r = " << sigma_r << ", sigma = " << sigma << endl;
            if (sigma_r == 0) {
                alpha_min = 0;
                alpha_max = 0;
            }
            if (sigma_r > sigma) {
                alpha_max = alpha;
            }
            else {
                alpha_min = alpha;
            }
        //         alpha_min = 1;
        // alpha_max = 1;
            // cout << "(alpha_max - alpha_min) = " << (alpha_max - alpha_min) << endl;
            //cout << "alpha = " << alpha << endl;
            // cout << "iteration: " << itr << endl;
            // cout << "--------------------------------------------------- " << endl;
        }
        allalphas.push_back(alpha);
        for (int l = 0; l < wlcoef_temp.size(); l++){
            wlcoef.push_back(wlcoef_temp.at(l));
        }
    }


    // for (unsigned int i = 0; i < model.frequencyBands().size(); i++)
    // {   
    //     alpha = allalphas.at(i);
    //     std::vector<double> wlcoef_temp;
    //     wlcoef_temp.clear();
    //     for (int j = 0; j < data.frequencyBands()[i].coefficients().size(); j++){
    //         if (data.frequencyBands()[i].coefficients()[j].value() > 0.0){
    //             double coef_temp = minimize_jm(model.frequencyBands()[i].coefficients()[j].value(),data.frequencyBands()[i].coefficients()[j].value(), alpha,data.frequencyBands()[i].rms()); //
    //             wlcoef.push_back(coef_temp);
    //         }
    //         else{
    //             wlcoef.push_back(0.0);
    //         }
    //     }
    // }       
    
    double Pileup = pileup;

    wePFlow.scale([wlcoef,Pileup](const NewWave::WaveletCoefficient &c)->double
        {
            if (c.isSmoothing()){
                return 1.0 / Pileup;
            }
            return (c.value() == 0.) ? 0. : abs(wlcoef.at(c.key()))/abs(c.value()); // ? 1.
        }
    );
    return wePFlow;
}


NewWave::WaveletEvent<vector<TLorentzVector>> WaveletAnalysis :: trackscaling(std::vector<TLorentzVector> fjTrackAll, std::vector<TLorentzVector> fjTrackPV) {
    NewWave::WaveletEvent<vector<TLorentzVector>> wePFlow(PFOs,  _pixelDefinition, _waveletEngine);
    wePFlow.setPileUpThreshold(0.65);
    wePFlow.setScaleParticles(false);
    NewWave::WaveletEvent<vector<TLorentzVector>> weTrackPV(fjTrackPV,  _pixelDefinition, _waveletEngine);
    NewWave::WaveletEvent<vector<TLorentzVector>> weTrackAll(fjTrackAll, _pixelDefinition, _waveletEngine);

    NewWave::WaveletCoefficients cTrackPV  = weTrackPV.coefficients();
    NewWave::WaveletCoefficients cTrackAll = weTrackAll.coefficients();
    double Pileup = pileup;

    wePFlow.scale([cTrackPV,cTrackAll,Pileup](const NewWave::WaveletCoefficient &c)->double
        {
            if (c.isSmoothing()){
                return 1.0 / Pileup;
            }
            return (cTrackAll[c.key()].value() == 0.) ? 0. : cTrackPV[c.key()].value() / cTrackAll[c.key()].value(); // ? 1.
        }
    );
    return wePFlow;
}

NewWave::WaveletEvent<vector<TLorentzVector>> WaveletAnalysis :: trackfilter(std::vector<TLorentzVector> fjTrackAll, std::vector<TLorentzVector> fjTrackPV, double cut) {
    NewWave::WaveletEvent<vector<TLorentzVector>> wePFlow(PFOs,  _pixelDefinition, _waveletEngine);
    wePFlow.setPileUpThreshold(0.65);
    wePFlow.setScaleParticles(false);
    
    NewWave::WaveletEvent<vector<TLorentzVector>> weTrackPV(fjTrackPV,  _pixelDefinition, _waveletEngine);
    NewWave::WaveletEvent<vector<TLorentzVector>> weTrackAll(fjTrackAll, _pixelDefinition, _waveletEngine);
    double Pileup = pileup;


    NewWave::WaveletCoefficients cTrackPV  = weTrackPV.coefficients();
    NewWave::WaveletCoefficients cTrackAll = weTrackAll.coefficients();

    wePFlow.scale([cTrackPV, cTrackAll, cut,Pileup](const NewWave::WaveletCoefficient &c)->double {

        if (c.isSmoothing()){
            return 1.0 / Pileup;
        }
        if (cTrackAll[c.key()].value() == 0.){
            return 0.;
        }
        if (fabs(cTrackPV[c.key()].value() / cTrackAll[c.key()].value()) < cut){
            return 0.;
        }
        else {
            return 1.0;
        }
    });
    return wePFlow;
}



NewWave::WaveletEvent<vector<TLorentzVector>> WaveletAnalysis :: mydenoise() {
    NewWave::WaveletEvent<vector<TLorentzVector>> wePFlow(PFOs,  _pixelDefinition, _waveletEngine);
    wePFlow.setPileUpThreshold(0.65);
    wePFlow.setScaleParticles(false);
    double Pileup = pileup;

    wePFlow.scale([Pileup](const NewWave::WaveletCoefficient &c)->double {
        int m = c.yLevel();
        int n = c.phiLevel();
        double t = 1000 * Pileup * 64 / pow(2., m + n);
        if (c.isSmoothing()){
            return 1.0 / Pileup;
        }
        return (fabs(c.value()) < t) ? 0. : 1.0;
    });

    return wePFlow;
}



NewWave::WaveletEvent<vector<TLorentzVector>> WaveletAnalysis :: denoise(double cut) {
    NewWave::WaveletEvent<vector<TLorentzVector>> wePFlow(PFOs,  _pixelDefinition, _waveletEngine);
    wePFlow.setPileUpThreshold(0.65);
    wePFlow.setScaleParticles(false);
    double Pileup = pileup;

    wePFlow.scale([cut,Pileup](const NewWave::WaveletCoefficient &c)->double {
        if (cut == 0) {
            return 1.0;
        }
        if (c.isSmoothing()){
            return 1.0 / Pileup;
        }
        return (fabs(c.value()) < cut) ? 0. : 1.0;
    });
    return wePFlow;
}

